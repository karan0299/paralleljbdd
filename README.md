# PJBDD
PJBDD is a competitive and lightweight multithreated BDD library. 
Several java parallelization technologies are used to implement different versions of the BDD framework with parallel algorithms.
The library supports fully automated resource management and is highly customizable.
PJBDD supports Binary Decision Diagrams (BDD) and Zero-suppressed Decision Diagrams (ZDD).

## Getting Started
Installation is possible via Ivy.

**Usage:**

```Java
//instantiate a new bdd creator with 4 worker
     Creator creator =
          Builders.newBDDBuilder()
              .setVarCount(5)
              .setThreads(4)
              .build();
              
// Create bdd "f1 OR f2" with two integer variables
BDD f1 = creator.makeVariable();
BDD f2 = creator.makeVariable();
BDD orBDD = creator.makeOr(f1,f2);

//Get number of all satisfying true assingments
BigInteger satCount = creator.satCount(orBDD);

// print dot representation of "f1 OR f2"
System.out.println(new DotExporter().bddToString(orBDD));
```

## Examples
A n-queens example has been included to get you started:
```
paralleljbdd/src/org/sosy_lab/java_pbdd/examples/NQueens.java
paralleljbdd/src/org/sosy_lab/java_pbdd/examples/NQueensZDD.java
```