package org.sosy_lab.pjbdd.uniquetable;

import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.Lock;
import org.sosy_lab.pjbdd.uniquetable.bdd.BDDConcurrentArray;
import org.sosy_lab.pjbdd.uniquetable.bdd.BDDLockOnWriteArray;
import org.sosy_lab.pjbdd.uniquetable.bdd.BDDResizingUniqueTable;
import org.sosy_lab.pjbdd.uniquetable.intBDD.ConcurrentIntUniqueTable;

/**
 * Abstract class as base class for table implementations with backing arrays. This class does not
 * handle concurrent accesses or arrays itself.
 *
 * <p>Subclasses: {@link BDDConcurrentArray}, {@link BDDLockOnWriteArray} , {@link
 * BDDResizingUniqueTable} , {@link ConcurrentIntUniqueTable}.
 *
 * @author Stephan Holzner
 * @see BDDConcurrentArray
 * @see BDDLockOnWriteArray
 * @see BDDResizingUniqueTable
 * @see ConcurrentIntUniqueTable
 * @since 1.0
 */
public abstract class ResizingTable {
  /** maximal array size. */
  private static final int MAX_SIZE = Integer.MAX_VALUE - 5;
  /** representation for next free allocated bdd node. */
  protected final AtomicInteger nextFree = new AtomicInteger();
  /** number of free allocated bdd nodes. */
  protected final AtomicInteger freeCounter = new AtomicInteger();
  /** number of allocated bdd nodes. */
  protected final AtomicInteger nodeCount = new AtomicInteger();
  /** is resize operation in progress. */
  protected boolean resizeInProgress = false;
  /** resizing table's increase factor. */
  protected int increaseFactor;

  /**
   * Returns next free index and set next free pointer to next uses lock.
   *
   * @return next free index
   */
  protected int getNextFree() {
    getNextLock().lock();
    try {
      int next = nextFree.get();
      nextFree.set(getNext(next));
      return next;
    } finally {
      getNextLock().unlock();
    }
  }

  /**
   * Checks if table's reaches threshold and table resize must be performed.
   *
   * @return true if minimal threshold of free nodes reached or {@link #resizeInProgress} else
   */
  protected boolean checkResize() {
    if (nextFree.get() > 1) {
      return resizeInProgress;
    }
    return true;
  }

  /** Start table resize task. resolves new size and calls {@link #doResize(int, int)}. */
  protected void resizeTable() {
    int oldSize = nodeCount.get();
    int newSize = nodeCount.get();

    if (increaseFactor > 0) {
      newSize += newSize * increaseFactor;
    } else {
      newSize = newSize << 1;
    }

    if (newSize > MAX_SIZE) {
      newSize = MAX_SIZE;
    }

    doResize(oldSize, newSize);
  }

  /**
   * actually resize of table. Copies all entries to new table.
   *
   * @param oldSize - old table size
   * @param newSize - new table size
   */
  protected abstract void doResize(int oldSize, int newSize);

  /**
   * Get index of next bdd with same hashcode.
   *
   * @param root - the nodes index
   * @return next node's index with same hashcode
   */
  protected abstract int getNext(int root);

  /**
   * Get monitor object for next free index resolving.
   *
   * @return lock object
   */
  protected abstract Lock getNextLock();
}
