package org.sosy_lab.pjbdd.uniquetable.intBDD;

import java.util.function.IntConsumer;
import org.sosy_lab.pjbdd.creator.bdd.intBDD.IntCreator;

/**
 * Main int based unique table interface defining all unique table methods. Used in {@link
 * IntCreator} implementations.
 *
 * @author Stephan Holzner
 * @see IntCreator
 * @since 1.0
 */
public interface IntUniqueTable {

  /** int representation of invalid bdds. */
  int INVALID_BDD = -1;
  /** int representation of logical true bdd. */
  int ONE = 1;
  /** int representation of logical false bdd. */
  int ZERO = 0;

  /**
   * Get a bdd's low branch.
   *
   * @param r - the root bdd
   * @return root's low branch
   */
  int getLow(int r);

  /**
   * Get a bdd's high branch.
   *
   * @param r - the root bdd
   * @return root's high branch
   */
  int getHigh(int r);

  /**
   * Get a bdd variable's level.
   *
   * @param r - the root bdd
   * @return level of root's variable
   */
  int getVariable(int r);

  /** write unique table's statistics to console. */
  void printStats();

  /**
   * get current number of nodes.
   *
   * @return current number of nodes
   */
  int getNodeCount();

  /**
   * increase reference count for root bdd.
   *
   * @param root - the root bdd
   */
  void incRef(int root);

  /**
   * increase reference count for root multiple bdds.
   *
   * @param roots - the root bdd
   */
  void incRef(int... roots);

  /**
   * decrease reference count for root bdd.
   *
   * @param root - the root bdd
   */
  void decRef(int root);

  /**
   * decrease reference count for root multiple bdd.
   *
   * @param roots - the root bdds
   */
  void decRef(int... roots);

  /**
   * Get bdd with given input parameters or create and new.
   *
   * @param var - given variable
   * @param low - given low branch
   * @param high - given high branch
   * @return matching bdd's index
   */
  int getOrCreate(int var, int low, int high);

  /**
   * set level for given bdd.
   *
   * @param root - bdd
   * @param level - to set
   */
  void setVariable(int root, int level);

  /**
   * set reference count of given bdd to max (avoid garbage collection).
   *
   * @param root - bdd
   */
  void setMaxRef(int root);

  /** clear unique table. */
  void clear();

  /**
   * Apply function on each bdd in table.
   *
   * @param function - the function
   */
  void forEach(IntConsumer function);

  /**
   * Swap two variables and update all corresponding bdd.
   *
   * @param upper - the upper variable to be swapped down
   * @param lower - the lower variable to be swapped up
   * @param delegate - the node managing delegate
   */
  void swap(int upper, int lower, NodeMaker delegate);

  interface NodeMaker {
    int makeNode(int var, int low, int high);
  }
}
