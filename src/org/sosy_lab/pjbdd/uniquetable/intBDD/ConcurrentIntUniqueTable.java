package org.sosy_lab.pjbdd.uniquetable.intBDD;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.function.IntConsumer;
import java.util.stream.IntStream;
import org.sosy_lab.pjbdd.uniquetable.ResizingTable;
import org.sosy_lab.pjbdd.util.HashCodeGenerator;
import org.sosy_lab.pjbdd.util.PrimeUtils;

/**
 * Main IntUniqueTable implementation. Concurrent IntUniqueTable implementation which underlying
 * uses a resizing int array as data structure and a {@link Lock} array to guarantee thread-safe and
 * concurrent access.
 *
 * @author Stephan Holzner
 * @see ResizingTable
 * @see IntUniqueTable
 * @since 1.0
 */
public class ConcurrentIntUniqueTable extends ResizingTable implements IntUniqueTable {

  /** reference count bit mask. */
  private static final int REF_MASK = 0xFFC00000;
  /** level bit mask. */
  private static final int VAR_MASK = 0x001FFFFF;
  /** reference count modifier bit mask. */
  private static final int REF_INC = 0x00400000;

  /** offset of reference count and level in table. */
  private static final int OFFSET_REF_COUNT_AND_VAR = 0;

  /** offset of low index in table. */
  private static final int OFFSET_LOW = 1;

  /** offset of high index in table. */
  private static final int OFFSET_HIGH = 2;

  /** offset of hash value in table. */
  private static final int OFFSET_HASH = 3;

  /** offset of next index with same hash value in table. */
  private static final int OFFSET_NEXT = 4;

  /** each node's size in table. */
  private static final int NODE_SIZE = 5;

  /** bdd node table containing 5 entries for each node. */
  private int[] table;

  /** Locks to handle concurrent access. */
  private Lock[] locks;
  /** Lock for synchronized resize actions. */
  private Lock resizeLock;
  /** Lock for synchronized next index calculations. */
  private Lock nextLock;
  /** Lock for synchronized reference count modifications. */
  private Lock refLock;

  /**
   * Creates a new {@link ConcurrentIntUniqueTable} with specified initial size, parallelism and
   * increase factor.
   *
   * @param initialSize - the initial table size
   * @param parallelism - the concurrency factor for maximal concurrent accesses
   * @param increaseFactor - the table's increase factor
   */
  public ConcurrentIntUniqueTable(int initialSize, int parallelism, int increaseFactor) {
    this.nodeCount.set(PrimeUtils.getGreaterNextPrime(initialSize));
    this.increaseFactor = increaseFactor;
    table = new int[this.nodeCount.get() * NODE_SIZE];

    for (int n = 0; n < this.nodeCount.get(); n++) {
      setLow(n, INVALID_BDD);
      setNext(n, n + 1);
    }
    setNext(this.nodeCount.get() - 1, 0);

    setMaxRef(0);
    setMaxRef(1);
    setLow(0, 0);
    setHigh(0, 0);
    setVariable(0, -1);
    setLow(1, 1);
    setHigh(1, 1);
    setVariable(1, -2);

    nextFree.set(2);
    freeCounter.set(this.nodeCount.get() - 2);

    initLocks(parallelism);
  }

  /** {@inheritDoc} */
  @Override
  public int getOrCreate(int var, int low, int high) {
    int hash = hashNode(var, low, high);
    boolean resize = false;
    boolean computationComplete = false;
    int res = 0;
    Lock lock = getSegmentLock(hash);
    lock.lock();
    try {
      if (hash == hashNode(var, low, high)) {

        res = getNodeWithHash(hash);

        while (res != 0) {
          if (getVariable(res) == var && getLow(res) == low && getHigh(res) == high) {
            incRef(res);
            computationComplete = true;
            break;
          }

          res = getNext(res);
        }
        if (!computationComplete) {
          if (nextFree.get() < 2) {
            resize = true;
          } else {
            res = getNextFree();
            if (res >= 2) {
              freeCounter.decrementAndGet();
              setVariable(res, var);
              setLow(res, low);
              setHigh(res, high);

              setNext(res, getNodeWithHash(hash));
              setHash(hash, res);
              incRef(res, high, low);
              computationComplete = true;
            }
          }
        }
      }
    } finally {
      lock.unlock();
    }
    if (resize) {
      tryResizing();
    }
    if (!computationComplete) {
      return getOrCreate(var, low, high);
    }
    return res;
  }

  /** {@inheritDoc} */
  @Override
  public void printStats() {
    throw new UnsupportedOperationException("Statistic printing not yet implemented");
  }

  /** Try to resize table. Wait if resize op is already in progress. Collect all locks else. */
  private void tryResizing() {
    if (checkResize()) {
      resizeLock.lock();
      try {
        if (checkResize()) {
          fullyLock();
          resizeInProgress = true;
          resizeTable();
          resizeInProgress = false;
          fullyUnlock();
        }
      } finally {
        resizeLock.unlock();
      }
    }
  }

  /** {@inheritDoc} */
  @Override
  protected void doResize(int oldSize, int newSize) {
    clean();

    newSize = PrimeUtils.getLowerNextPrime(newSize);

    if (oldSize > newSize) {
      return;
    }

    int[] newUniqueTable;

    newUniqueTable = new int[newSize * NODE_SIZE];
    System.arraycopy(table, 0, newUniqueTable, 0, table.length);
    table = newUniqueTable;
    nodeCount.set(newSize);
    int n;
    for (n = 0; n < oldSize; n++) {
      setHash(n, 0);
      setNext(n, 0);
    }

    for (n = oldSize; n < nodeCount.get(); n++) {
      setLow(n, INVALID_BDD);
      setNext(n, n + 1);
    }
    rehash();
  }

  /** clean unused references. */
  private void clean() {
    for (int root = nodeCount.get() - 1; root > 2; --root) {
      if (getLow(root) != INVALID_BDD) {
        if (!hasRef(root)) {
          cascadingDelete(root);
        }
      }
    }
  }

  /**
   * cascading deRef collected node's children.
   *
   * @param root - the collected node
   */
  private void cascadingDelete(int root) {
    int low = getLow(root);
    int high = getHigh(root);
    setLow(root, INVALID_BDD);
    setHigh(root, 0);
    setHash(root, 0);
    setVariable(root, 0);
    freeCounter.incrementAndGet();
    if (low != INVALID_BDD) {
      internalDecRef(low);
      if (!hasRef(low)) {
        cascadingDelete(low);
      }
    }
    if (high != INVALID_BDD) {
      internalDecRef(high);
      if (!hasRef(high)) {
        cascadingDelete(high);
      }
    }
  }

  /**
   * Init locks with given parallelism level. Each level has a corresponding {@link Lock} object.
   *
   * @param parallelism - level
   */
  private void initLocks(int parallelism) {
    nextLock = new ReentrantLock();
    resizeLock = new ReentrantLock();
    refLock = new ReentrantLock();
    locks = new Lock[parallelism];
    IntStream.range(0, parallelism).forEach(i -> locks[i] = new ReentrantLock());
  }

  /** Rehash table entries after resize task, because hash value changes with table's size. */
  private void rehash() {
    nextFree.set(0);
    freeCounter.set(0);

    for (int n = nodeCount.get() - 1; n >= 2; n--) {
      if (getLow(n) != INVALID_BDD) {
        int hash;

        hash = hashNode(getVariable(n), getLow(n), getHigh(n));
        setNext(n, getNodeWithHash(hash));
        setHash(hash, n);
      } else {
        setNext(n, nextFree.get());
        nextFree.set(n);
        freeCounter.incrementAndGet();
      }
    }
  }

  /** {@inheritDoc} */
  @Override
  public void setMaxRef(int node) {
    table[node * NODE_SIZE + OFFSET_REF_COUNT_AND_VAR] |= REF_MASK;
  }

  /** {@inheritDoc} */
  @Override
  public void clear() {
    table = null;
  }

  /** {@inheritDoc} */
  @Override
  public void forEach(IntConsumer function) {
    IntStream.range(0, nodeCount.get())
        .forEach(
            i -> {
              if (getLow(i) != INVALID_BDD) { // filter unused nodes
                function.accept(i);
              }
            });
  }

  /** {@inheritDoc} */
  @Override
  public void swap(int upper, int lower, NodeMaker delegate) {
    forEach(
        bdd -> {
          if (getVariable(bdd) == lower) {
            int oldHash = hashNode(bdd);

            int low = getLow(bdd);
            int high = getHigh(bdd);

            if (getVariable(low) == upper || getVariable(high) == upper) {
              int low0 = low;
              int low1 = low;
              int high0 = high;
              int high1 = high;

              if (getVariable(low) == upper) {
                low0 = getLow(low);
                low1 = getHigh(low);
              }

              if (getVariable(high) == upper) {
                high0 = getLow(high);
                high1 = getHigh(high);
              }

              low = delegate.makeNode(lower, low0, high0);
              high = delegate.makeNode(lower, low1, high1);
              decRef(getHigh(bdd));
              decRef(getLow(bdd));

              setVariable(bdd, upper);
              setHigh(bdd, high);
              setLow(bdd, low);
              rehash(bdd, oldHash);
            }
          }
        });
  }

  /**
   * Rehash bdd node after variable swap changes.
   *
   * @param bdd - node to be rehashed
   * @param oldHash - old hash code before swap
   */
  private void rehash(int bdd, int oldHash) {
    int newHash = hashNode(bdd);
    if (oldHash != newHash) {
      int res = getNodeWithHash(oldHash);
      int previous = res;
      while (res != bdd) {
        previous = res;
        res = getNext(res);
      }
      if (previous == res) { // bdd is result of getNodeWithHash
        setHash(oldHash, getNext(res));
      } else {
        setNext(previous, getNext(res));
      }
      setNext(bdd, getNodeWithHash(newHash));
      setHash(newHash, bdd);
    }
  }

  /** {@inheritDoc} */
  @Override
  public void incRef(int node) {
    refLock.lock();
    try {
      internalIncRef(node);
    } finally {
      refLock.unlock();
    }
  }

  /** {@inheritDoc} */
  @Override
  public void incRef(int... roots) {
    refLock.lock();
    try {
      for (int root : roots) {
        internalIncRef(root);
      }
    } finally {
      refLock.unlock();
    }
  }

  /**
   * increase node's reference count value.
   *
   * @param node - a bdd
   */
  private void internalIncRef(int node) {

    if (node < 2 || node >= getNodeCount() || getLow(node) == IntUniqueTable.INVALID_BDD) {
      return;
    }
    if ((table[node * NODE_SIZE + OFFSET_REF_COUNT_AND_VAR] & REF_MASK) != REF_MASK) {
      table[node * NODE_SIZE + OFFSET_REF_COUNT_AND_VAR] += REF_INC;
    }
  }

  /** {@inheritDoc} */
  @Override
  public void setVariable(int node, int val) {
    table[node * NODE_SIZE + OFFSET_REF_COUNT_AND_VAR] &= ~VAR_MASK;
    table[node * NODE_SIZE + OFFSET_REF_COUNT_AND_VAR] |= val;
  }

  /** {@inheritDoc} */
  @Override
  public int getHigh(int r) {
    if (r <= ONE) {
      return r;
    }
    return table[r * NODE_SIZE + OFFSET_HIGH];
  }

  /** {@inheritDoc} */
  @Override
  public int getLow(int r) {
    if (r <= ONE) {
      return r;
    }
    return table[r * NODE_SIZE + OFFSET_LOW];
  }

  /** {@inheritDoc} */
  @Override
  public int getVariable(int node) {
    return table[node * NODE_SIZE + OFFSET_REF_COUNT_AND_VAR] & VAR_MASK;
  }

  /** {@inheritDoc} */
  @Override
  public int getNodeCount() {
    return nodeCount.get() - freeCounter.get();
  }

  /** {@inheritDoc} */
  @Override
  public void decRef(int root) {
    refLock.lock();
    try {
      internalDecRef(root);
    } finally {
      refLock.unlock();
    }
  }

  /** {@inheritDoc} */
  @Override
  public void decRef(int... roots) {
    refLock.lock();
    try {
      for (int root : roots) {
        internalDecRef(root);
      }
    } finally {
      refLock.unlock();
    }
  }

  /**
   * decreases node's reference count value.
   *
   * @param node - a bdd
   */
  private void internalDecRef(int node) {
    if (node < 2 || node >= getNodeCount() || getLow(node) == IntUniqueTable.INVALID_BDD) {
      return;
    }
    if (hasRef(node)) {
      if ((table[node * NODE_SIZE + OFFSET_REF_COUNT_AND_VAR] & REF_MASK) != REF_MASK) {
        table[node * NODE_SIZE + OFFSET_REF_COUNT_AND_VAR] -= REF_INC;
      }
    }
  }

  /**
   * Calculates the hashcode for a node at given index.
   *
   * @param index - given bdd's index
   * @return hash code of given bdd
   */
  private int hashNode(int index) {
    return hashNode(getVariable(index), getLow(index), getHigh(index));
  }

  /**
   * Set low for given bdd.
   *
   * @param root - the bdd
   * @param low - bdd
   */
  private void setLow(int root, int low) {
    table[root * NODE_SIZE + OFFSET_LOW] = low;
  }

  /**
   * Set high for given bdd.
   *
   * @param root - the bdd
   * @param high - bdd
   */
  private void setHigh(int root, int high) {
    table[root * NODE_SIZE + OFFSET_HIGH] = high;
  }

  /**
   * Lookup node with given hash value.
   *
   * @param hash - the hash value
   * @return node with given hash value
   */
  private int getNodeWithHash(int hash) {
    return table[hash * NODE_SIZE + OFFSET_HASH];
  }

  /**
   * Set hash for given index.
   *
   * @param hash bdd's hash value
   * @param index bdd's index
   */
  private void setHash(int hash, int index) {
    table[hash * NODE_SIZE + OFFSET_HASH] = index;
  }

  /** {@inheritDoc} */
  @Override
  protected int getNext(int root) {
    return table[root * NODE_SIZE + OFFSET_NEXT];
  }

  /** {@inheritDoc} */
  @Override
  protected Lock getNextLock() {
    return nextLock;
  }

  /**
   * set next bdd with same hashcode.
   *
   * @param root bdd
   * @param next bdd with same hash
   */
  private void setNext(int root, int next) {
    table[root * NODE_SIZE + OFFSET_NEXT] = next;
  }

  /**
   * calculate hashcode for given input params.
   *
   * @param lvl - bdd's level
   * @param low - bdd's low branch
   * @param high - bdd's high branch
   * @return hashcode
   */
  private int hashNode(int lvl, int low, int high) {
    return Math.abs(HashCodeGenerator.generateHashCode(lvl, low, high) % nodeCount.get());
  }

  /**
   * Check if a given bdd node has remaining references.
   *
   * @param node - given bdd
   * @return true if given bdd has remaining references, false else
   */
  private boolean hasRef(int node) {
    return (table[node * NODE_SIZE + OFFSET_REF_COUNT_AND_VAR] & REF_MASK) != 0;
  }

  /**
   * Lock all table accesses, with locking all {@link Lock} objects in {@link #locks} used before
   * resize.
   */
  private void fullyLock() {
    IntStream.range(0, locks.length).forEach(i -> locks[i].lock());
  }

  /**
   * Unlock all table accesses, with unlocking all {@link Lock} objects in {@link #locks} used after
   * resize.
   */
  private void fullyUnlock() {
    IntStream.range(0, locks.length).forEach(i -> locks[i].unlock());
  }

  /**
   * Get the hash segment {@link Lock} object for a given hash value.
   *
   * @param hash - the given hash value
   * @return hash segment's {@link Lock} object
   */
  private Lock getSegmentLock(int hash) {
    return locks[Math.abs(hash % locks.length)];
  }
}
