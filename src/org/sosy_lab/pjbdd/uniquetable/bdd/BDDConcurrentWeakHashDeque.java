package org.sosy_lab.pjbdd.uniquetable.bdd;

import java.lang.ref.Reference;
import java.lang.ref.ReferenceQueue;
import java.lang.ref.WeakReference;
import java.util.ArrayDeque;
import java.util.Deque;
import java.util.Objects;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedDeque;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.atomic.LongAdder;
import java.util.function.Consumer;
import org.sosy_lab.pjbdd.node.BDD;
import org.sosy_lab.pjbdd.node.BDDNode;
import org.sosy_lab.pjbdd.util.HashCodeGenerator;
import org.sosy_lab.pjbdd.util.reference.ComparableWeakBDDReference;
import org.sosy_lab.pjbdd.util.reference.ReclaimedReferenceCleaningThread;

/**
 * {@link UniqueTable} implementation which uses a {@link ConcurrentMap} as backing concurrent
 * collection. The backing map uses {@link Integer} keys and {@link Deque} values. The key {@link
 * Integer} corresponds to {@link BDD} hash codes. All {@link BDD} with equal hashcode stored as
 * {@link ComparableWeakBDDReference} in the conforming {@link Deque}.
 *
 * @author Stephan Holzner
 * @see UniqueTable
 * @see ComparableWeakBDDReference
 * @see BDD
 * @since 1.0
 */
public class BDDConcurrentWeakHashDeque implements UniqueTable<BDD> {
  /** the concurrent map as backing hash bucket. */
  private final ConcurrentMap<Integer, Deque<WeakReference<? extends BDD>>> weakValuesHashBucket;
  /** reference queue to collect reclaimed nodes. */
  private final ReferenceQueue<BDD> referenceQueue;
  /** current number of node entries. */
  private final LongAdder size;
  /** logical zero/false representation. */
  private BDD zero;
  /** logical one/true representation. */
  private BDD one;
  /** factory for bdd creations. */
  private final BDD.Factory<BDD> factory;
  /** demon thread to cleanup reclaimed entries. */
  private final CleanUpThread cleaner;

  /**
   * Creates new {@link BDDConcurrentWeakHashDeque} instances with given parameters.
   *
   * @param tableSize - backing map's initial size
   * @param parallelism - backing map's concurrency factor
   * @param factory - the factory for node creation
   */
  public BDDConcurrentWeakHashDeque(int tableSize, int parallelism, BDDNode.Factory factory) {
    weakValuesHashBucket = new ConcurrentHashMap<>(tableSize, 0.75f, parallelism);
    referenceQueue = new ReferenceQueue<>();
    size = new LongAdder();
    this.factory = factory;
    one = factory.createTrue();
    zero = factory.createFalse();
    cleaner = new CleanUpThread();
    cleaner.start();
  }

  /**
   * Creates new {@link BDDConcurrentWeakHashDeque} instances with given parameters and a new
   * BDDNode.Factory instance.
   *
   * @param tableSize - backing map's initial size
   * @param parallelism - backing map's concurrency factor
   */
  public BDDConcurrentWeakHashDeque(int tableSize, int parallelism) {
    this(tableSize, parallelism, new BDDNode.Factory());
  }

  /** {@inheritDoc} */
  @Override
  public BDD getOrCreate(BDD low, BDD high, int var) {
    int hash = HashCodeGenerator.generateHashCode(var, low.hashCode(), high.hashCode());
    if (weakValuesHashBucket.containsKey(hash)) {
      synchronized (weakValuesHashBucket.get(hash)) {
        for (Reference<? extends BDD> ref : weakValuesHashBucket.get(hash)) {
          BDD value = ref.get();
          if (value == null) {
            continue;
          }
          if (Objects.equals(value.getHigh(), high)
              && Objects.equals(value.getLow(), low)
              && value.getVariable() == var) {
            return value;
          }
        }
      }
    }
    return add(factory.createNode(var, low, high));
  }

  /**
   * Add {@link BDD} to corresponding hash bucket. Creates bucket if there is no existing.
   *
   * @param node - to add
   * @return the node
   */
  private BDD add(BDD node) {
    if (node.isTrue()) {
      one = node;
    } else if (node.isFalse()) {
      zero = node;
    } else {
      WeakReference<BDD> ref = new ComparableWeakBDDReference(node, referenceQueue);
      weakValuesHashBucket.putIfAbsent(node.hashCode(), new ConcurrentLinkedDeque<>());
      weakValuesHashBucket.get(node.hashCode()).add(ref);
    }
    size.increment();
    return node;
  }

  /** {@inheritDoc} */
  @Override
  public void clear() {
    weakValuesHashBucket.clear();
    size.reset();
  }

  /** {@inheritDoc} */
  @Override
  public BDD getTrue() {
    return one;
  }

  /** {@inheritDoc} */
  @Override
  public BDD getFalse() {
    return zero;
  }

  /** {@inheritDoc} */
  @Override
  public void shutDown() {
    clear();
    cleaner.shutdown();
  }

  /** {@inheritDoc} */
  @Override
  public BDD.Factory<BDD> getFactory() {
    return factory;
  }

  /** {@inheritDoc} */
  @Override
  public void forEach(Consumer<BDD> bddConsumer) {
    weakValuesHashBucket
        .values()
        .stream()
        .flatMap(Deque::stream)
        .map(Reference::get)
        .filter(Objects::nonNull)
        .forEach(bddConsumer);
  }

  /** {@inheritDoc} */
  @Override
  public void rehash(BDD node, int oldHash) {
    if (node.hashCode() != oldHash) {
      for (WeakReference<? extends BDD> next :
          weakValuesHashBucket.getOrDefault(oldHash, new ArrayDeque<>())) {
        if (Objects.equals(next.get(), node)) {
          removeReference(next);
          break;
        }
      }
      add(node);
    }
  }

  /**
   * remove a given reference from unique table.
   *
   * @param reference - to be removed
   */
  private void removeReference(WeakReference<? extends BDD> reference) {
    int hash = reference.hashCode();
    weakValuesHashBucket.getOrDefault(hash, new ArrayDeque<>()).remove(reference);
    size.decrement();
  }

  /**
   * {@link ReclaimedReferenceCleaningThread} sub class to handle garbage collected bdd instances.
   *
   * @author Stephan Holzner
   * @see ReclaimedReferenceCleaningThread
   * @since 1.0
   */
  private class CleanUpThread extends ReclaimedReferenceCleaningThread {

    /** {@inheritDoc} */
    @Override
    protected void deleteReclaimedEntries() throws InterruptedException {
      Reference<? extends BDD> reference = referenceQueue.remove(1000);
      if (reference != null) {
        if (reference instanceof ComparableWeakBDDReference && !isShutdown()) {
          removeReference((ComparableWeakBDDReference) reference);
        }
      }
    }
  }
}
