package org.sosy_lab.pjbdd.uniquetable.bdd;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.stream.IntStream;
import org.sosy_lab.pjbdd.node.BDD;
import org.sosy_lab.pjbdd.node.BDDNode;
import org.sosy_lab.pjbdd.uniquetable.ResizingTable;
import org.sosy_lab.pjbdd.util.PrimeUtils;

/**
 * {@link UniqueTable} implementation which uses {@link BDDResizingUniqueTable} as base with backing
 * arrays. Handles concurrent accesses with a next and a resize {@link Lock} object and an array of
 * type {@link Object}, for synchronized hast segment access.
 *
 * @author Stephan Holzner
 * @see ResizingTable
 * @see BDDResizingUniqueTable
 * @see UniqueTable
 * @since 1.0
 */
public class BDDConcurrentArray extends BDDResizingUniqueTable {
  /**
   * Array of monitor objects to synchronize access for specific hash segments. Array length
   * corresponds parallelism factor.
   */
  private final Object[] lock;
  /** Monitor object synchronizing next index resolving for bdd creations. */
  private final Lock nextLock;
  /** Monitor object synchronizing resizing tasks. */
  private final Lock resizeLock;

  /**
   * Creates new {@link BDDConcurrentArray} instances with default parameters parallelism = 100
   * increase factor = 1 initial size = 1000 factory = new BDDNode.Factory instance.
   */
  public BDDConcurrentArray() {
    this(100, 1, 1000, new BDDNode.Factory());
  }

  /**
   * Creates new {@link BDDConcurrentArray} instances with given parameters.
   *
   * @param parallelism - the parallelism factor, number of maximal concurrent accesses
   * @param increaseFactor - backing array's increase factor
   * @param initialSize - backing array's initial size
   * @param factory - BDDNode.Factory instance for node creations
   */
  public BDDConcurrentArray(
      int parallelism, int increaseFactor, int initialSize, BDDNode.Factory factory) {
    super(factory);
    this.increaseFactor = increaseFactor;
    this.nodeCount.set(PrimeUtils.getGreaterNextPrime(initialSize));
    this.initIntern();
    lock = new Object[parallelism];
    IntStream.range(0, parallelism).forEach(i -> lock[i] = new Object());
    nextLock = new ReentrantLock();
    resizeLock = new ReentrantLock();
  }

  /** {@inheritDoc} */
  @Override
  public BDD getOrCreate(BDD low, BDD high, int var) {

    int hash = hashNode(var, low.hashCode(), high.hashCode());
    boolean tryResize = false;
    synchronized (lock[Math.abs(hash % lock.length)]) {
      // Check for performed resize operations
      if (hash == hashNode(var, low.hashCode(), high.hashCode())) {
        // Look up in existing nodes
        BDD bdd = get(low, high, var);
        if (bdd != null) {
          return bdd;
        }

        int res;
        nextLock.lock();
        try {
          res = getNextFree();
        } finally {
          nextLock.unlock();
        }
        if (res > 1) {
          freeCounter.decrementAndGet();
          return create(low, high, var, res);
        } else {
          tryResize = true;
        }
      }
    }
    if (tryResize) {
      resizeLock.lock();
      try {
        tryResizing();
      } finally {
        resizeLock.unlock();
      }
    }
    return getOrCreate(low, high, var);
  }

  /** Try to resize table. Wait if resize op is already in progress. Collect all locks else. */
  protected void tryResizing() {
    if (checkResize()) { // check if already resized with other task
      recursiveGatherSyncObjectsAndResize(0);
    }
  }

  /**
   * Recursive helper method which collect and locks all monitor objects and performs resize task
   * afterwards.
   *
   * @param i - the recursion step's index
   */
  private void recursiveGatherSyncObjectsAndResize(int i) {
    if (i < lock.length) {
      synchronized (lock[i]) {
        recursiveGatherSyncObjectsAndResize(++i);
      }
    } else {
      resizeTable();
    }
  }

  /** {@inheritDoc} */
  @Override
  protected Lock getNextLock() {
    return nextLock;
  }
}
