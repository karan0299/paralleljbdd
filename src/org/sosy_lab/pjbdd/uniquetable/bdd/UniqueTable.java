package org.sosy_lab.pjbdd.uniquetable.bdd;

import java.util.function.Consumer;
import org.sosy_lab.pjbdd.creator.bdd.BDDCreator;
import org.sosy_lab.pjbdd.node.BDD;

/**
 * Main {@link BDD} based unique table interface defining all unique table methods. Used in {@link
 * BDDCreator} implementations.
 *
 * @param <V> the generic {@link BDD} subtype
 * @author Stephan Holzner
 * @see BDDCreator
 * @see BDD
 * @since 1.0
 */
public interface UniqueTable<V extends BDD> {

  /** Clear all unique table entries. */
  void clear();

  /**
   * Get existing or else create the {@link BDD} with following attributes.
   *
   * @param low - the low branch
   * @param high - the high branch
   * @param var - the bdd variable
   * @return the matching bdd
   */
  V getOrCreate(V low, V high, int var);

  /**
   * Get the logical true {@link BDD} representation.
   *
   * @return logical true representation
   */
  V getTrue();

  /**
   * Get the logical false {@link BDD} representation.
   *
   * @return logical false representation
   */
  V getFalse();

  /** Shutdown unique table: cleanup resources. */
  void shutDown();

  /**
   * Apply function on each bdd in table.
   *
   * @param function - the function
   */
  void forEach(Consumer<V> function);

  /**
   * Swap two variables and update all corresponding bdd.
   *
   * @param upper - the upper variable to be swapped down
   * @param lower - the lower variable to be swapped up
   * @param delegate - the node managing delegate
   */
  default void swap(int upper, int lower, NodeMaker delegate) {
    forEach(
        bdd -> {
          if (bdd.getVariable() == lower) {

            BDD.Factory<BDD> factory = getFactory();

            BDD low = bdd.getLow();
            BDD high = bdd.getHigh();

            if (low.getVariable() == upper || high.getVariable() == upper) {
              BDD low0 = low;
              BDD low1 = low;
              BDD high0 = high;
              BDD high1 = high;

              if (low.getVariable() == upper) {
                low0 = low.getLow();
                low1 = low.getHigh();
              }

              if (high.getVariable() == upper) {
                high0 = high.getLow();
                high1 = high.getHigh();
              }

              low = delegate.makeNode(low0, high0, lower);
              high = delegate.makeNode(low1, high1, lower);
              factory.setVariable(upper, bdd);
              factory.setHigh(high, bdd);
              factory.setLow(low, bdd);

              int oldHash = bdd.hashCode();
              rehash(bdd, oldHash);
            }
          }
        });
  }

  /**
   * Rehash a given node after reorder operations - old hash code needed.
   *
   * @param node - the node to be rehashed
   * @param oldHash - the old hash code
   */
  void rehash(BDD node, int oldHash);

  /**
   * Get the table's bdd node factory.
   *
   * @return the node factory
   */
  BDD.Factory<BDD> getFactory();

  interface NodeMaker {
    BDD makeNode(BDD low, BDD high, int var);
  }
}
