package org.sosy_lab.pjbdd.uniquetable.bdd;

import java.lang.ref.Reference;
import java.lang.ref.ReferenceQueue;
import java.lang.ref.WeakReference;
import java.util.Objects;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedDeque;
import java.util.concurrent.ConcurrentMap;
import java.util.function.Consumer;
import org.sosy_lab.pjbdd.node.BDD;
import org.sosy_lab.pjbdd.node.BDDNode;
import org.sosy_lab.pjbdd.util.reference.ComparableWeakBDDReference;
import org.sosy_lab.pjbdd.util.reference.ReclaimedReferenceCleaningThread;

/**
 * {@link UniqueTable} implementation which uses a {@link ConcurrentMap} as backing concurrent
 * collection. The backing map uses the custom {@link WeakReference} type {@link
 * ComparableWeakBDDReference} for keys and regular type {@link WeakReference} for values.
 *
 * <p>Custom {@link ComparableWeakBDDReference} enables {@link ConcurrentMap#get(Object)} with
 * regular {@link BDD} instances. To avoid overhead due to instantiation for lookup and instant
 * garbage collection on hit,{@link BDDConcurrentWeakHashMap} nodes will be recycled. Therefore
 * {@link BDDConcurrentWeakHashMap} uses Factory.RecyclingNodePool and {@link Factory} as custom
 * BDD.Factory implementation.
 *
 * @author Stephan Holzner
 * @see UniqueTable
 * @see ComparableWeakBDDReference
 * @see BDD
 * @see Factory
 * @since 1.0
 */
public class BDDConcurrentWeakHashMap implements UniqueTable<BDD> {
  /** the backing concurrent map with weak keys and values. */
  private final ConcurrentMap<ComparableWeakBDDReference, WeakReference<BDD>> map;
  /** reference queue to collect reclaimed nodes. */
  private final ReferenceQueue<BDD> referenceQueue;
  /** logical zero/false representation. */
  private final BDD zero;
  /** logical one/true representation. */
  private final BDD one;
  /** custom factory for bdd creations and node recycling. */
  private final BDD.Factory<BDD> factory;

  /** demon thread to cleanup reclaimed entries. */
  private final CleanUpThread cleaner;

  /**
   * Creates new {@link BDDConcurrentWeakHashMap} instances with default parameters parallelism =
   * 100 initial size = 10000.
   */
  public BDDConcurrentWeakHashMap() {
    this(10000, 100);
  }

  /**
   * Creates new {@link BDDConcurrentWeakHashMap} instances with given parameters.
   *
   * @param initialCapacity - backing map's initial size
   * @param parallelism - backing map's concurrency factor
   */
  public BDDConcurrentWeakHashMap(int initialCapacity, int parallelism) {
    this(initialCapacity, parallelism, new Factory());
  }

  /**
   * Creates new {@link BDDConcurrentWeakHashMap} instances with given parameters.
   *
   * @param initialCapacity - backing map's initial size
   * @param parallelism - backing map's concurrency factor
   * @param factory - the node Factory
   */
  public BDDConcurrentWeakHashMap(int initialCapacity, int parallelism, BDD.Factory<BDD> factory) {
    this.map = new ConcurrentHashMap<>(initialCapacity, 0.75f, parallelism);
    this.referenceQueue = new ReferenceQueue<>();
    this.cleaner = new CleanUpThread();
    this.factory = factory;
    this.zero = factory.createFalse();
    this.one = factory.createTrue();
    this.startCleaner();
  }

  /** Start cleanup thread. */
  private void startCleaner() {
    cleaner.start();
  }

  /** {@inheritDoc} */
  @Override
  public void forEach(Consumer<BDD> bddConsumer) {
    map.values().stream().map(Reference::get).filter(Objects::nonNull).forEach(bddConsumer);
  }

  @Override
  public void rehash(BDD node, int oldHash) {
    // no rehash needed with this map type
  }

  /** {@inheritDoc} */
  @Override
  public BDD.Factory<BDD> getFactory() {
    return factory;
  }

  /** {@inheritDoc} */
  @Override
  public BDD getOrCreate(BDD low, BDD high, int var) {
    BDD lookUp = factory.createNode(var, low, high);
    ComparableWeakBDDReference ref = new ComparableWeakBDDReference(lookUp, referenceQueue);
    if (map.containsKey(ref)) {
      BDD res = map.get(ref).get();
      if (factory instanceof Factory) {
        ((Factory) factory).addToPool(lookUp);
      }
      return res;
    }
    if (lookUp.isLeaf()) {
      return lookUp;
    }
    // handle concurrency: BDD may be created with other task
    WeakReference<BDD> refRes = map.putIfAbsent(ref, ref);
    if (refRes == null) {
      return lookUp;
    }
    // handle concurrency: other BDD may already be collected
    BDD res = refRes.get();
    if (res != null) {
      return res;
    }
    return getOrCreate(low, high, var);
  }

  /** {@inheritDoc} */
  @Override
  public BDD getTrue() {
    return one;
  }

  /** {@inheritDoc} */
  @Override
  public BDD getFalse() {
    return zero;
  }

  /** {@inheritDoc} */
  @Override
  public void shutDown() {
    cleaner.shutdown();
    clear();
  }

  /** {@inheritDoc} */
  @Override
  public void clear() {
    map.clear();
  }

  /**
   * {@link ReclaimedReferenceCleaningThread} sub class to handle garbage collected bdd instances.
   *
   * @author Stephan Holzner
   * @see ReclaimedReferenceCleaningThread
   * @since 1.0
   */
  private class CleanUpThread extends ReclaimedReferenceCleaningThread {

    /** {@inheritDoc} */
    @Override
    protected void deleteReclaimedEntries() throws InterruptedException {
      Reference<? extends BDD> sv = referenceQueue.remove(1000);
      if (sv != null) {
        if (sv instanceof ComparableWeakBDDReference) {
          map.remove(sv);
        }
      }
    }
  }

  /**
   * BDDNode.Factory sub class which tries to recycle {@link RecyclingNodePool}s {@link BDDNode}
   * objects before new creation.
   *
   * @author Stephan Holzner
   * @see BDDNode
   * @see RecyclingNodePool
   * @since 1.0
   */
  public static class Factory extends BDDNode.Factory {

    /** recycling pool for object reuse. */
    private final RecyclingNodePool pool = new RecyclingNodePool();

    /** {@inheritDoc} */
    @Override
    public BDD createNode(int var, BDD low, BDD high) {

      BDD node = pool.restore();
      if (node != null) {
        setHigh(high, node);
        setLow(low, node);
        setVariable(var, node);
        return node;
      }

      return super.createNode(var, low, high);
    }

    /**
     * Add node to recycling pool.
     *
     * @param bdd - node to append
     */
    public void addToPool(BDD bdd) {
      pool.add(bdd);
    }

    /**
     * Node pool to recycle for look up only instantiated {@link BDD} objects.
     *
     * @author Stephan Holzner
     * @since 1.0
     */
    private static class RecyclingNodePool {
      /** {@link ConcurrentLinkedDeque} as backing collection. */
      private ConcurrentLinkedDeque<BDD> pool = new ConcurrentLinkedDeque<>();
      /** maximal pool size. */
      private static final int MAX_LENGTH = 100;

      /**
       * Add bdd to pool if pool.size() <= {@link #MAX_LENGTH}.
       *
       * @param bdd - to be recycled
       */
      private void add(BDD bdd) {
        if (pool.size() <= MAX_LENGTH) {
          pool.add(bdd);
        }
      }

      /**
       * Recycle unused node.
       *
       * @return unused node
       */
      private BDD restore() {
        return pool.poll();
      }
    }
  }
}
