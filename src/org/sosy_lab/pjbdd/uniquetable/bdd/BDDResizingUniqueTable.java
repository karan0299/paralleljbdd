package org.sosy_lab.pjbdd.uniquetable.bdd;

import java.lang.ref.ReferenceQueue;
import java.lang.ref.WeakReference;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Consumer;
import java.util.stream.Stream;
import org.sosy_lab.pjbdd.node.BDD;
import org.sosy_lab.pjbdd.node.BDDNode;
import org.sosy_lab.pjbdd.uniquetable.ResizingTable;
import org.sosy_lab.pjbdd.util.HashCodeGenerator;
import org.sosy_lab.pjbdd.util.PrimeUtils;
import org.sosy_lab.pjbdd.util.reference.ReclaimedReferenceCleaningThread;

/**
 * Abstract {@link UniqueTable} implementation as base class for implementations with backing arrays
 * of {@link WeakBDDReference}s. This class does not handle concurrent accesses.
 *
 * <p>Subclasses: {@link BDDConcurrentArray}, {@link BDDLockOnWriteArray}
 *
 * @author Stephan Holzner
 * @see ResizingTable
 * @see BDDConcurrentArray
 * @see BDDLockOnWriteArray
 * @see WeakBDDReference
 * @see UniqueTable
 * @since 1.0
 */
public abstract class BDDResizingUniqueTable extends ResizingTable implements UniqueTable<BDD> {

  /**
   * threshold for resizing operations: if table contains more than 'ONLY_CLEAN_THRESHOLD's
   * percentage of recycled elements, there will be no resize but clean operation.
   */
  private static final double ONLY_CLEAN_THRESHOLD = 0.2;

  /** count of garbage collected table entries. */
  protected final AtomicInteger collectedCount;

  /** {@link ReferenceQueue} for weak references. */
  protected final ReferenceQueue<BDD> referenceQueue;

  /** reference cleaning thread. */
  protected final CleanUpThread cleaner;
  /** underlying unique table. */
  protected WeakBDDReference[] table;
  /** underlying hash table. */
  protected int[] hashTable;
  /** underlying next table. */
  protected int[] nextTable;
  /** logical one/true representation. */
  protected BDD one;
  /** logical zero/false representation. */
  protected BDD zero;
  /** factory for node creations. */
  protected final BDDNode.Factory factory;

  /**
   * Creates a new {@link BDDResizingUniqueTable} with specified node factory.
   *
   * @param factory - the specified node factory
   */
  public BDDResizingUniqueTable(BDDNode.Factory factory) {
    this.cleaner = new CleanUpThread();
    this.collectedCount = new AtomicInteger();
    this.referenceQueue = new ReferenceQueue<>();
    this.factory = factory;
    this.startCleaner();
  }

  /** Start cleanup thread. */
  private void startCleaner() {
    cleaner.start();
  }

  /** {@inheritDoc} */
  @Override
  public void forEach(Consumer<BDD> bddConsumer) {
    Stream.of(table)
        .filter(Objects::nonNull)
        .map(WeakBDDReference::get)
        .filter(Objects::nonNull)
        .forEach(bddConsumer);
  }

  /** Initialize unique table and it's backing arrays. */
  protected void initIntern() {
    this.freeCounter.set(nodeCount.get() - 2);
    this.freeCounter.set(2);
    one = factory.createTrue();
    zero = factory.createFalse();
    table = new WeakBDDReference[this.nodeCount.get()];
    nextTable = new int[this.nodeCount.get()];
    hashTable = new int[this.nodeCount.get()];

    for (int n = 0; n < this.nodeCount.get(); n++) {
      setNext(n, n + 1);
    }
    setNext(this.nodeCount.get() - 1, 0);
    nextFree.set(2);
    freeCounter.set(this.nodeCount.get() - 2);
  }

  /**
   * Set index's next node with equal hash code.
   *
   * @param index - the root node's index
   * @param next - the next node's index with equal hash code
   */
  protected void setNext(int index, int next) {
    if (index != next) {
      nextTable[index] = next;
    }
  }

  /** {@inheritDoc} */
  @Override
  protected int getNext(int index) {
    return nextTable[index];
  }

  /** {@inheritDoc} */
  @Override
  protected void resizeTable() {
    int oldSize = nodeCount.get();
    int newSize = nodeCount.get();

    if (collectedCount.get() >= nodeCount.get() * ONLY_CLEAN_THRESHOLD) {
      doResize(oldSize, newSize);
    } else {
      super.resizeTable();
    }
    collectedCount.set(0);
  }

  /**
   * Creates a new {@link BDD} with given parameters and corresponding table entries. Method do not
   * handle concurrent accesses!
   *
   * @param low - the given low branch
   * @param high - the given high branch
   * @param var - the given bdd variable
   * @param res - the new nodes array index
   * @return the created {@link BDD}
   */
  protected BDD create(BDD low, BDD high, int var, int res) {
    freeCounter.decrementAndGet();
    int hash = hashNode(var, low.hashCode(), high.hashCode());
    int nodeForhash = getNodeWithHash(hash);
    setNext(res, nodeForhash);
    setHash(hash, res);
    BDD result = factory.createNode(var, low, high);
    table[res] = new WeakBDDReference(result, referenceQueue);
    return result;
  }

  /**
   * Get the index of the {@link BDD} with a given hashcode.
   *
   * @param hash - the given hashcode
   * @return node's index with given hashcode
   */
  protected int getNodeWithHash(int hash) {
    return hashTable[hash];
  }

  /**
   * Set the index of the {@link BDD} with a given hashcode.
   *
   * @param hash - the node's hashcode
   * @param index - the node's index
   */
  protected void setHash(int hash, int index) {
    hashTable[hash] = index;
  }

  /**
   * Calculates hashcode for given input triple.
   *
   * @param lvl - a node's level
   * @param low - a node's low branch
   * @param high - a node's high branch
   * @return hashcode for given input triple
   */
  protected int hashNode(int lvl, int low, int high) {
    return Math.abs(HashCodeGenerator.generateHashCode(lvl, low, high) % nodeCount.get());
  }

  /** Shutdown Reference cleaning {@link CleanUpThread} demon thread. */
  protected void shutdownCleaner() {
    this.cleaner.shutdown();
  }

  /** {@inheritDoc} */
  @Override
  protected void doResize(int oldSize, int newSize) {

    newSize = PrimeUtils.getLowerNextPrime(newSize);

    if (oldSize < newSize) {

      int[] newHashTable = new int[newSize];
      int[] newNextTable = new int[newSize];
      WeakBDDReference[] newUniqueTable = new WeakBDDReference[newSize];

      System.arraycopy(table, 0, newUniqueTable, 0, table.length);
      System.arraycopy(hashTable, 0, newHashTable, 0, hashTable.length);
      System.arraycopy(nextTable, 0, newNextTable, 0, nextTable.length);
      table = newUniqueTable;
      nextTable = newNextTable;
      hashTable = newHashTable;

      nodeCount.set(newSize);
    }

    for (int n = 0; n < oldSize; n++) {
      setHash(n, 0);
    }

    for (int n = oldSize; n < nodeCount.get(); n++) {
      setNext(n, n + 1);
    }
    setNext(nodeCount.get() - 1, nextFree.get());
    nextFree.set(oldSize);
    freeCounter.set(nodeCount.get() - oldSize);
    rehash();
  }

  /** Rehash table entries after resize task. (Hash value changes with table size). */
  private void rehash() {
    nextFree.set(0);
    freeCounter.set(0);

    for (int n = nodeCount.get() - 1; n >= 2; n--) {
      if (table[n] != null) {
        BDD bdd = table[n].get();
        if (bdd != null) {
          int hash = hashNode(bdd.getVariable(), bdd.getLow().hashCode(), bdd.getHigh().hashCode());
          setNext(n, getNodeWithHash(hash));
          setHash(hash, n);
          continue;
        }
      }
      table[n] = null;
      setNext(n, nextFree.get());
      nextFree.set(n);
      freeCounter.incrementAndGet();
    }
  }

  /**
   * Lookup a {@link BDD} node with specified parameters.
   *
   * @param low - the nodes low branch
   * @param high - the nodes high branch
   * @param var - the nodes bdd variable
   * @return the matching node or <code>null</code> else
   */
  protected BDD get(BDD low, BDD high, int var) {
    int hash = hashNode(var, low.hashCode(), high.hashCode());
    int res = getNodeWithHash(hash);

    while (res != 0) {
      if (table[res] == null) {
        res = getNext(res);
        continue;
      }
      BDD bdd = table[res].get();
      if (bdd == null) {
        res = getNext(res);
        continue;
      }
      if (bdd.getVariable() == var && bdd.getLow() == low && bdd.getHigh() == high) {
        return bdd;
      }

      res = getNext(res);
    }
    return null;
  }

  /** {@inheritDoc} */
  @Override
  public void rehash(BDD node, int oldHash) {
    int newHash = hashNode(node.getVariable(), node.getLow().hashCode(), node.getHigh().hashCode());
    oldHash = Math.abs(oldHash % nodeCount.get());
    if (oldHash != newHash) {

      int index = getNodeWithHash(oldHash);
      BDD res = table[index].get();
      int previous = index;
      while (res != node) {
        previous = index;
        index = getNext(index);
        res = table[index].get();
      }
      if (previous == index) { // bdd is result of getNodeWithHash
        setHash(oldHash, getNext(index));
      } else {
        setNext(previous, getNext(index));
      }
      setNext(index, getNodeWithHash(newHash));
      setHash(newHash, index);
    }
  }

  /** {@inheritDoc} */
  @Override
  protected int getNextFree() {
    int next = nextFree.get();
    nextFree.set(getNext(next));
    return next;
  }

  /** {@inheritDoc} */
  @Override
  public BDD getTrue() {
    return one;
  }

  /** {@inheritDoc} */
  @Override
  public BDD getFalse() {
    return zero;
  }

  /** {@inheritDoc} */
  @Override
  public void shutDown() {
    shutdownCleaner();
    clear();
  }

  /** {@inheritDoc} */
  @Override
  public BDD.Factory<BDD> getFactory() {
    return factory;
  }

  /** {@inheritDoc} */
  @Override
  public void clear() {
    table = null;
    hashTable = null;
    nextTable = null;
  }

  /** Wrapper class for WeakReference of {@link BDD}. */
  protected static class WeakBDDReference extends WeakReference<BDD> {
    WeakBDDReference(BDD referent, ReferenceQueue<BDD> queue) {
      super(referent, queue);
    }
  }

  /**
   * {@link ReclaimedReferenceCleaningThread} sub class to handle.
   *
   * @author Stephan Holzner
   * @see ReclaimedReferenceCleaningThread
   * @since 1.0
   */
  class CleanUpThread extends ReclaimedReferenceCleaningThread {

    /** {@inheritDoc} */
    @Override
    protected void deleteReclaimedEntries() throws InterruptedException {
      if (referenceQueue.remove(1000) != null) {
        // TODO correct removal of elements in here atm collected elements will be removed with
        // resize
        // Problem: Concurrency issues in hash chain
        collectedCount.incrementAndGet();
      }
    }
  }
}
