package org.sosy_lab.pjbdd.uniquetable.bdd;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import org.sosy_lab.pjbdd.node.BDD;
import org.sosy_lab.pjbdd.node.BDDNode;
import org.sosy_lab.pjbdd.uniquetable.ResizingTable;
import org.sosy_lab.pjbdd.util.PrimeUtils;

/**
 * {@link UniqueTable} implementation which uses {@link BDDResizingUniqueTable} as base with backing
 * arrays. Handles concurrent accesses with a {@link ReadWriteLock} object. Therefore there can be
 * multiple Read(get) actions in parallel but no Write(create or resize) action. A Write(create or
 * resize) action locks the unique table.
 *
 * @author Stephan Holzner
 * @see ResizingTable
 * @see BDDResizingUniqueTable
 * @see UniqueTable
 * @since 1.0
 */
public class BDDLockOnWriteArray extends BDDResizingUniqueTable {
  /** The read write monitor object. */
  private final ReadWriteLock rwLock;

  /**
   * Creates new {@link BDDLockOnWriteArray} instances with default parameters increase factor = 1
   * initial size = 1000 factory = new BDDNode.Factory instance.
   */
  public BDDLockOnWriteArray() {
    this(1, 1000, new BDDNode.Factory());
  }

  /**
   * Creates new {@link BDDLockOnWriteArray} instances with given parameters.
   *
   * @param increaseFactor - backing array's increase factor
   * @param initialSize - backing array's initial size
   * @param factory - BDDNode.Factory instance for node creations
   */
  public BDDLockOnWriteArray(int increaseFactor, int initialSize, BDDNode.Factory factory) {
    super(factory);
    this.increaseFactor = increaseFactor;
    this.nodeCount.set(PrimeUtils.getGreaterNextPrime(initialSize));
    this.initIntern();
    rwLock = new ReentrantReadWriteLock();
  }

  /** {@inheritDoc} */
  @Override
  public BDD getOrCreate(BDD low, BDD high, int var) {
    BDD bdd;
    rwLock.readLock().lock();
    try {
      bdd = get(low, high, var);
    } finally {
      rwLock.readLock().unlock();
    }
    if (bdd != null) {
      return bdd;
    }

    rwLock.writeLock().lock();
    try {
      bdd = get(low, high, var);
      if (bdd != null) {
        return bdd;
      }
      if (checkResize()) {
        resizeTable();
      }
      int res = getNextFree();
      return create(low, high, var, res);
    } finally {
      rwLock.writeLock().unlock();
    }
  }

  /** {@inheritDoc} */
  @Override
  protected Lock getNextLock() {
    return rwLock.writeLock();
  }
}
