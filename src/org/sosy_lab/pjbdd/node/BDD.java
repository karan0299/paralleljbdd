package org.sosy_lab.pjbdd.node;

/**
 * Main bdd interface defining all bdd methods.
 *
 * @author Stephan Holzner
 * @since 1.0
 */
public interface BDD {

  /**
   * Returns the variable of this bdd.
   *
   * @return the variable
   */
  int getVariable();

  /**
   * Returns the low branch of this bdd.
   *
   * @return the low branch
   */
  BDD getLow();

  /**
   * Returns the high branch of this bdd.
   *
   * @return the high branch
   */
  BDD getHigh();

  /**
   * Indicates if this bdd is a leaf bdd. Default implementation returns {@link #isTrue()} ||
   * {@link #isFalse()}.
   *
   * @return true if this bdd either corresponds to the logical true or the logical false
   *         representation - else return false
   */
  default boolean isLeaf() {
    return isTrue() || isFalse();
  }

  /**
   * Indicates if this bdd is the logical true representation.
   *
   * @return true if this bdd corresponds to the logical true representation - else return false
   */
  boolean isTrue();

  /**
   * Indicates if this bdd is the logical false representation.
   *
   * @return true if this bdd corresponds to the logical false representation - else return false
   */
  boolean isFalse();

  /** {@inheritDoc} */
  @Override
  boolean equals(Object o);

  /** {@inheritDoc} */
  @Override
  int hashCode();

  /**
   * Main bdd factory interface defining all bdd creation or modification methods. The only use case
   * to modify a bdd is while reordering or node recycling in special cases.
   *
   * @param <V> the most general BDD type this factory will be able to create BDDs for.
   * @author Stephan Holzner
   * @see BDD
   * @since 1.0
   */
  interface Factory<V extends BDD> {

    /**
     * Creates a new bdd object with specified variable, low and high branch.
     *
     * @param lvl  - the specified variable
     * @param low  - the specified low branch
     * @param high - the specified high branch
     * @return the new bdd object
     */
    V createNode(int lvl, V low, V high);

    /**
     * Creates logical true representation.
     *
     * @return the logical true representation
     */
    V createTrue();

    /**
     * Creates logical false representation.
     *
     * @return the logical false representation
     */
    V createFalse();

    /**
     * Change a given node's low branch. This method should only be used while reordering or
     * recycling!
     *
     * @param low  - the new low branch
     * @param node - the given node
     */
    void setLow(V low, V node);

    /**
     * Change a given node's high branch This method should only be used while reordering or
     * recycling!
     *
     * @param high - the new high branch
     * @param node - the given node
     */
    void setHigh(V high, V node);

    /**
     * Change a given node's variable This method should only be used while reordering or recycling!
     *
     * @param variable - the new variable
     * @param node     - the given node
     */
    void setVariable(int variable, V node);
  }
}
