package org.sosy_lab.pjbdd.node.manager.bdd;

import org.sosy_lab.pjbdd.node.BDD;
import org.sosy_lab.pjbdd.uniquetable.bdd.UniqueTable;

/**
 * Main and simple {@link NodeManager} implementation for ZDD creations. Overrides {@link
 * BDDNodeManager#makeNode(BDD, BDD, int)} of {@link BDDNodeManager}
 *
 * @author Stephan Holzner
 * @see NodeManager
 * @see BDDNodeManager
 * @since 1.0
 */
public class ZDDNodeManager extends BDDNodeManager {

  /**
   * Creates new node manager instance.
   *
   * @param uniqueTable - the uniquetable to use
   */
  public ZDDNodeManager(UniqueTable<BDD> uniqueTable) {
    super(uniqueTable);
  }

  /** {@inheritDoc} */
  @Override
  public BDD makeNode(BDD low, BDD high, int var) {
    if (high.isFalse()) {
      return low;
    }
    if (!checkLvl(var)) {
      setVarCount(var + 1);
    }

    return uniqueTable.getOrCreate(low, high, var);
  }
}
