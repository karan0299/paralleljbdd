package org.sosy_lab.pjbdd.node.manager.bdd;

import com.google.common.primitives.Ints;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.IntStream;
import org.sosy_lab.pjbdd.node.BDD;
import org.sosy_lab.pjbdd.uniquetable.bdd.UniqueTable;

/**
 * Main and simple {@link NodeManager} implementation.
 *
 * @author Stephan Holzner
 * @see NodeManager
 * @see BDD
 * @since 1.0
 */
public class BDDNodeManager implements NodeManager {

  /** number of defined BDD variables. */
  protected int varCount;

  /** Backing arrays for storing level and variable relation. */
  protected int[] levelVar;

  protected int[] varLevel;

  /**
   * Set of defined {@link BDD} variables. Prevents variable deletion from unique table (due to
   * {@link java.lang.ref.WeakReference}).
   */
  protected Set<BDD> varSet;

  /** Backing unique table for bdd storage. */
  protected final UniqueTable<BDD> uniqueTable;

  public BDDNodeManager(UniqueTable<BDD> uniqueTable) {
    this.uniqueTable = uniqueTable;
  }

  /** {@inheritDoc} */
  @Override
  public int var(int level) {
    return levelVar[level];
  }

  /** {@inheritDoc} */
  @Override
  public int setVarCount(int count) {
    if (count < 1 || count <= varCount) {
      return varCount;
    }
    int oldCount = varCount;
    varCount = count;

    if (varSet == null) {
      varSet = Collections.synchronizedSet(new HashSet<>());
      levelVar = new int[varCount];
      varLevel = new int[varCount];
    } else {
      int[] newLevelVar = new int[varCount];
      int[] newVarLevel = new int[varCount];
      System.arraycopy(levelVar, 0, newLevelVar, 0, levelVar.length);
      System.arraycopy(varLevel, 0, newVarLevel, 0, levelVar.length);
      levelVar = newLevelVar;
      varLevel = newVarLevel;
    }

    for (int i = oldCount; i < count; i++) {
      makeVariable(i);
    }
    return varCount;
  }

  /** {@inheritDoc} */
  @Override
  public int getNext(int lvl) {
    if (lvl >= levelVar.length) {
      throw new IndexOutOfBoundsException("no such variable");
    }
    return levelVar[lvl + 1];
  }

  /** {@inheritDoc} */
  @Override
  public int level(int variable) {
    return (variable < 0) ? varCount : varLevel[variable];
  }

  /** {@inheritDoc} */
  @Override
  public int getVarCount() {
    return varCount;
  }

  /** {@inheritDoc} */
  @Override
  public int[] getCurrentOrdering() {
    return Arrays.copyOf(levelVar, levelVar.length);
  }

  /** {@inheritDoc} */
  @Override
  public boolean checkLvl(int level) {
    return (level < varCount);
  }

  /** {@inheritDoc} */
  @Override
  public void setVarOrder(List<Integer> pOrder) {
    // Fill up existing order
    int max = Collections.max(pOrder);
    if (!checkLvl(max)) {
      setVarCount(max);
    }
    // actually reorder
    IntStream.range(0, pOrder.size())
        .forEach(
            i -> {
              if (!Objects.equals(pOrder.get(i), levelVar[i])) {
                swapVarToLevel(pOrder.get(i), i);
              }
            });
  }
  /** {@inheritDoc} */
  @Override
  public int topVar(int[] levels) {
    return var(Ints.min(levels));
  }
  /** {@inheritDoc} */
  @Override
  public BDD makeNode(BDD low, BDD high, int var) {
    if (low == high) {
      return low;
    }
    if (!checkLvl(var)) {
      setVarCount(var + 1);
    }

    return uniqueTable.getOrCreate(low, high, var);
  }
  /** {@inheritDoc} */
  @Override
  public BDD getFalse() {
    return uniqueTable.getFalse();
  }
  /** {@inheritDoc} */
  @Override
  public BDD getTrue() {
    return uniqueTable.getTrue();
  }
  /** {@inheritDoc} */
  @Override
  public void shutdown() {
    varLevel = null;
    levelVar = null;
    varSet.clear();
    uniqueTable.shutDown();
  }

  /**
   * Swaps a specific variable up or down to a certain level.
   *
   * @param var - the variable to be swapped
   * @param level - the target level
   */
  private void swapVarToLevel(int var, int level) {
    int levelOfVar = varLevel[var];
    while (levelOfVar != level) {
      if (levelOfVar < level) {
        swapLevel(levelOfVar + 1, levelOfVar++);
      } else {
        swapLevel(levelOfVar, --levelOfVar);
      }
    }
  }

  /**
   * Swaps variables by given variable levels.
   *
   * @param levelA - getIf variable's level
   * @param levelB - getThen variable's level
   */
  private void swapLevel(int levelA, int levelB) {
    int varA = levelVar[levelA];
    int varB = levelVar[levelB];
    varLevel[varA] = levelB;
    varLevel[varB] = levelA;
    levelVar[levelB] = varA;
    levelVar[levelA] = varB;
    uniqueTable.swap(varA, varB, this::makeNode);
  }

  /** {@inheritDoc} */
  @Override
  public BDD makeNext() {
    return makeVariable(varCount + 1);
  }

  /**
   * Actually creates the variable and its negation as {@link BDD}.
   *
   * @param var - the variable to be created
   * @return the created variable as {@link BDD}
   */
  private BDD makeVariable(int var) {
    BDD posVar = makeNode(getFalse(), getTrue(), var);
    varSet.add(posVar);
    levelVar[var] = var;
    varLevel[var] = var;

    BDD negVar = makeNode(getTrue(), getFalse(), var);
    varSet.add(negVar);
    return posVar;
  }
}
