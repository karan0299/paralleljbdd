package org.sosy_lab.pjbdd.node.manager.bdd;

import java.util.List;
import org.sosy_lab.pjbdd.creator.bdd.BDDCreator;
import org.sosy_lab.pjbdd.node.BDD;

/**
 * interface for {@link BDD} based variable management. Used as {@link BDDCreator} component.
 *
 * @author Stephan Holzner
 * @since 1.0
 */
public interface NodeManager {

  /**
   * Get the bdd variable at given level.
   *
   * @param level - given level
   * @return variable for given level
   */
  int var(int level);

  /**
   * set variable count and create bdd variables.
   *
   * @param count - variable count
   * @return max level
   */
  int setVarCount(int count);

  /**
   * get a given level's next greater level.
   *
   * @param lvl - old level
   * @return next level
   */
  int getNext(int lvl);

  /**
   * Get the level of a given bdd variable.
   *
   * @param variable - given variable
   * @return variable's level
   */
  int level(int variable);

  /**
   * get number of created bdd variables.
   *
   * @return the number of created bdd variables
   */
  int getVarCount();

  /**
   * Get the current bdd variable ordering.
   *
   * @return the current bdd variable ordering
   */
  int[] getCurrentOrdering();

  /**
   * make next bdd variable.
   *
   * @return created variable as {@link BDD}
   */
  BDD makeNext();

  /**
   * Check if the given level already exists.
   *
   * @param level - the level to be checked
   * @return <code>true</code> if the level exists, <code>false</code> else
   */
  boolean checkLvl(int level);

  /**
   * Change the current variable ordering and reorder bdd.
   *
   * @param pOrder - the new order
   */
  void setVarOrder(List<Integer> pOrder);

  /**
   * find a various number of levels' topmost variable.
   *
   * @param levels - variable amount of levels
   * @return topmost variable
   */
  int topVar(int[] levels);

  /**
   * Look up for existing node with given properties or creates a new bdd node.
   *
   * @param low - the low branch
   * @param high - the high branch
   * @param var - the variable
   * @return - existing or created node
   */
  BDD makeNode(BDD low, BDD high, int var);

  /**
   * get logical false representation.
   *
   * @return false bdd
   */
  BDD getFalse();
  /**
   * get logical true representation.
   *
   * @return true bdd
   */
  BDD getTrue();

  /** Shutdown manager and corresponding components. */
  void shutdown();
}
