package org.sosy_lab.pjbdd.node;

import org.sosy_lab.pjbdd.util.HashCodeGenerator;

/**
 * Main {@link BDD} implementation used in most regular bdd boolean_operations implementations.
 *
 * @author Stephan Holzner
 * @see BDD
 * @since 1.0
 */
public class BDDNode implements BDD {

  /** a bdd nodes low child. */
  private BDD low;
  /** a bdd nodes high child. */
  private BDD high;
  /** a bdd nodes variable. */
  private int var;
  /** a bdd nodes hashcode, only calculated once cause recursive calculation required. */
  private int hashCode;

  /**
   * Creates a new {@link BDDNode} with specified variable, low and high branch. Constructor is
   * protected because it should not be called outside {@link BDDNode.Factory}.
   *
   * @param low - the specified low branch
   * @param high - the specified high branch
   * @param var - the specified variable
   * @see BDDNode.Factory
   */
  private BDDNode(BDD low, BDD high, int var) {
    this.low = low;
    this.high = high;
    this.var = var;
    rehash();
  }

  /** {@inheritDoc} */
  @Override
  public int getVariable() {
    return var;
  }

  /** {@inheritDoc} */
  @Override
  public BDD getLow() {
    if (isLeaf()) {
      return this;
    }
    return low;
  }

  /** {@inheritDoc} */
  @Override
  public BDD getHigh() {
    if (isLeaf()) {
      return this;
    }
    return high;
  }

  /** {@inheritDoc} */
  @Override
  public boolean isTrue() {
    return var == -1;
  }

  /** {@inheritDoc} */
  @Override
  public boolean isFalse() {
    return var == -2;
  }

  /** {@inheritDoc} */
  @Override
  public boolean equals(Object o) {
    if (!(o instanceof BDD)) {
      return false;
    }
    BDD other = (BDD) o;
    if (other.isTrue() && isTrue()) {
      return true;
    }
    if (other.isFalse() && isFalse()) {
      return true;
    }
    if (other.getVariable() != getVariable()) {
      return false;
    }
    return other.getHigh() == getHigh() && other.getLow() == getLow();
  }

  /** {@inheritDoc} */
  @Override
  public int hashCode() {
    return hashCode;
  }

  /** Recalculate nodes hashcode after changes due to node recycling or bdd reordering. */
  protected void rehash() {
    hashCode =
        (var < 0)
            ? Math.abs(var)
            : HashCodeGenerator.generateHashCode(
                getVariable(), getLow().hashCode(), getHigh().hashCode());
  }

  /**
   * Main BDD.Factory implementation used in most regular bdd boolean_operations implementations.
   * Creates {@link BDD}s from type {@link BDDNode}.
   *
   * @author Stephan Holzner
   * @see BDD.Factory
   * @see BDD
   * @see BDDNode
   * @since 1.0
   */
  public static class Factory implements BDD.Factory<BDD> {

    /** The logical true leaf representation. */
    private BDDNode one;
    /** The logical false leaf representation. */
    private BDDNode zero;

    /** {@inheritDoc} */
    @Override
    public BDD createNode(int var, BDD low, BDD high) {
      return new BDDNode(low, high, var);
    }

    /** {@inheritDoc} */
    @Override
    public BDD createTrue() {
      if (one == null) {
        one = new BDDNode(null, null, -1);
      }
      return one;
    }

    /** {@inheritDoc} */
    @Override
    public BDD createFalse() {
      if (zero == null) {
        zero = new BDDNode(null, null, -2);
      }
      return zero;
    }

    /** {@inheritDoc} */
    @Override
    public void setLow(BDD low, BDD node) {
      if (node instanceof BDDNode) {
        ((BDDNode) node).low = low;
        ((BDDNode) node).rehash();
      }
    }

    /** {@inheritDoc} */
    @Override
    public void setHigh(BDD high, BDD node) {
      if (node instanceof BDDNode) {
        ((BDDNode) node).high = high;
        ((BDDNode) node).rehash();
      }
    }

    /** {@inheritDoc} */
    @Override
    public void setVariable(int variable, BDD node) {
      if (node instanceof BDDNode) {
        ((BDDNode) node).var = variable;
        ((BDDNode) node).rehash();
      }
    }
  }
}
