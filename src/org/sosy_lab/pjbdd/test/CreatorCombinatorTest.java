package org.sosy_lab.pjbdd.test;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameter;
import org.junit.runners.Parameterized.Parameters;
import org.sosy_lab.pjbdd.Builders;
import org.sosy_lab.pjbdd.creator.bdd.BDDBuilder;
import org.sosy_lab.pjbdd.creator.bdd.Creator;

/**
 * Abstract test class used as base class to perform a boolean operation test on all known creators.
 *
 * @author Stephan Holzner
 * @since 1.0
 */
@RunWith(Parameterized.class)
public class CreatorCombinatorTest {

  /** List with creator instances used to perform test for each instance. */
  @Parameters(name = "creator= {0}, ct={2}, table={1}")
  public static Collection<Object[]> data() {
    List<Object[]> creators = new ArrayList<>();
    for (CreatorType ct : CreatorType.values()) {
      if (ct == CreatorType.ConcurrentInt || ct == CreatorType.SerialInt) {
        creators.add(
            new Object[] {
              resolveCreator(ct, UniqueTableType.ResizingArray), UniqueTableType.ResizingArray, ct,
            });
      } else {
        for (UniqueTableType ut : UniqueTableType.values()) {
          creators.add(
              new Object[] {
                resolveCreator(ct, ut), ut, ct,
              });
        }
      }
    }
    return creators;
  }

  @Parameter(2)
  public CreatorType ct;

  @Parameter(0)
  public Creator creator;

  @Parameter(1)
  public UniqueTableType uniqueTableType;

  /** Perform class specific test for specific {@link Creator} and {@link UniqueTableType}. */
  @Test
  public void test() {}

  /**
   * Initialize scenario for specific combination of queens N CreatorType and UniqueTableType.
   *
   * @param ct - the UniqueTableType
   * @param ut - the CreatorType
   * @return the resolved creator uniquetable combination
   */
  private static Creator resolveCreator(CreatorType ct, UniqueTableType ut) {

    Creator creator;
    if (ct == CreatorType.ConcurrentInt) {
      creator = Builders.newIntBuilder().setSelectedThreads(4).build();
    } else if (ct == CreatorType.SerialInt) {
      creator = Builders.newIntBuilder().setSelectedThreads(1).build();
    } else {
      BDDBuilder creatorBuilder = Builders.newBDDBuilder();
      switch (ut) {
        case HashSet:
          creatorBuilder.setTableType(Builders.TableType.ConcurrentHashMap);
          break;
        case HashBucket:
          creatorBuilder.setTableType(Builders.TableType.ConcurrentHashBucket);
          break;
        case ResizingArray:
          creatorBuilder.setTableType(Builders.TableType.Array);
          break;
        case RWLockArray:
          creatorBuilder.setTableType(Builders.TableType.RWLockArray);
          break;
        default:
          throw new IllegalArgumentException("Unknown Uniquetable type");
      }

      switch (ct) {
        case ForkJoin:
          creator =
              creatorBuilder
                  .setUseApply(false)
                  .setParallelizationType(Builders.ParallelizationType.FORK_JOIN)
                  .build();
          break;
        case ApplyOp:
          creator =
              creatorBuilder
                  .setUseApply(true)
                  .setParallelizationType(Builders.ParallelizationType.FORK_JOIN)
                  .build();
          break;
        case SerialApply:
          creator =
              creatorBuilder
                  .setUseApply(true)
                  .setParallelizationType(Builders.ParallelizationType.NONE)
                  .build();
          break;
        case CompletableApply:
          creator =
              creatorBuilder
                  .setUseApply(true)
                  .setParallelizationType(Builders.ParallelizationType.COMPLETABLE_FUTURE)
                  .build();
          break;
        case CompletableFuture:
          creator =
              creatorBuilder
                  .setUseApply(false)
                  .setParallelizationType(Builders.ParallelizationType.COMPLETABLE_FUTURE)
                  .build();
          break;
        case Future:
          creator =
              creatorBuilder
                  .setUseApply(false)
                  .setParallelizationType(Builders.ParallelizationType.FUTURE)
                  .build();
          break;
        case Stream:
          creator =
              creatorBuilder
                  .setUseApply(false)
                  .setParallelizationType(Builders.ParallelizationType.STREAM)
                  .build();
          break;
        case Serial:
          creator =
              creatorBuilder
                  .setUseApply(false)
                  .setParallelizationType(Builders.ParallelizationType.NONE)
                  .build();
          break;
        case Guava:
          creator =
              creatorBuilder
                  .setUseApply(false)
                  .setParallelizationType(Builders.ParallelizationType.GUAVA_FUTURE)
                  .build();
          break;
        default:
          throw new IllegalArgumentException("Unknown Creator type");
      }
    }
    return creator;
  }

  /** enum to resolve creator types. */
  enum CreatorType {
    Future,
    Guava,
    Serial,
    Stream,
    CompletableFuture,
    ForkJoin,
    ApplyOp,
    SerialApply,
    CompletableApply,
    ConcurrentInt,
    SerialInt,
  }

  /** enum to resolve unique table types. */
  public enum UniqueTableType {
    RWLockArray,
    HashBucket,
    HashSet,
    ResizingArray
  }
}
