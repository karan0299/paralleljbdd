package org.sosy_lab.pjbdd.test;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;
import org.sosy_lab.pjbdd.examples.Example;
import org.sosy_lab.pjbdd.examples.NQueens;
import org.sosy_lab.pjbdd.node.BDD;
import org.sosy_lab.pjbdd.util.parser.DotExporter;

public class ReorderTest extends CreatorCombinatorTest {
  @Override
  public void test() {
    BDD bdd1 = creator.makeNode(creator.makeFalse(), creator.makeTrue(), 0);
    BDD bdd2 = creator.makeNode(creator.makeFalse(), creator.makeTrue(), 1);

    BDD orBDD = creator.makeOr(bdd1, bdd2);
    Example example = new NQueens(4, creator);
    example.build();

    List<Integer> newOrder = new ArrayList<>();
    newOrder.add(1);
    newOrder.add(0);
    creator.setVarOrder(newOrder);

    assertEquals(1, orBDD.getVariable());

    String reorderedNQueens = new DotExporter().bddToString(example.solution());
    Example example2 = new NQueens(4, creator);
    example2.build();

    String reorderedNQueens2 = new DotExporter().bddToString(example2.solution());
    assertEquals(reorderedNQueens2, reorderedNQueens);

    creator.shutDown();
  }
}
