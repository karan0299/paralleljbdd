package org.sosy_lab.pjbdd.test;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.sosy_lab.pjbdd.Builders;
import org.sosy_lab.pjbdd.creator.bdd.Creator;
import org.sosy_lab.pjbdd.examples.NQueens;
import org.sosy_lab.pjbdd.node.BDD;
import org.sosy_lab.pjbdd.util.parser.BDDStringExporter;
import org.sosy_lab.pjbdd.util.parser.BDDStringImporter;
import org.sosy_lab.pjbdd.util.parser.DotExporter;
import org.sosy_lab.pjbdd.util.parser.DotImporter;
import org.sosy_lab.pjbdd.util.parser.Exporter;
import org.sosy_lab.pjbdd.util.parser.Importer;

/**
 * Test export and import classes.
 *
 * @author Stephan Holzner
 * @since 1.0
 */
public class ImportExportTest {

  /** Test dot export and dor import. */
  @Test
  public void testDotExport() {
    Creator c =
        Builders.newBDDBuilder()
            .setSelectedCacheSize(1000)
            .setIncreaseFactor(1)
            .setTableSize(10000)
            .setVarCount(100)
            .setParallelism(1000)
            .build();
    NQueens queens = new NQueens(4, c);
    queens.build();
    queens.solve();
    Exporter exporter = new DotExporter();
    String stringBDD = exporter.bddToString(queens.solution());
    // System.out.println(stringBDD);

    Importer importer = new DotImporter(c);
    BDD bdd = importer.bddFromString(stringBDD);
    assertEquals(bdd, queens.solution());
  }

  /** Test custom string representation export and dor import. */
  @Test
  public void testStringExport() {
    Creator c =
        Builders.newBDDBuilder()
            .setSelectedCacheSize(1000)
            .setIncreaseFactor(1)
            .setTableSize(10000)
            .setVarCount(100)
            .setParallelism(1000)
            .build();
    NQueens queens = new NQueens(4, c);
    queens.build();
    queens.solve();
    Exporter exporter = new BDDStringExporter(c);
    String stringBDD = exporter.bddToString(queens.solution());
    // System.out.println(stringBDD);

    Importer importer = new BDDStringImporter(c);
    BDD bdd = importer.bddFromString(stringBDD);
    assertEquals(bdd, queens.solution());
  }
}
