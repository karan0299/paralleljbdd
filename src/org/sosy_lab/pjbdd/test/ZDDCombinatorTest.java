package org.sosy_lab.pjbdd.test;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameter;
import org.junit.runners.Parameterized.Parameters;
import org.sosy_lab.pjbdd.Builders;
import org.sosy_lab.pjbdd.creator.zdd.ZDDBuilder;
import org.sosy_lab.pjbdd.creator.zdd.ZDDCreator;

@RunWith(Parameterized.class)
public class ZDDCombinatorTest {

  /** List with creator instances used to perform test for each instance. */
  @Parameters(name = "creator={0}, table={1}")
  public static Collection<Object[]> data() {
    List<Object[]> creators = new ArrayList<>();
    for (CreatorType ct : CreatorType.values()) {
      for (UniqueTableType ut : UniqueTableType.values()) {
        creators.add(new Object[] {resolveCreator(ut, ct), ut});
      }
    }
    return creators;
  }

  @Parameter(0)
  public ZDDCreator creator;

  @Parameter(1)
  public UniqueTableType uniqueTableType;

  /** Perform class specific test for specific {@link ZDDCreator} and {@link UniqueTableType}. */
  @Test
  public void test() {}

  /**
   * Initialize scenario for specific combination of CreatorType and UniqueTableType.
   *
   * @param ut - the UniqueTableType
   * @param ct - the CreatorType
   * @return the Creator combination
   */
  private static ZDDCreator resolveCreator(UniqueTableType ut, CreatorType ct) {

    ZDDBuilder creatorBuilder = Builders.newZDDBuilder().setVarCount(0);
    switch (ut) {
      case HashSet:
        creatorBuilder.setTableType(Builders.TableType.ConcurrentHashMap);
        break;
      case HashBucket:
        creatorBuilder.setTableType(Builders.TableType.ConcurrentHashBucket);
        break;
      case ResizingArray:
        creatorBuilder.setTableType(Builders.TableType.Array);
        break;
      case RWLockArray:
        creatorBuilder.setTableType(Builders.TableType.RWLockArray);
        break;
      default:
        throw new IllegalArgumentException("Unknown Uniquetable type");
    }
    switch (ct) {
      case Serial:
        return creatorBuilder.setThreads(1).build();
      case Concurrent:
        return creatorBuilder.setThreads(4).build();
    }
    return creatorBuilder.setThreads(1).build();
  }

  /** enum to resolve unique table types. */
  enum UniqueTableType {
    RWLockArray,
    HashBucket,
    HashSet,
    ResizingArray
  }

  /** enum to resolve creator types. */
  enum CreatorType {
    Serial,
    Concurrent
  }
}
