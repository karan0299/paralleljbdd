package org.sosy_lab.pjbdd.algorithm.zdd;

import java.util.Optional;
import org.sosy_lab.pjbdd.algorithm.ManipulatingAlgorithm;
import org.sosy_lab.pjbdd.cache.bdd.Cache;
import org.sosy_lab.pjbdd.creator.bdd.BDDBuilder;
import org.sosy_lab.pjbdd.node.BDD;
import org.sosy_lab.pjbdd.node.manager.bdd.NodeManager;
import org.sosy_lab.pjbdd.node.manager.bdd.ZDDNodeManager;

/**
 * A {@link ZDDAlgorithm} implementation which uses {@link ManipulatingAlgorithm} as base class.
 * Performs single core zdd operations. (But multiple operations can be performed on multiple
 * cores).
 *
 * @author Stephan Holzner
 * @see ZDDAlgorithm
 * @see ManipulatingAlgorithm
 * @since 1.0
 */
public class ZDDSerialAlgorithm extends ManipulatingAlgorithm implements ZDDAlgorithm {

  /**
   * Creates new {@link ZDDSerialAlgorithm} instances with specified parameters.
   *
   * <p>It is strongly recommended to use a {@link BDDBuilder} for instantiation.
   *
   * @param computedTable - the computation cache component
   * @param nodeManager - the node manager
   */
  public ZDDSerialAlgorithm(
      Cache<Integer, Cache.CacheData> computedTable, NodeManager nodeManager) {
    super(computedTable, nodeManager);
    assert (nodeManager instanceof ZDDNodeManager);
  }

  /** {@inheritDoc} */
  @Override
  public BDD subSet1(BDD zdd, BDD var) {
    return unaryShannon(zdd, var, ZddOps.SUB_SET1);
  }

  /**
   * Helper function to chain terminal case and cache checks for subset1 operation.
   *
   * @param zdd - the zdd
   * @param var - the variable
   * @return Optional of subset1 terminal check
   */
  private Optional<BDD> subset1Check(BDD zdd, BDD var) {
    if (level(zdd) > level(var)) {
      return Optional.of(empty());
    } else if (level(zdd) == level(var)) {
      return Optional.of(zdd.getHigh());
    }
    return checkBinaryCache(zdd, var, ZddOps.SUB_SET1.ordinal());
  }

  /** {@inheritDoc} */
  @Override
  public BDD subSet0(BDD zdd, BDD var) {
    return unaryShannon(zdd, var, ZddOps.SUB_SET0);
  }

  /**
   * Helper function to chain terminal case and cache checks for subset0 operation.
   *
   * @param zdd - the zdd
   * @param var - the variable
   * @return Optional of subset0 terminal check
   */
  private Optional<BDD> subset0Check(BDD zdd, BDD var) {
    if (level(zdd) > level(var)) {
      return Optional.of(zdd);
    } else if (level(zdd) == level(var)) {
      return Optional.of(zdd.getLow());
    }
    return checkBinaryCache(zdd, var, ZddOps.SUB_SET0.ordinal());
  }

  /** {@inheritDoc} */
  @Override
  public BDD change(BDD zdd, BDD var) {
    return unaryShannon(zdd, var, ZddOps.CHANGE);
  }

  /**
   * Helper function to chain terminal case and cache checks for change operation.
   *
   * @param zdd - the zdd
   * @param var - the variable
   * @return Optional of change terminal check
   */
  private Optional<BDD> changeCheck(BDD zdd, BDD var) {
    if (level(zdd) > level(var)) {
      return Optional.of(makeNode(empty(), zdd, var.getVariable()));
    } else if (level(zdd) == level(var)) {
      return Optional.of(
          makeNode(/* low= */ zdd.getHigh(), /* high= */ zdd.getLow(), var.getVariable()));
    }
    return checkBinaryCache(zdd, var, ZddOps.CHANGE.ordinal());
  }

  /**
   * Helper method for methods with one argument and a variable, where terminal case shannon
   * expansion can be used.
   *
   * @param zdd - the ZDD node
   * @param var - the variable in {@link BDD}-representation
   * @param op - the operation code
   * @return the shannon expansion of given operation over zdd and var
   */
  protected BDD unaryShannon(BDD zdd, BDD var, ZddOps op) {
    return operationCheck(zdd, var, op)
        .orElseGet(
            () -> {
              BDD low = unaryShannon(zdd.getLow(), var, op);
              BDD high = unaryShannon(zdd.getHigh(), var, op);
              BDD res = makeNode(low, high, zdd.getVariable());
              cacheBinaryItem(zdd, var, op.ordinal(), res);
              return res;
            });
  }

  /** {@inheritDoc} */
  @Override
  public BDD union(BDD f1, BDD f2) {
    return unionCheck(f1, f2)
        .orElseGet(
            () -> {
              BDD res;
              if (level(f1) < level(f2)) {
                res = makeNode(union(f1.getLow(), f2), f1.getHigh(), f1.getVariable());
              } else if (level(f1) == level(f2)) {
                res =
                    makeNode(
                        union(f1.getLow(), f2.getLow()),
                        union(f1.getHigh(), f2.getHigh()),
                        f2.getVariable());
              } else {
                res = makeNode(union(f1, f2.getLow()), f2.getHigh(), f2.getVariable());
              }
              cacheBinaryItem(f1, f2, ZddOps.UNION.ordinal(), res);
              return res;
            });
  }

  /**
   * Helper function to chain terminal case and cache checks for union operation.
   *
   * @param f1 - the getIf zdd argument
   * @param f2 - the getThen zdd argument
   * @return Optional of union terminal check
   */
  protected Optional<BDD> unionCheck(BDD f1, BDD f2) {
    if (f1.equals(empty())) {
      return Optional.of(f2);
    }
    if (f2.equals(empty())) {
      return Optional.of(f1);
    }
    if (f1.equals(f2)) {
      return Optional.of(f1);
    }
    return checkBinaryCache(f1, f2, ZddOps.UNION.ordinal());
  }

  /** {@inheritDoc} */
  @Override
  public BDD difference(BDD f1, BDD f2) {
    return differenceCheck(f1, f2)
        .orElseGet(
            () -> {
              BDD res;
              if (level(f1) < level(f2)) {
                res = makeNode(difference(f1.getLow(), f2), f1.getHigh(), f1.getVariable());
              } else if (level(f1) == level(f2)) {
                res =
                    makeNode(
                        difference(f1.getLow(), f2.getLow()),
                        difference(f1.getHigh(), f2.getHigh()),
                        f2.getVariable());
              } else {
                res = difference(f1, f2.getLow());
              }
              cacheBinaryItem(f1, f2, ZddOps.DIFF.ordinal(), res);
              return res;
            });
  }

  /**
   * Helper function to chain terminal case and cache checks for difference operation.
   *
   * @param f1 - the getIf zdd argument
   * @param f2 - the getThen zdd argument
   * @return Optional of difference terminal check
   */
  protected Optional<BDD> differenceCheck(BDD f1, BDD f2) {
    if (f1.equals(empty())) {
      return Optional.of(empty());
    }
    if (f2.equals(empty())) {
      return Optional.of(f1);
    }
    if (f1.equals(f2)) {
      return Optional.of(empty());
    }
    return checkBinaryCache(f1, f2, ZddOps.DIFF.ordinal());
  }

  /** {@inheritDoc} */
  @Override
  public BDD intersection(BDD f1, BDD f2) {
    return intersectsCheck(f1, f2)
        .orElseGet(
            () -> {
              BDD res;
              if (level(f1) < level(f2)) {
                res = intersection(f1.getLow(), f2);
              } else if (level(f1) == level(f2)) {
                res =
                    makeNode(
                        intersection(f1.getLow(), f2.getLow()),
                        intersection(f1.getHigh(), f2.getHigh()),
                        f2.getVariable());
              } else {
                res = intersection(f1, f2.getLow());
              }
              cacheBinaryItem(f1, f2, ZddOps.INTSEC.ordinal(), res);
              return res;
            });
  }

  /**
   * Helper function to chain terminal case and cache checks for intersects operation.
   *
   * @param f1 - the getIf zdd argument
   * @param f2 - the getThen zdd argument
   * @return Optional of intersects terminal check
   */
  protected Optional<BDD> intersectsCheck(BDD f1, BDD f2) {
    if (f1.equals(empty())) {
      return Optional.of(empty());
    }
    if (f2.equals(empty())) {
      return Optional.of(empty());
    }
    if (f1.equals(f2)) {
      return Optional.of(f1);
    }
    return checkBinaryCache(f1, f2, ZddOps.INTSEC.ordinal());
  }

  /** {@inheritDoc} */
  @Override
  public BDD product(BDD f1, BDD f2) {
    Optional<BDD> check = productCheck(f1, f2);
    if (check.isPresent()) {
      return check.get();
    }
    BDD res;
    if (level(f1) > level(f2)) {
      res = makeNode(product(f1, f2.getLow()), product(f1, f2.getHigh()), f2.getVariable());
    } else if (level(f1) < level(f2)) {
      res = makeNode(product(f1.getLow(), f2), product(f1.getHigh(), f2), f1.getVariable());
    } else {
      BDD high = product(f1.getHigh(), f2.getHigh());
      BDD low = product(f1.getHigh(), f2.getLow());
      res = product(high, low);

      high = product(f1.getLow(), f2.getHigh());
      high = product(res, high);

      low = product(f1.getLow(), f2.getLow());
      res = makeNode(low, high, f1.getVariable());
    }

    cacheBinaryItem(f1, f2, ZddOps.MUL.ordinal(), res);
    return res;
  }

  /**
   * Helper function to chain terminal case and cache checks for product operation.
   *
   * @param f1 - the getIf zdd argument
   * @param f2 - the getThen zdd argument
   * @return Optional of product terminal check
   */
  protected Optional<BDD> productCheck(BDD f1, BDD f2) {
    if (f1.equals(empty()) || f2.equals(empty())) {
      return Optional.of(empty());
    }
    if (f1.equals(base())) {
      return Optional.of(f2);
    }
    if (f2.equals(base())) {
      return Optional.of(f1);
    }
    return checkBinaryCache(f1, f2, ZddOps.MUL.ordinal());
  }

  /** {@inheritDoc} */
  @Override
  public BDD modulo(BDD zdd1, BDD zdd2) {
    return checkBinaryCache(zdd1, zdd2, ZddOps.MOD.ordinal())
        .orElseGet(
            () -> {
              BDD result = difference(zdd1, product(zdd2, difference(zdd1, zdd2)));
              cacheBinaryItem(zdd1, zdd2, ZddOps.MUL.ordinal(), result);
              return result;
            });
  }

  /** {@inheritDoc} */
  @Override
  public BDD division(BDD f1, BDD f2) {
    Optional<BDD> check = divisionCheck(f1, f2);
    if (check.isPresent()) {
      return check.get();
    }
    BDD res;
    if (level(f1) < level(f2)) {
      res = makeNode(division(f1.getLow(), f2), division(f1.getHigh(), f2), f1.getVariable());
    } else {
      res = sequentialDivisionCase(f1, f2);
    }
    cacheBinaryItem(f1, f2, ZddOps.DIV.ordinal(), res);
    return res;
  }

  /**
   * Perform divisions case that can not be performed in parallel.
   *
   * @param f1 - getIf {@link BDD} argument
   * @param f2 - getThen {@link BDD} argument
   * @return (zdd1 { @ literal / } zdd2)
   */
  protected BDD sequentialDivisionCase(BDD f1, BDD f2) {
    BDD res = division(f1.getHigh(), f2.getHigh());
    BDD tmp = f2.getLow();
    if (!res.isFalse() && !tmp.isFalse()) {
      tmp = division(f1.getLow(), tmp);
      res = intersection(tmp, res);
    }
    return res;
  }

  /**
   * Helper function to chain terminal case and cache checks for division operation.
   *
   * @param f1 - the getIf zdd argument
   * @param f2 - the getThen zdd argument
   * @return Optional of division terminal check
   */
  protected Optional<BDD> divisionCheck(BDD f1, BDD f2) {
    if (f1.equals(empty()) || f1.equals(base())) {
      return Optional.of(empty());
    }
    if (f1.equals(f2)) {
      return Optional.of(base());
    }
    if (f2.equals(base())) {
      return Optional.of(f1);
    }
    if (level(f2) < level(f1)) {
      return Optional.of(empty());
    }
    return checkBinaryCache(f1, f2, ZddOps.DIV.ordinal());
  }

  /** {@inheritDoc} */
  @Override
  public BDD exclude(BDD f1, BDD f2) {
    Optional<BDD> check = excludeCheck(f1, f2);
    if (check.isPresent()) {
      return check.get();
    }

    BDD res;
    if (level(f1) > level(f2)) {
      res = exclude(f1, f2.getLow());
    } else if (level(f1) < level(f2)) {
      res = makeNode(exclude(f1.getLow(), f2), exclude(f1.getHigh(), f2), f1.getVariable());
    } else {
      BDD low;
      BDD high;
      if (followLow(f2.getHigh()).equals(base())) {
        high = empty();
      } else {
        high = exclude(f1.getHigh(), f2.getLow());
        low = exclude(f1.getHigh(), f2.getHigh());
        high = intersection(high, low);
      }
      low = exclude(f1.getLow(), f2.getLow());
      res = makeNode(low, high, f1.getVariable());
    }

    cacheBinaryItem(f1, f2, ZddOps.EXCLUDE.ordinal(), res);
    return res;
  }

  /**
   * Helper function to chain terminal case and cache checks for exclude operation.
   *
   * @param f1 - the getIf zdd argument
   * @param f2 - the getThen zdd argument
   * @return Optional of exclude terminal check
   */
  protected Optional<BDD> excludeCheck(BDD f1, BDD f2) {
    if (f1.equals(empty()) || f2.equals(base()) || f1.equals(f2)) {
      return Optional.of(empty());
    }
    if (f1.equals(base()) || f2.equals(empty())) {
      return Optional.of(f1);
    }

    return checkBinaryCache(f1, f2, ZddOps.EXCLUDE.ordinal());
  }
  /** {@inheritDoc} */
  @Override
  public BDD restrict(BDD f1, BDD f2) {
    Optional<BDD> check = restrictCheck(f1, f2);
    if (check.isPresent()) {
      return check.get();
    }

    BDD res;
    if (level(f1) > level(f2)) {
      res = restrict(f1, f2.getLow());
    } else if (level(f1) < level(f2)) {
      res = makeNode(restrict(f1.getLow(), f2), restrict(f1.getHigh(), f2), f1.getVariable());
    } else {
      BDD low = restrict(f1.getHigh(), f2.getLow());
      BDD high = restrict(f1.getHigh(), f2.getHigh());
      high = union(high, low);
      low = restrict(f1.getLow(), f2.getLow());
      res = makeNode(low, high, f1.getVariable());
    }

    cacheBinaryItem(f1, f2, ZddOps.RESTRICT.ordinal(), res);
    return res;
  }
  /** {@inheritDoc} */
  @Override
  public BDD empty() {
    return nodeManager.getFalse();
  }
  /** {@inheritDoc} */
  @Override
  public BDD base() {
    return nodeManager.getTrue();
  }
  /** {@inheritDoc} */
  @Override
  public void shutdown() {
    computedTable.clear();
  }

  /**
   * Helper function to chain terminal case and cache checks for restrict operation.
   *
   * @param f1 - the getIf zdd argument
   * @param f2 - the getThen zdd argument
   * @return Optional of restrict terminal check
   */
  protected Optional<BDD> restrictCheck(BDD f1, BDD f2) {
    if (f1.equals(empty()) || f2.equals(empty())) {
      return Optional.of(empty());
    }
    if (f1.equals(f2)) {
      return Optional.of(f1);
    }
    return checkBinaryCache(f1, f2, ZddOps.RESTRICT.ordinal());
  }

  /**
   * Follow low branch till leaf node is reached.
   *
   * @param bdd - the input node
   * @return leaf of low branch path
   */
  protected BDD followLow(BDD bdd) {
    BDD copy = bdd;
    while (!copy.isLeaf()) {
      copy = copy.getLow();
    }
    return copy;
  }

  /**
   * Apply operation check to avoid lambda terminal check references.
   *
   * @param f1 - the getIf argument
   * @param f2 - the getThen argument
   * @param op - the operation to be applied
   * @return Optional of terminal check f1 'op' f2
   */
  protected Optional<BDD> operationCheck(BDD f1, BDD f2, ZddOps op) {
    switch (op) {
      case UNION:
        return unionCheck(f1, f2);
      case DIFF:
        return differenceCheck(f1, f2);
      case INTSEC:
        return intersectsCheck(f1, f2);
      case MUL:
        return productCheck(f1, f2);
      case EXCLUDE:
        return excludeCheck(f1, f2);
      case RESTRICT:
        return restrictCheck(f1, f2);
      case DIV:
        return divisionCheck(f1, f2);
      case CHANGE:
        return changeCheck(f1, f2);
      case SUB_SET0:
        return subset0Check(f1, f2);
      case SUB_SET1:
        return subset1Check(f1, f2);
      default:
        throw new IllegalArgumentException("Unknown Operation: " + op);
    }
  }

  @Override
  public String toString() {
    return this.getClass().getSimpleName();
  }
}
