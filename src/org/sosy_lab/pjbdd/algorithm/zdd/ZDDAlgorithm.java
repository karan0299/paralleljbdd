package org.sosy_lab.pjbdd.algorithm.zdd;

import org.sosy_lab.pjbdd.node.BDD;

/**
 * Interface for zdd manipulating operations.
 *
 * @author Stephan Holzner
 * @since 1.1
 */
public interface ZDDAlgorithm {

  /**
   * Creates a {@link BDD} representing the high subset of an zdd with restricting a given variable
   * to 1.
   *
   * @param zdd - given zdd
   * @param var - {@link BDD} representation of given variable
   * @return (subset of zdd with var equals ONE)
   */
  BDD subSet1(BDD zdd, BDD var);

  /**
   * Creates a {@link BDD} representing the high subset of an zdd with restricting a given variable
   * to 0.
   *
   * @param zdd - given zdd
   * @param var - {@link BDD} representation of given variable
   * @return (subset of zdd with var equals ZERO)
   */
  BDD subSet0(BDD zdd, BDD var);

  /**
   * Creates a {@link BDD} with inverted variable.
   *
   * @param zdd - given zdd
   * @param var - {@link BDD} representation of given variable
   * @return (invert variable var for zdd)
   */
  BDD change(BDD zdd, BDD var);

  /**
   * Creates a zdd representing the union set of two arguments.
   *
   * @param zdd1 - getIf {@link BDD} argument
   * @param zdd2 - getThen {@link BDD} argument
   * @return (zdd1 UNION zdd2)
   */
  BDD union(BDD zdd1, BDD zdd2);

  /**
   * Creates a zdd representing the difference set of two arguments.
   *
   * @param zdd1 - getIf {@link BDD} argument
   * @param zdd2 - getThen {@link BDD} argument
   * @return (zdd1 DIFFERENCE zdd2)
   */
  BDD difference(BDD zdd1, BDD zdd2);

  /**
   * Creates a zdd representing the intersection set of two arguments.
   *
   * @param zdd1 - getIf {@link BDD} argument
   * @param zdd2 - getThen {@link BDD} argument
   * @return (zdd1 INTERSECT zdd2)
   */
  BDD intersection(BDD zdd1, BDD zdd2);

  /**
   * Creates a zdd representing the product product set of two arguments.
   *
   * @param zdd1 - getIf {@link BDD} argument
   * @param zdd2 - getThen {@link BDD} argument
   * @return (zdd1 { @ literal * } zdd2)
   */
  BDD product(BDD zdd1, BDD zdd2);

  /**
   * Creates a zdd representing the remainder set of two arguments.
   *
   * @param zdd1 - getIf {@link BDD} argument
   * @param zdd2 - getThen {@link BDD} argument
   * @return (zdd1 { @ literal % } zdd2)
   */
  BDD modulo(BDD zdd1, BDD zdd2);

  /**
   * Creates a zdd representing the quotient set of two arguments.
   *
   * @param zdd1 - getIf {@link BDD} argument
   * @param zdd2 - getThen {@link BDD} argument
   * @return (zdd1 { @ literal / } zdd2)
   */
  BDD division(BDD zdd1, BDD zdd2);

  /**
   * Creates a zdd representing the exclusion set of two arguments.
   *
   * @param zdd1 - getIf {@link BDD} argument
   * @param zdd2 - getThen {@link BDD} argument
   * @return (zdd1 EXCLUDE zdd2)
   */
  BDD exclude(BDD zdd1, BDD zdd2);

  /**
   * Creates a zdd representing the restriction set of two arguments.
   *
   * @param zdd1 - getIf {@link BDD} argument
   * @param zdd2 - getThen {@link BDD} argument
   * @return (zdd1 RESTRICT zdd2)
   */
  BDD restrict(BDD zdd1, BDD zdd2);

  /**
   * Get the empty zdd.
   *
   * @return the zdd false representation.
   */
  BDD empty();

  /**
   * get the base zdd representation.
   *
   * @return the zdd true representation.
   */
  BDD base();

  /** Shutdown algorithm instance and components. */
  void shutdown();

  /**
   * Enum with zdd operation types. Each type belongs to one binary method.
   *
   * @author Stephan Holzner
   * @version 1.0
   */
  enum ZddOps {
    SUB_SET1,
    SUB_SET0,
    CHANGE,
    UNION,
    DIFF,
    INTSEC,
    MUL,
    MOD,
    EXCLUDE,
    RESTRICT,
    DIV
  }
}
