package org.sosy_lab.pjbdd.algorithm.zdd;

import java.util.Optional;
import java.util.concurrent.ForkJoinTask;
import java.util.function.BiFunction;
import org.sosy_lab.pjbdd.cache.bdd.Cache;
import org.sosy_lab.pjbdd.creator.bdd.BDDBuilder;
import org.sosy_lab.pjbdd.node.BDD;
import org.sosy_lab.pjbdd.node.manager.bdd.NodeManager;
import org.sosy_lab.pjbdd.node.manager.bdd.ZDDNodeManager;
import org.sosy_lab.pjbdd.util.threadpool.ParallelismManager;

/**
 * A {@link ZDDAlgorithm} implementation which uses {@link ZDDSerialAlgorithm} as base class.
 * Performs multi core zdd operations. For parallel computations the Fork/Join Framework is used.
 *
 * @author Stephan Holzner
 * @see ZDDSerialAlgorithm
 * @see ZDDAlgorithm
 * @since 1.0
 */
public class ZDDConcurrentAlgorithm extends ZDDSerialAlgorithm {

  /** Worker thread pool manager. */
  private final ParallelismManager parallelismManager;

  /**
   * Creates new {@link ZDDConcurrentAlgorithm} instances with specified parameters.
   *
   * <p>It is strongly recommended to use a {@link BDDBuilder} for instantiation.
   */
  public ZDDConcurrentAlgorithm(
      Cache<Integer, Cache.CacheData> computedTable,
      NodeManager nodeManager,
      ParallelismManager parallelismManager) {
    super(computedTable, nodeManager);
    assert (nodeManager instanceof ZDDNodeManager);
    this.parallelismManager = parallelismManager;
  }

  /** {@inheritDoc} */
  @Override
  protected BDD unaryShannon(BDD zdd, BDD var, ZddOps op) {
    return operationCheck(zdd, var, op).orElseGet(() -> asyncShannon(zdd, var, op));
  }

  /**
   * Asynchronous unary shannon expansion of operation. Terminal check for current recursion step
   * must be performed before calling this method.
   *
   * @param zdd - ZDD argument
   * @param var - variable argument
   * @param op - op to be applied
   * @return f1 'op' f2
   */
  private BDD asyncShannon(BDD zdd, BDD var, ZddOps op) {
    BDD res = applyOperation(zdd, var, op);
    cacheBinaryItem(zdd, var, op.ordinal(), res);
    return res;
  }

  /** {@inheritDoc} */
  @Override
  public BDD union(BDD f1, BDD f2) {
    return unionCheck(f1, f2).orElseGet(() -> asyncUnion(f1, f2));
  }

  /**
   * Asynchronous union operation. Terminal check for current recursion step must be performed
   * before calling this method.
   *
   * @param f1 - getIf zdd argument
   * @param f2 - getThen zdd argument
   * @return f1 'UNION' f2
   */
  private BDD asyncUnion(BDD f1, BDD f2) {
    BDD res;
    if (level(f1) < level(f2)) {
      res = makeNode(union(f1.getLow(), f2), f1.getHigh(), f1.getVariable());
    } else if (level(f1) == level(f2)) {
      res = applyOperation(f1, f2, ZddOps.UNION);
    } else {
      res = makeNode(union(f1, f2.getLow()), f2.getHigh(), f2.getVariable());
    }
    cacheBinaryItem(f1, f2, ZddOps.UNION.ordinal(), res);
    return res;
  }

  /** {@inheritDoc} */
  @Override
  public BDD difference(BDD f1, BDD f2) {
    return differenceCheck(f1, f2).orElseGet(() -> asyncDifference(f1, f2));
  }

  /**
   * Asynchronous difference operation. Terminal check for current recursion step must be performed
   * before calling this method.
   *
   * @param f1 - getIf zdd argument
   * @param f2 - getThen zdd argument
   * @return f1 'DIFFERENCE' f2
   */
  private BDD asyncDifference(BDD f1, BDD f2) {
    BDD res;
    if (level(f1) < level(f2)) {
      // next step check not yet performed
      res = makeNode(difference(f1.getLow(), f2), f1.getHigh(), f1.getVariable());
    } else if (level(f1) == level(f2)) {
      res = applyOperation(f1, f2, ZddOps.DIFF);
    } else {
      // next step check not yet performed
      res = difference(f1, f2.getLow());
    }
    cacheBinaryItem(f1, f2, ZddOps.DIFF.ordinal(), res);
    return res;
  }

  /** {@inheritDoc} */
  @Override
  public BDD intersection(BDD f1, BDD f2) {
    return intersectsCheck(f1, f2).orElseGet(() -> asyncIntersect(f1, f2));
  }

  /**
   * Asynchronous intersection operation. Terminal check for current recursion step must be
   * performed before calling this method.
   *
   * @param f1 - getIf zdd argument
   * @param f2 - getThen zdd argument
   * @return f1 'INTERSECTS' f2
   */
  private BDD asyncIntersect(BDD f1, BDD f2) {
    BDD res;
    if (level(f1) < level(f2)) {
      res = intersection(f1.getLow(), f2);
    } else if (level(f1) == level(f2)) {
      res = applyOperation(f1, f2, ZddOps.INTSEC);
    } else {
      res = intersection(f1, f2.getLow());
    }
    cacheBinaryItem(f1, f2, ZddOps.INTSEC.ordinal(), res);
    return res;
  }

  /** {@inheritDoc} */
  @Override
  public BDD product(BDD f1, BDD f2) {
    return productCheck(f1, f2).orElseGet(() -> asyncProduct(f1, f2));
  }

  /**
   * Asynchronous product operation. Terminal check for current recursion step must be performed
   * before calling this method.
   *
   * @param f1 - getIf zdd argument
   * @param f2 - getThen zdd argument
   * @return f1 'PRODUCT' f2
   */
  private BDD asyncProduct(BDD f1, BDD f2) {
    BDD res;
    if (level(f1) != level(f2)) {
      res = applyOperation(f1, f2, ZddOps.MUL);
    } else {
      BDD lowTmp;
      BDD highTmp;
      BDD high;
      BDD low;
      ForkJoinTask<BDD> lowTask1 = null;
      ForkJoinTask<BDD> highTask1 = null;
      ForkJoinTask<BDD> highTask = null;
      ForkJoinTask<BDD> lowTask = null;

      BDD lowF1 = f1.getLow();
      BDD lowF2 = f2.getLow();
      BDD highF1 = f1.getHigh();
      BDD highF2 = f2.getHigh();

      Optional<BDD> optLowTmp = trySerialComputation(highF1, lowF2, ZddOps.MUL);
      if (optLowTmp.isEmpty()) {
        lowTask1 = createTask(this::asyncProduct, highF1, lowF2);
      }

      Optional<BDD> optHighTmp = trySerialComputation(highF1, highF2, ZddOps.MUL);
      if (optHighTmp.isEmpty()) {
        highTask1 = createTask(this::asyncProduct, highF1, highF2);
      }

      Optional<BDD> optHighTask = trySerialComputation(lowF1, highF2, ZddOps.MUL);
      if (optHighTask.isEmpty()) {
        highTask = createTask(this::asyncProduct, lowF1, highF2);
      }
      Optional<BDD> optLowTask = trySerialComputation(lowF1, lowF2, ZddOps.MUL);
      if (optLowTask.isEmpty()) {
        lowTask = createTask(this::asyncProduct, lowF1, lowF2);
      }

      if (optHighTmp.isPresent()) {
        highTmp = optHighTmp.get();
      } else {
        highTmp = extract(highTask1);
      }

      if (optLowTmp.isPresent()) {
        lowTmp = optLowTmp.get();
      } else {
        lowTmp = extract(lowTask1);
      }

      if (optHighTask.isPresent()) {
        high = optHighTask.get();
      } else {
        high = extract(highTask);
      }

      if (optLowTask.isPresent()) {
        low = optLowTask.get();
      } else {
        low = extract(lowTask);
      }

      res = union(highTmp, lowTmp);
      high = union(res, high);
      res = makeNode(low, high, f1.getVariable());
    }

    cacheBinaryItem(f1, f2, ZddOps.MUL.ordinal(), res);
    return res;
  }

  /** {@inheritDoc} */
  @Override
  public BDD division(BDD f1, BDD f2) {
    return divisionCheck(f1, f2).orElseGet(() -> asyncDivision(f1, f2));
  }

  /**
   * Asynchronous division operation. Terminal check for current recursion step must be performed
   * before calling this method.
   *
   * @param f1 - getIf zdd argument
   * @param f2 - getThen zdd argument
   * @return f1 'DIVIDE' f2
   */
  private BDD asyncDivision(BDD f1, BDD f2) {
    BDD res;
    if (level(f1) < level(f2)) {
      res = applyOperation(f1, f2, ZddOps.DIV);
    } else {
      res = sequentialDivisionCase(f1, f2);
    }
    cacheBinaryItem(f1, f2, ZddOps.DIV.ordinal(), res);
    return res;
  }

  /** {@inheritDoc} */
  @Override
  public BDD exclude(BDD f1, BDD f2) {
    return excludeCheck(f1, f2).orElseGet(() -> asyncExclude(f1, f2));
  }

  /**
   * Asynchronous exclude operation. Terminal check for current recursion step must be performed
   * before calling this method.
   *
   * @param f1 - getIf zdd argument
   * @param f2 - getThen zdd argument
   * @return f1 'EXCLUDE' f2
   */
  private BDD asyncExclude(BDD f1, BDD f2) {
    BDD res;
    if (level(f1) > level(f2)) {
      res = exclude(f1, f2.getLow());
    } else if (level(f1) < level(f2)) {
      res = applyOperation(f1, f2, ZddOps.EXCLUDE);
    } else {
      BDD low;
      BDD high;
      if (followLow(f2.getHigh()).equals(base())) {
        high = empty();
      } else {
        BDD lowF2 = f2.getLow();
        BDD highF1 = f1.getHigh();
        BDD highF2 = f2.getHigh();

        Optional<BDD> lowCheck = excludeCheck(highF1, highF2);
        Optional<BDD> highCheck = excludeCheck(highF1, lowF2);

        if (lowCheck.isEmpty() && highCheck.isEmpty() && parallelismManager.canFork(level(f1))) {
          ForkJoinTask<BDD> lowTask = createTask(this::asyncExclude, highF1, highF2);
          ForkJoinTask<BDD> highTask = createTask(this::asyncExclude, highF1, lowF2);
          high = intersection(extract(highTask), extract(lowTask));
        } else {
          high = highCheck.orElseGet(() -> asyncExclude(highF1, lowF2));
          low = lowCheck.orElseGet(() -> asyncExclude(highF1, highF2));
          high = intersection(high, low);
        }
      }
      low = exclude(f1.getLow(), f2.getLow());
      res = makeNode(low, high, f1.getVariable());
    }

    cacheBinaryItem(f1, f2, ZddOps.EXCLUDE.ordinal(), res);
    return res;
  }

  /** {@inheritDoc} */
  @Override
  public BDD restrict(BDD f1, BDD f2) {
    return restrictCheck(f1, f2).orElseGet(() -> asyncRestrict(f1, f2));
  }

  /**
   * Asynchronous restrict operation. Terminal check for current recursion step must be performed
   * before calling this method.
   *
   * @param f1 - getIf zdd argument
   * @param f2 - getThen zdd argument
   * @return f1 'RESTRICT' f2
   */
  private BDD asyncRestrict(BDD f1, BDD f2) {
    BDD res;
    if (level(f1) > level(f2)) {
      res = restrict(f1, f2.getLow());
    } else if (level(f1) < level(f2)) {
      res = applyOperation(f1, f2, ZddOps.RESTRICT);
    } else {
      BDD lowTmp;
      BDD highTmp;
      BDD lowRes;
      BDD highRes;
      ForkJoinTask<BDD> lowTmpTask = null;
      ForkJoinTask<BDD> highTmpTask = null;
      ForkJoinTask<BDD> lowResTask = null;

      BDD lowF1 = f1.getLow();
      BDD lowF2 = f2.getLow();
      BDD highF1 = f1.getHigh();
      BDD highF2 = f2.getHigh();

      Optional<BDD> optLowTmp = trySerialComputation(highF1, lowF2, ZddOps.RESTRICT);
      if (optLowTmp.isEmpty()) {
        lowTmpTask = createTask(this::asyncRestrict, highF1, lowF2);
      }
      Optional<BDD> optHighTmp = trySerialComputation(highF1, highF2, ZddOps.RESTRICT);
      if (optHighTmp.isEmpty()) {
        highTmpTask = createTask(this::asyncRestrict, highF1, highF2);
      }
      Optional<BDD> optLowRes = trySerialComputation(lowF1, lowF2, ZddOps.RESTRICT);
      if (optLowRes.isEmpty()) {
        lowResTask = createTask(this::asyncRestrict, lowF1, lowF2);
      }

      if (optLowTmp.isPresent()) {
        lowTmp = optLowTmp.get();
      } else {
        lowTmp = extract(lowTmpTask);
      }

      if (optHighTmp.isPresent()) {
        highTmp = optHighTmp.get();
      } else {
        highTmp = extract(highTmpTask);
      }

      Optional<BDD> optHighRes = trySerialComputation(lowTmp, highTmp, ZddOps.UNION);
      if (optHighRes.isEmpty()) {
        ForkJoinTask<BDD> highResTask = createTask(this::asyncUnion, highTmp, lowTmp);
        highRes = extract(highResTask);
      } else {
        highRes = optHighRes.get();
      }

      if (optLowRes.isPresent()) {
        lowRes = optLowRes.get();
      } else {
        lowRes = extract(lowResTask);
      }

      res = makeNode(lowRes, highRes, f1.getVariable());
    }

    cacheBinaryItem(f1, f2, ZddOps.RESTRICT.ordinal(), res);
    return res;
  }

  /**
   * Asynchronous applying zdd operation on child nodes.
   *
   * @param f1 - the getIf zdd argument
   * @param f2 - the getThen zdd argument
   * @param op - the operation to be applied
   * @return f1 'op' f2
   */
  private BDD applyOperation(BDD f1, BDD f2, ZddOps op) {
    BDD lowF1 = f1;
    BDD lowF2 = f2;
    BDD highF1 = f1;
    BDD highF2 = f2;
    if (level(f1) <= level(f2)) {
      lowF1 = f1.getLow();
      highF1 = f1.getHigh();
    }
    if (level(f2) <= level(f1)) {
      lowF2 = f2.getLow();
      highF2 = f2.getHigh();
    }

    BDD low;
    BDD high;
    int var = level(f1) <= level(f2) ? f1.getVariable() : f2.getVariable();
    // avoid terminal case fork
    Optional<BDD> lowCheck = operationCheck(lowF1, lowF2, op);
    Optional<BDD> highCheck = operationCheck(highF1, highF2, op);

    if (lowCheck.isEmpty() && highCheck.isEmpty() && parallelismManager.canFork(level(f1))) {
      ForkJoinTask<BDD> lowTask = createTask((z1, z2) -> apply(z1, z2, op), lowF1, lowF2);
      ForkJoinTask<BDD> highTask = createTask((z1, z2) -> apply(z1, z2, op), highF1, highF2);
      return makeNode(extract(lowTask), extract(highTask), var);
    } else {
      if (lowCheck.isPresent()) {
        low = lowCheck.get();
      } else {
        low = apply(lowF1, lowF2, op);
      }

      if (highCheck.isPresent()) {
        high = highCheck.get();
      } else {
        high = apply(highF1, highF2, op);
      }
      return makeNode(low, high, var);
    }
  }

  /**
   * Helper method to resolve actual operation to be applied. Necessary because there is no common
   * shannon expansion for zdd operations.
   *
   * @param f1 - the getIf zdd argument
   * @param f2 - the getThen zdd argument
   * @param op - the operation to be applied
   * @return f1 'op' f2
   */
  private BDD apply(BDD f1, BDD f2, ZddOps op) {
    switch (op) {
      case UNION:
        return asyncUnion(f1, f2);
      case DIFF:
        return asyncDifference(f1, f2);
      case INTSEC:
        return asyncIntersect(f1, f2);
      case MUL:
        return asyncProduct(f1, f2);
      case DIV:
        return asyncDivision(f1, f2);
      case EXCLUDE:
        return asyncExclude(f1, f2);
      case RESTRICT:
        return asyncRestrict(f1, f2);
      case SUB_SET1:
      case CHANGE:
      case SUB_SET0:
        return asyncShannon(f1, f2, op);
      default:
        throw new IllegalArgumentException("Unknown Operator");
    }
  }

  /**
   * Helper function to avoid terminal case forking and reduces some duplicate code. Returns
   * terminal case result, operation result if worker pool is busy or empty if computation can be
   * performed in parallel.
   *
   * @param f1 - the getIf zdd argument
   * @param f2 - the getThen zdd argument
   * @param op - the operation to be performed
   * @return Optional of computation result
   */
  private Optional<BDD> trySerialComputation(BDD f1, BDD f2, ZddOps op) {
    return operationCheck(f1, f2, op)
        .map(Optional::of)
        .orElseGet(
            () -> {
              if (!parallelismManager.canFork(level(f1))) {
                return Optional.of(apply(f1, f2, op));
              }
              return Optional.empty();
            });
  }

  /**
   * Helper function to create and submit a {@link ForkJoinTask} to {@link
   * ZDDConcurrentAlgorithm#parallelismManager}'s thread pool.
   *
   * @param operation - the operation the new task will perform
   * @param f1 - getIf {@link BDD} argument
   * @param f2 - getThen {@link BDD} argument
   * @return the task performing the f1 'operation' f2
   */
  private ForkJoinTask<BDD> createTask(BiFunction<BDD, BDD, BDD> operation, BDD f1, BDD f2) {
    parallelismManager.taskSupplied();
    return parallelismManager.getThreadPool().submit(() -> operation.apply(f1, f2));
  }

  /**
   * Helper function to extract asynchronous task result. May block the supplied task.
   *
   * @param task - the computation task
   * @return the tasks computation result
   */
  private BDD extract(ForkJoinTask<BDD> task) {
    BDD res = task.join();
    parallelismManager.taskDone();
    return res;
  }
  /** {@inheritDoc} */
  @Override
  public void shutdown() {
    super.shutdown();
    parallelismManager.shutdown();
  }
}
