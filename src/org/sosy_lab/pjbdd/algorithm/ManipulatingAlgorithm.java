package org.sosy_lab.pjbdd.algorithm;

import java.util.Optional;
import org.sosy_lab.pjbdd.cache.bdd.Cache;
import org.sosy_lab.pjbdd.node.BDD;
import org.sosy_lab.pjbdd.node.manager.bdd.NodeManager;
import org.sosy_lab.pjbdd.util.HashCodeGenerator;

/**
 * Abstract base class for {@link Algorithm} implementations.
 *
 * @author Stephan Holzner
 * @see Algorithm
 * @since 1.1
 */
public abstract class ManipulatingAlgorithm implements Algorithm {

  /** the node creating and handling instance. */
  protected final NodeManager nodeManager;
  /** the computation cache. */
  protected final Cache<Integer, Cache.CacheData> computedTable;

  /**
   * Constructor for initializing variables.
   *
   * @param computedTable - the computation cache to be used
   * @param nodeManager - the node manager to be used
   */
  public ManipulatingAlgorithm(
      Cache<Integer, Cache.CacheData> computedTable, NodeManager nodeManager) {
    this.nodeManager = nodeManager;
    this.computedTable = computedTable;
  }
  /** {@inheritDoc} */
  @Override
  public void shutdown() {
    nodeManager.shutdown();
    computedTable.clear();
  }
  /** {@inheritDoc} */
  @Override
  public BDD makeFalse() {
    return nodeManager.getFalse();
  }
  /** {@inheritDoc} */
  @Override
  public BDD makeTrue() {
    return nodeManager.getTrue();
  }

  /** {@inheritDoc} */
  @Override
  public BDD makeNode(BDD low, BDD high, int var) {
    return nodeManager.makeNode(low, high, var);
  }
  /** {@inheritDoc} */
  @Override
  public BDD restrict(BDD bdd, int var, boolean restrictionVar) {
    return bdd.getVariable() != var ? bdd : restrictionVar ? bdd.getHigh() : bdd.getLow();
  }

  /**
   * Restricts given BDD to it's low child if it's level equal to given level.
   *
   * @param f - given BDD
   * @param topVar - given level
   * @return low or f
   */
  protected BDD low(BDD f, int topVar) {
    return restrict(f, topVar, false);
  }

  /**
   * Restricts given BDD to it's high child if it's level equal to given level.
   *
   * @param f - given BDD
   * @param topVar - given level
   * @return high or f
   */
  protected BDD high(BDD f, int topVar) {
    return restrict(f, topVar, true);
  }

  /**
   * find a various number of levels' topmost level variable.
   *
   * @param levels - variable amount of levels
   * @return topmost variable
   */
  protected int topVar(int... levels) {
    return nodeManager.topVar(levels);
  }

  /**
   * Get a bdd node's variable level.
   *
   * @param bdd - a bdd node
   * @return nodes's variable level
   */
  protected int level(BDD bdd) {
    return nodeManager.level(bdd.getVariable());
  }

  /**
   * check if 'ITE' input triple already computed in cache.
   *
   * @param f1 - if branch
   * @param f2 - then branch
   * @param f3 - else branch
   * @return Optional of cached item or Optional empty
   */
  protected Optional<BDD> checkITECache(BDD f1, BDD f2, BDD f3) {
    int hash = HashCodeGenerator.generateHashCode(f1.hashCode(), f2.hashCode(), f3.hashCode());
    Cache.CacheData data = computedTable.get(hash);

    if (data instanceof Cache.CacheDataITE) {
      Cache.CacheDataITE d = (Cache.CacheDataITE) data;
      if (d.getF1().equals(f1) && d.getF2().equals(f2) && d.getF3().equals(f3)) {
        return Optional.of(d.getRes());
      }
    }
    return Optional.empty();
  }

  /**
   * Store item for 'ITE' input triple in computation cache.
   *
   * @param f1 - if branch
   * @param f2 - then branch
   * @param f3 - else branch
   * @param item - output node
   */
  protected void cacheItem(BDD f1, BDD f2, BDD f3, BDD item) {
    Cache.CacheDataITE data = new Cache.CacheDataITE();
    data.setF1(f1);
    data.setF2(f2);
    data.setF3(f3);
    data.setRes(item);
    int hash = HashCodeGenerator.generateHashCode(f1.hashCode(), f2.hashCode(), f3.hashCode());
    cache(computedTable, data, hash);
  }

  /**
   * Store data in given computation cash for given hash.
   *
   * @param cache - given cash
   * @param data - to write
   * @param hash - data's input hash
   */
  protected void cache(Cache<Integer, Cache.CacheData> cache, Cache.CacheData data, int hash) {
    cache.put(hash, data);
  }

  /**
   * Store calculated apply operation with input tuple in cache.
   *
   * @param f1 - getIf {@link BDD} argument
   * @param f2 - getThen {@link BDD} argument
   * @param op - applied operator
   * @param item - output {@link BDD} argument
   */
  protected void cacheBinaryItem(BDD f1, BDD f2, int op, BDD item) {
    Cache.CacheDataBinaryOp data = new Cache.CacheDataBinaryOp();
    data.setF1(f1);
    data.setF2(f2);
    data.setOp(op);
    data.setRes(item);
    int hash = HashCodeGenerator.generateHashCode(f1.hashCode(), f2.hashCode(), op);
    cacheEntry(data, hash);
  }

  /**
   * Store calculated operation with input in cache.
   *
   * @param f - input {@link BDD} argument
   * @param item - output {@link BDD} argument
   */
  protected void cacheUnaryItem(BDD f, BDD item) {
    Cache.CacheDataNot data = new Cache.CacheDataNot();
    data.setF(f);
    data.setRes(item);
    int hash = f.hashCode();
    cacheEntry(data, hash);
  }

  /**
   * Actually write data in computation cache.
   *
   * @param data - to be saved
   * @param hash - data's hash value
   */
  private void cacheEntry(Cache.CacheData data, int hash) {
    computedTable.put(hash, data);
  }

  /**
   * check if computation cache contains apply operation with input tuple.
   *
   * @param f1 - getIf {@link BDD} argument
   * @param f2 - getThen {@link BDD} argument
   * @param op - applied operator
   * @return Optional of cached item or Optional empty
   */
  protected Optional<BDD> checkBinaryCache(BDD f1, BDD f2, int op) {
    int hash = HashCodeGenerator.generateHashCode(f1.hashCode(), f2.hashCode(), op);
    Cache.CacheData data = computedTable.get(hash);
    if (data instanceof Cache.CacheDataBinaryOp) {
      Cache.CacheDataBinaryOp d = (Cache.CacheDataBinaryOp) data;
      if (d.getF1().equals(f1) && d.getF2().equals(f2) && d.getOp() == op) {
        return Optional.of(d.getRes());
      }
    }

    return Optional.empty();
  }

  /**
   * check if computation cache contains negated input argument.
   *
   * @param f - the {@link BDD} argument
   * @return Optional of cached item or Optional empty
   */
  protected Optional<BDD> checkNotCache(BDD f) {
    int hash = f.hashCode();
    Cache.CacheData data = computedTable.get(hash);
    if (data instanceof Cache.CacheDataNot) {
      Cache.CacheDataNot d = (Cache.CacheDataNot) data;
      if (d.getF().equals(f)) {
        return Optional.of(d.getRes());
      }
    }

    return Optional.empty();
  }
}
