package org.sosy_lab.pjbdd.algorithm.sat;

import java.math.BigInteger;
import org.sosy_lab.pjbdd.cache.bdd.Cache;
import org.sosy_lab.pjbdd.node.BDD;
import org.sosy_lab.pjbdd.node.manager.bdd.NodeManager;

/**
 * ZDD based {@link SatAlgorithm} implementation. Uses serial algorithms for zdd sat operations and
 * {@link BDDSat} as base case since anySat implementations are equal.
 *
 * @author Stephan Holzner
 * @see SatAlgorithm
 * @see BDDSat
 * @since 1.1
 */
public class ZDDSat extends BDDSat {

  public ZDDSat(Cache<BDD, BigInteger> satCountCache, NodeManager nodeManager) {
    super(satCountCache, nodeManager);
  }

  /** {@inheritDoc} */
  @Override
  public BigInteger satCount(BDD b) {

    if (b.isTrue()) {
      return BigInteger.ONE;
    }
    if (b.isFalse()) {
      return BigInteger.ZERO;
    }

    BigInteger count = satCountCache.get(b);
    if (count != null) {
      return count;
    }

    BigInteger res = satCount(b.getLow()).add(satCount(b.getHigh()));
    satCountCache.put(b, res);
    return res;
  }
}
