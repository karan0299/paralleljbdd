package org.sosy_lab.pjbdd.algorithm.sat;

import java.math.BigInteger;
import org.sosy_lab.pjbdd.node.BDD;

/**
 * BDD based interface for sat algorithms.
 *
 * @author Stephan Holzner
 * @since 1.1
 */
public interface SatAlgorithm {
  /**
   * Get {@link BDD}'s number of satisfying variable assignments. All created variables are
   * considered.
   *
   * @param b - the {@link BDD} node
   * @return the number of satisfying variable assignments.
   */
  BigInteger satCount(BDD b);
  /**
   * Takes a {@link BDD} and finds any satisfying variable assignment.
   *
   * @param bdd - the {@link BDD} node
   * @return one satisfying variable assignment.
   */
  BDD anySat(BDD bdd);
}
