package org.sosy_lab.pjbdd.algorithm.sat;

import java.math.BigInteger;
import org.sosy_lab.pjbdd.cache.bdd.Cache;
import org.sosy_lab.pjbdd.node.BDD;
import org.sosy_lab.pjbdd.node.manager.bdd.NodeManager;

/**
 * BDD based {@link SatAlgorithm} implementation. Uses serial algorithms for bdd sat operations.
 *
 * @author Stephan Holzner
 * @see SatAlgorithm
 * @since 1.1
 */
public class BDDSat implements SatAlgorithm {

  /** {@link Cache} used for computed sat counts. */
  protected final Cache<BDD, BigInteger> satCountCache;

  protected final NodeManager nodeManager;

  public BDDSat(Cache<BDD, BigInteger> satCountCache, NodeManager nodeManager) {
    this.satCountCache = satCountCache;
    this.nodeManager = nodeManager;
  }

  /** {@inheritDoc} */
  @Override
  public BDD anySat(BDD bdd) {
    if (bdd.isLeaf()) {
      return bdd;
    }
    if (bdd.getLow().isFalse()) {
      return nodeManager.makeNode(nodeManager.getFalse(), anySat(bdd.getHigh()), bdd.getVariable());
    } else {
      return nodeManager.makeNode(anySat(bdd.getLow()), nodeManager.getFalse(), bdd.getVariable());
    }
  }

  /** {@inheritDoc} */
  @Override
  public BigInteger satCount(BDD b) {
    return BigInteger.valueOf(2).pow(level(b)).multiply(satCountRec(b));
  }

  /**
   * recursive calculation of number of possible satisfying truth assignments for given bdd.
   *
   * @param root - a bdd
   * @return root's number of possible satisfying truth assignments
   */
  private BigInteger satCountRec(BDD root) {
    if (root.isFalse()) {
      return BigInteger.ZERO;
    }
    if (root.isTrue()) {
      return BigInteger.ONE;
    }
    BigInteger count = satCountCache.get(root);
    if (count != null) {
      return count;
    }

    BigInteger s = BigInteger.valueOf(2).pow((level(root.getLow()) - level(root) - 1));
    BigInteger size = s.multiply(satCountRec(root.getLow()));

    s = BigInteger.valueOf(2).pow((level(root.getHigh()) - level(root) - 1));
    size = size.add(s.multiply(satCountRec(root.getHigh())));

    satCountCache.put(root, size);

    return size;
  }

  /**
   * helper call to determine bdd's variable level.
   *
   * @param bdd - the bdd
   * @return level of bdd
   */
  private int level(BDD bdd) {
    return nodeManager.level(bdd.getVariable());
  }
}
