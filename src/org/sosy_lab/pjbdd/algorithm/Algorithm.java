package org.sosy_lab.pjbdd.algorithm;

import org.sosy_lab.pjbdd.node.BDD;

/**
 * general Interface for BDD manipulating algorithms.
 *
 * @author Stephan Holzner
 * @since 1.1
 */
public interface Algorithm {

  /** Shutdown algorithm instance and components. */
  void shutdown();

  /**
   * Get the logical false bdd.
   *
   * @return false bdd
   */
  BDD makeFalse();
  /**
   * Get the logical true bdd.
   *
   * @return true bdd
   */
  BDD makeTrue();
  /**
   * Create a new node with given input triple or return a matching existing.
   *
   * @param low - low branch bdd
   * @param high - high branch bdd
   * @param var - bdd variable
   * @return new or matching bdd
   */
  BDD makeNode(BDD low, BDD high, int var);

  /**
   * Restrict a given variable in a given bdd to high or low branch.
   *
   * @param bdd - given bdd
   * @param var - given variable
   * @param restrictionVar - high or low branch
   * @return restricted bdd
   */
  BDD restrict(BDD bdd, int var, boolean restrictionVar);
}
