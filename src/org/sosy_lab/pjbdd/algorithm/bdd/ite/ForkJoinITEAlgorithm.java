package org.sosy_lab.pjbdd.algorithm.bdd.ite;

import java.util.concurrent.ForkJoinTask;
import org.sosy_lab.pjbdd.cache.bdd.Cache;
import org.sosy_lab.pjbdd.creator.bdd.BDDBuilder;
import org.sosy_lab.pjbdd.node.BDD;
import org.sosy_lab.pjbdd.node.manager.bdd.NodeManager;
import org.sosy_lab.pjbdd.util.threadpool.ParallelismManager;

/**
 * A {@link org.sosy_lab.pjbdd.algorithm.bdd.BDDAlgorithm} implementation which uses {@link
 * ParallelITEAlgorithm} as base class. The java ForkJoin framework is used to perform asynchronous,
 * parallel 'ITE' calculations.
 *
 * @author Stephan Holzner
 * @see ParallelITEAlgorithm
 * @since 1.0
 */
public class ForkJoinITEAlgorithm extends ParallelITEAlgorithm {

  /**
   * Creates new {@link ForkJoinITEAlgorithm} instances with specified parameters.
   *
   * <p>It is strongly recommended to use a {@link BDDBuilder} for instantiation
   *
   * @param computedTable - the computation cache component
   * @param nodeManager - the variable manager
   * @param parallelismManager - the worker thread pool manager
   */
  public ForkJoinITEAlgorithm(
      Cache<Integer, Cache.CacheData> computedTable,
      NodeManager nodeManager,
      ParallelismManager parallelismManager) {
    super(computedTable, nodeManager, parallelismManager);
  }

  /** {@inheritDoc} */
  @Override
  protected BDD forkITE(
      BDD lowF1, BDD lowF2, BDD lowF3, BDD highF1, BDD highF2, BDD highF3, int topVar) {
    parallelismManager.taskSupplied();
    ForkJoinTask<BDD> lowTask =
        parallelismManager.getThreadPool().submit(() -> makeIte(lowF1, lowF2, lowF3));
    ForkJoinTask<BDD> highTask =
        parallelismManager.getThreadPool().submit(() -> makeIte(highF1, highF2, highF3));
    BDD bdd = makeNode(lowTask.join(), highTask.join(), topVar);
    parallelismManager.taskDone();
    return bdd;
  }
}
