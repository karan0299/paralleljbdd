package org.sosy_lab.pjbdd.algorithm.bdd.ite;

import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.sosy_lab.pjbdd.algorithm.bdd.AbstractBDDAlgorithm;
import org.sosy_lab.pjbdd.cache.bdd.Cache;
import org.sosy_lab.pjbdd.creator.bdd.BDDBuilder;
import org.sosy_lab.pjbdd.node.BDD;
import org.sosy_lab.pjbdd.node.manager.bdd.NodeManager;
import org.sosy_lab.pjbdd.util.threadpool.ParallelismManager;

/**
 * A {@link AbstractBDDAlgorithm} implementation which uses {@link ParallelITEAlgorithm} as base
 * class. A parallel {@link Stream} is used to perform asynchronous 'ITE' calculations.
 *
 * @author Stephan Holzner
 * @see ParallelITEAlgorithm
 * @since 1.0
 */
public class StreamITEAlgorithm extends ParallelITEAlgorithm {

  /**
   * Creates new {@link StreamITEAlgorithm} instances with specified parameters.
   *
   * <p>It is strongly recommended to use a {@link BDDBuilder} for instantiation
   *
   * @param computedTable - the computation cache component
   * @param nodeManager - the node manager
   * @param parallelismManager - the worker thread pool manager
   */
  public StreamITEAlgorithm(
      Cache<Integer, Cache.CacheData> computedTable,
      NodeManager nodeManager,
      ParallelismManager parallelismManager) {
    super(computedTable, nodeManager, parallelismManager);
  }

  /** {@inheritDoc} */
  @Override
  protected BDD forkITE(
      BDD lowF1, BDD lowF2, BDD lowF3, BDD highF1, BDD highF2, BDD highF3, int topVar) {
    parallelismManager.taskSupplied();
    parallelismManager.taskSupplied();
    Map<Boolean, BDD> subs =
        parallelismManager
            .getThreadPool()
            .submit(
                () ->
                    Stream.of(false, true)
                        .parallel()
                        .collect(
                            Collectors.toMap(
                                b -> b,
                                b -> {
                                  if (b) {
                                    return makeIte(highF1, highF2, highF3);
                                  } else {
                                    return makeIte(lowF1, lowF2, lowF3);
                                  }
                                })))
            .join();
    parallelismManager.taskDone();
    parallelismManager.taskDone();
    return makeNode(subs.get(false), subs.get(true), topVar);
  }
}
