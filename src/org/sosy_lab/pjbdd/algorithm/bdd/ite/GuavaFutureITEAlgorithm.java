package org.sosy_lab.pjbdd.algorithm.bdd.ite;

import com.google.common.util.concurrent.FutureCallback;
import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.ListenableFuture;
import com.google.common.util.concurrent.ListeningExecutorService;
import com.google.common.util.concurrent.MoreExecutors;
import java.util.concurrent.ExecutionException;
import org.sosy_lab.pjbdd.cache.bdd.Cache;
import org.sosy_lab.pjbdd.creator.bdd.BDDBuilder;
import org.sosy_lab.pjbdd.node.BDD;
import org.sosy_lab.pjbdd.node.manager.bdd.NodeManager;
import org.sosy_lab.pjbdd.util.threadpool.ParallelismManager;

/**
 * A {@link org.sosy_lab.pjbdd.algorithm.bdd.BDDAlgorithm} implementation which uses {@link
 * ITEBDDAlgorithm} as base class. Google Guava's {@link ListenableFuture}s are used to perform
 * asynchronous, parallel 'ITE' calculations.
 *
 * @author Stephan Holzner
 * @see ITEBDDAlgorithm
 * @since 1.0
 */
public class GuavaFutureITEAlgorithm extends ITEBDDAlgorithm {
  /** Guavas worker thread pool wrapper interface. */
  private final ListeningExecutorService service;

  /**
   * Creates new {@link GuavaFutureITEAlgorithm} instances with specified parameters.
   *
   * <p>It is strongly recommended to use a {@link BDDBuilder} for instantiation
   *
   * @param computedTable - the computation cache component
   * @param nodeManager - the variable manager
   * @param parallelismManager - the worker thread pool manager
   */
  public GuavaFutureITEAlgorithm(
      Cache<Integer, Cache.CacheData> computedTable,
      NodeManager nodeManager,
      ParallelismManager parallelismManager) {
    super(computedTable, nodeManager);
    service = MoreExecutors.listeningDecorator(parallelismManager.getThreadPool());
  }

  /** {@inheritDoc} */
  @Override
  public BDD makeIte(BDD f1, BDD f2, BDD f3) {
    try {
      return asyncExpand(f1, f2, f3).get();
    } catch (InterruptedException | ExecutionException e) {
      throw new RuntimeException(e.getCause());
    }
  }

  /**
   * Recursive construction of an 'ITE' task as async ListenableFuture.
   *
   * @param f1 - if branch
   * @param f2 - then branch
   * @param f3 - else branch
   * @return the constructed CompletableFuture of ite of (f1,f2,f3)
   */
  private ListenableFuture<BDD> asyncExpand(BDD f1, BDD f2, BDD f3) {
    return terminalIteCheck(f1, f2, f3)
        .map(Futures::immediateFuture)
        .orElseGet(
            () -> {
              int topVar = topVar(level(f1), level(f2), level(f3));

              ListenableFuture<BDD> lowFut =
                  Futures.transformAsync(
                      Futures.immediateFuture(null), // null causes no new Object instantiation
                      input -> asyncExpand(low(f1, topVar), low(f2, topVar), low(f3, topVar)),
                      service);

              ListenableFuture<BDD> highFut =
                  Futures.transformAsync(
                      Futures.immediateFuture(null), // null causes no new Object instantiation
                      input -> asyncExpand(high(f1, topVar), high(f2, topVar), high(f3, topVar)),
                      service);

              ListenableFuture<BDD> future =
                  Futures.whenAllSucceed(lowFut, highFut)
                      .call(
                          () -> makeNode(lowFut.get(), highFut.get(), topVar),
                          MoreExecutors.directExecutor());

              Futures.addCallback(
                  future,
                  (OnSuccessListener) result -> cacheItem(f1, f2, f3, result),
                  MoreExecutors.directExecutor());

              return future;
            });
  }

  /** {@inheritDoc} */
  @Override
  public void shutdown() {
    super.shutdown();
    service.shutdown();
  }

  /**
   * Custom {@link FutureCallback} interface with default {@link #onFailure(Throwable)}
   * implementation. Enables Lambda notation for {@link FutureCallback#onSuccess(Object)}.
   *
   * @author Stephan Holzner
   * @since 1.0
   */
  private interface OnSuccessListener extends FutureCallback<BDD> {
    /** {@inheritDoc} */
    @Override
    default void onFailure(Throwable t) {}
  }
}
