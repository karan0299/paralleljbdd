package org.sosy_lab.pjbdd.algorithm.bdd.ite;

import java.util.Optional;
import org.sosy_lab.pjbdd.cache.bdd.Cache;
import org.sosy_lab.pjbdd.creator.bdd.Creator;
import org.sosy_lab.pjbdd.node.BDD;
import org.sosy_lab.pjbdd.node.manager.bdd.NodeManager;
import org.sosy_lab.pjbdd.util.threadpool.ParallelismManager;

/**
 * abstract {@link ITEBDDAlgorithm} implementation with {@link BDD} objects as bdd data structure.
 * Base class for several concurrent ITEBDDAlgorithm implementations Subclasses:
 *
 * <ul>
 *   <li>{@link FutureITEAlgorithm}
 *   <li>{@link StreamITEAlgorithm}
 *   <li>{@link ForkJoinITEAlgorithm}
 * </ul>
 *
 * @author Stephan Holzner
 * @see Creator
 * @see FutureITEAlgorithm
 * @see StreamITEAlgorithm
 * @see ForkJoinITEAlgorithm
 * @see ITEBDDAlgorithm
 * @since 1.0
 */
public abstract class ParallelITEAlgorithm extends ITEBDDAlgorithm {
  /** Worker thread pool manager. */
  protected final ParallelismManager parallelismManager;

  /**
   * Creates new {@link ParallelITEAlgorithm} instances with given parameters.
   *
   * @param computedTable - the computation cache component
   * @param nodeManager - the node manager
   * @param parallelismManager - the parallelism manager
   */
  protected ParallelITEAlgorithm(
      Cache<Integer, Cache.CacheData> computedTable,
      NodeManager nodeManager,
      ParallelismManager parallelismManager) {
    super(computedTable, nodeManager);
    this.parallelismManager = parallelismManager;
  }

  /**
   * Asynchronous 'ITE' calculation for input triple.
   *
   * @param f1 - if branch
   * @param f2 - then branch
   * @param f3 - else branch
   * @return ite(f1, f2, f3)
   */
  @Override
  public BDD makeIte(BDD f1, BDD f2, BDD f3) {
    Optional<BDD> check = terminalIteCheck(f1, f2, f3);
    if (check.isPresent()) {
      return check.get();
    }

    int topVar = topVar(level(f1), level(f2), level(f3));
    BDD res;
    BDD low = null;
    BDD high = null;
    BDD lowF1 = low(f1, topVar);
    BDD lowF2 = low(f2, topVar);
    BDD lowF3 = low(f3, topVar);
    BDD highF1 = high(f1, topVar);
    BDD highF2 = high(f2, topVar);
    BDD highF3 = high(f3, topVar);

    Optional<BDD> lowCheck;
    Optional<BDD> highCheck;
    lowCheck = terminalIteCheck(lowF1, lowF2, lowF3);
    highCheck = terminalIteCheck(highF1, highF2, highF3);

    if (lowCheck.isPresent()) {
      low = lowCheck.get();
    }
    if (highCheck.isPresent()) {
      high = highCheck.get();
    }

    if (forkCheck(low, high, topVar)) {
      res = forkITE(lowF1, lowF2, lowF3, highF1, highF2, highF3, topVar);
    } else {
      if (low == null) {
        low = makeIte(lowF1, lowF2, lowF3);
      }
      if (high == null) {
        high = makeIte(highF1, highF2, highF3);
      }
      res = makeNode(low, high, topVar);
    }
    this.cacheItem(f1, f2, f3, res);
    return res;
  }

  /**
   * Performs parallel recursion step.
   *
   * @param lowF1 - f1 low branch cofactor
   * @param lowF2 - f2 low branch cofactor
   * @param lowF3 - f3 low branch cofactor
   * @param highF1 - f1 high branch cofactor
   * @param highF2 - f2 high branch cofactor
   * @param highF3 - f3 high branch cofactor
   * @param topVar - ite recursions top var
   * @return makeNode(ite ( lowF1, lowF2, lowF3), ite(highF1,highF2, highF3),topVar)
   */
  protected abstract BDD forkITE(
      BDD lowF1, BDD lowF2, BDD lowF3, BDD highF1, BDD highF2, BDD highF3, int topVar);

  /**
   * Check if forkITE to be called.
   *
   * @param low - terminal checked low branch
   * @param high - terminal checked high branch
   * @param topVar - ite recursions top var
   * @return true if 'forkITE' will be called in current recursion step false else
   */
  private boolean forkCheck(BDD low, BDD high, int topVar) {
    return parallelismManager.canFork(topVar) && high == null && low == null;
  }

  /** {@inheritDoc} */
  @Override
  public void shutdown() {
    super.shutdown();
    parallelismManager.shutdown();
  }
}
