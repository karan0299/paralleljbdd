package org.sosy_lab.pjbdd.algorithm.bdd;

import org.sosy_lab.pjbdd.algorithm.Algorithm;
import org.sosy_lab.pjbdd.node.BDD;

/** Interface for algorithm implementations to perform BDD manipulation . */
public interface BDDAlgorithm extends Algorithm {

  /**
   * Performs binary BDD manipulation operations.
   *
   * @param f1 - the first bdd node
   * @param f2 - the second bdd node
   * @param op - the operation to be performed
   * @return f1 'op' f2
   */
  BDD makeOp(BDD f1, BDD f2, ApplyOp op);

  /**
   * Performs if then else computation on input triple.
   *
   * @param f1 - the if bdd node
   * @param f2 - the then bdd node
   * @param f3 - the else bdd node
   * @return ite (f1, f2, f3)
   */
  BDD makeIte(BDD f1, BDD f2, BDD f3);

  /**
   * Negates bdd argument.
   *
   * @param b - the bdd argument
   * @return not b
   */
  BDD makeNot(BDD b);

  /**
   * Creates a {@link BDD} representing an existential quantification of the getThen argument.
   *
   * @param f1 - getIf {@link BDD} argument
   * @param levels - the variable levels to be quantified
   * @return (exists f2 : f1)
   */
  BDD makeExists(BDD f1, int... levels);

  /**
   * Enum with 'Apply' operation types. Each type belongs to one binary method {@link ApplyOp}s
   * corresponding to following methods.
   *
   * <ul>
   *   <li>{@link ApplyOp#OP_AND}: Perform boolean AND for to arguments
   *   <li>{@link ApplyOp#OP_XOR}: Perform boolean XOR for to arguments
   *   <li>{@link ApplyOp#OP_OR}: Perform boolean OR for to arguments
   *   <li>{@link ApplyOp#OP_NAND}: Perform boolean NAND for to arguments
   *   <li>{@link ApplyOp#OP_NOR}: Perform boolean NOR for to arguments
   *   <li>{@link ApplyOp#OP_IMP}: Perform boolean IMP for to arguments
   *   <li>{@link ApplyOp#OP_XNOR}: Perform boolean OP_XNOR for to arguments
   * </ul>
   *
   * @author Stephan Holzner
   * @version 1.0
   */
  enum ApplyOp {
    OP_AND,
    OP_XOR,
    OP_OR,
    OP_NAND,
    OP_NOR,
    OP_IMP,
    OP_XNOR
  }
}
