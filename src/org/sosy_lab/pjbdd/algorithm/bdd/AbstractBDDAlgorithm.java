package org.sosy_lab.pjbdd.algorithm.bdd;

import static org.sosy_lab.pjbdd.algorithm.bdd.BDDAlgorithm.ApplyOp.OP_OR;

import org.sosy_lab.pjbdd.algorithm.ManipulatingAlgorithm;
import org.sosy_lab.pjbdd.cache.bdd.Cache;
import org.sosy_lab.pjbdd.node.BDD;
import org.sosy_lab.pjbdd.node.manager.bdd.NodeManager;
import org.sosy_lab.pjbdd.util.HashCodeGenerator;
import org.sosy_lab.pjbdd.util.IntArrayUtils;

/**
 * An abstract {@link org.sosy_lab.pjbdd.algorithm.bdd.BDDAlgorithm} implementation.
 *
 * @author Stephan Holzner
 * @see ManipulatingAlgorithm
 * @see BDDAlgorithm
 * @since 1.1
 */
public abstract class AbstractBDDAlgorithm extends ManipulatingAlgorithm implements BDDAlgorithm {

  /** additional cache for existential quantification. */
  protected Cache<Integer, Cache.CacheData> quantCache;

  public AbstractBDDAlgorithm(
      Cache<Integer, Cache.CacheData> computedTable, NodeManager nodeManager) {
    super(computedTable, nodeManager);
    quantCache = computedTable.cleanCopy();
  }

  public BDD makeExist(BDD f, int level) {
    if (f.isLeaf() || level(f) > level) {
      return f;
    }
    int hash = HashCodeGenerator.generateHashCode(f.hashCode(), level);
    Cache.CacheDataExQuant data = (Cache.CacheDataExQuant) quantCache.get(hash);
    if (data != null && data.getF().equals(f) && data.getLevel() == level) {
      return data.getRes();
    }
    BDD res = null;
    if (level(f) == level) {
      res = makeOp(f.getHigh(), f.getLow(), OP_OR);
    }
    if (res == null) {
      BDD high = makeExist(f.getHigh(), level);
      BDD low = makeExist(f.getLow(), level);

      res = makeNode(low, high, f.getVariable());
    }

    Cache.CacheDataExQuant d = new Cache.CacheDataExQuant();
    d.setF(f);
    d.setLevel(level);
    d.setRes(res);
    quantCache.put(hash, d);
    return res;
  }

  @Override
  public BDD makeExists(BDD f, int... levels) {
    if (f.isLeaf() || (level(f) > levels[0] && levels.length == 1)) {
      return f;
    }

    int hash = HashCodeGenerator.generateHashCode(f.hashCode(), levels[0]);
    Cache.CacheDataExQuantVararg data = (Cache.CacheDataExQuantVararg) quantCache.get(hash);
    BDD res = null;

    if (data != null && data.matches(f, levels)) {
      res = data.getRes();
    }
    if (res == null) {
      int fLevel = level(f);
      if (fLevel > levels[0]) {
        int[] newTail = IntArrayUtils.subArray(levels, 1, levels.length);
        res = makeExists(f, newTail);
      } else if (fLevel == levels[0]) {
        if (levels.length == 1) {
          res = makeOp(f.getHigh(), f.getLow(), OP_OR);
        } else {
          int[] newTail = IntArrayUtils.subArray(levels, 1, levels.length);
          res = makeOp(makeExists(f.getLow(), newTail), makeExists(f.getHigh(), newTail), OP_OR);
        }
      } else {
        BDD high = makeExists(f.getHigh(), levels);
        BDD low = makeExists(f.getLow(), levels);
        res = makeNode(low, high, f.getVariable());
      }

      Cache.CacheDataExQuantVararg d = new Cache.CacheDataExQuantVararg();
      d.setF(f);
      d.setLevels(levels);
      d.setRes(res);
      quantCache.put(hash, d);
    }
    return res;
  }
}
