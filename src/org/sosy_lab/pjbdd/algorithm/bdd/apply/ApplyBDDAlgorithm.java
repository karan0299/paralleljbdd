package org.sosy_lab.pjbdd.algorithm.bdd.apply;

import java.util.Optional;
import org.sosy_lab.pjbdd.algorithm.bdd.AbstractBDDAlgorithm;
import org.sosy_lab.pjbdd.algorithm.bdd.ite.ITEBDDAlgorithm;
import org.sosy_lab.pjbdd.cache.bdd.Cache;
import org.sosy_lab.pjbdd.creator.bdd.BDDBuilder;
import org.sosy_lab.pjbdd.node.BDD;
import org.sosy_lab.pjbdd.node.manager.bdd.NodeManager;

/**
 * A {@link org.sosy_lab.pjbdd.algorithm.bdd.BDDAlgorithm} implementation which uses {@link
 * AbstractBDDAlgorithm} as base class. Performs serial 'APPLY' calculations.
 *
 * @author Stephan Holzner
 * @see AbstractBDDAlgorithm
 * @since 1.1
 */
public class ApplyBDDAlgorithm extends ITEBDDAlgorithm {

  /**
   * Creates new {@link ApplyBDDAlgorithm} instances with specified parameters.
   *
   * <p>It is strongly recommended to use a {@link BDDBuilder} for instantiation
   *
   * @param computedTable - the computation cache component
   * @param nodeManager - the node manager
   */
  public ApplyBDDAlgorithm(Cache<Integer, Cache.CacheData> computedTable, NodeManager nodeManager) {
    super(computedTable, nodeManager);
  }

  /** {@inheritDoc} */
  @Override
  public BDD makeNot(BDD f) {
    return makeNotRecursive(f);
  }

  /**
   * Helper method for recursive 'NOT' calls.
   *
   * @param f - the bdd
   * @return the negation of f
   */
  private BDD makeNotRecursive(BDD f) {
    return terminalNotCheck(f)
        .orElseGet(
            () -> {
              BDD bdd =
                  makeNode(
                      makeNotRecursive(f.getLow()), makeNotRecursive(f.getHigh()), f.getVariable());
              this.cacheUnaryItem(f, bdd);
              return bdd;
            });
  }

  /**
   * Asynchronous calculation for applying an operation on two input arguments, with an given
   * terminal function.
   *
   * @param f1 - getIf {@link BDD} argument
   * @param f2 - getThen {@link BDD} argument
   * @param op - the operation to be applied
   * @return (f1 op f2)
   */
  @Override
  public BDD makeOp(BDD f1, BDD f2, ApplyOp op) {
    return terminalCheck(f1, f2, op)
        .orElseGet(
            () -> {
              int topVar = topVar(level(f1), level(f2));
              BDD low = makeOp(low(f1, topVar), low(f2, topVar), op);
              BDD high = makeOp(high(f1, topVar), high(f2, topVar), op);

              BDD res = makeNode(low, high, topVar);
              this.cacheBinaryItem(f1, f2, op.ordinal(), res);
              return res;
            });
  }

  /**
   * Apply terminal check resolver.
   *
   * @param f1 - getIf {@link BDD} argument
   * @param f2 - getThen {@link BDD} argument
   * @param op - the operation to be applied
   * @return optional of terminal check
   */
  protected Optional<BDD> terminalCheck(BDD f1, BDD f2, ApplyOp op) {
    switch (op) {
      case OP_AND:
        return terminalAndCheck(f1, f2);
      case OP_OR:
        return terminalOrCheck(f1, f2);
      case OP_IMP:
        return terminalImplyCheck(f1, f2);
      case OP_XNOR:
        return terminalXnorCheck(f1, f2);
      case OP_NAND:
        return terminalNandCheck(f1, f2);
      case OP_NOR:
        return terminalNorCheck(f1, f2);
      case OP_XOR:
        return terminalXorCheck(f1, f2);
      default:
        throw new IllegalArgumentException("No such op supported yet");
    }
  }

  /**
   * Nested base case and cache check for apply op 'XOR' on two arguments.
   *
   * @param f1 - getIf {@link BDD} argument
   * @param f2 - getThen {@link BDD} argument
   * @return optional of check chain
   */
  private Optional<BDD> terminalXorCheck(BDD f1, BDD f2) {
    if (f1.equals(f2)) {
      return Optional.of(makeFalse());
    }
    if (f1.isTrue() && f2.isFalse()) {
      return Optional.of(makeTrue());
    }
    if (f1.isFalse() && f2.isTrue()) {
      return Optional.of(makeTrue());
    }

    return checkBinaryCache(f1, f2, ApplyOp.OP_XOR.ordinal());
  }

  /**
   * Nested base case and cache check for apply op 'NOR' on two arguments.
   *
   * @param f1 - getIf {@link BDD} argument
   * @param f2 - getThen {@link BDD} argument
   * @return optional of check chain
   */
  private Optional<BDD> terminalNorCheck(BDD f1, BDD f2) {
    if (f1.isTrue()) {
      return Optional.of(makeFalse());
    }
    if (f2.isTrue()) {
      return Optional.of(makeFalse());
    }
    if (f1.isFalse() && f2.isFalse()) {
      return Optional.of(makeTrue());
    }
    return checkBinaryCache(f1, f2, ApplyOp.OP_NOR.ordinal());
  }

  /**
   * Nested base case and cache check for apply op 'XNOR' on two arguments.
   *
   * @param f1 - getIf {@link BDD} argument
   * @param f2 - getThen {@link BDD} argument
   * @return optional of check chain
   */
  private Optional<BDD> terminalXnorCheck(BDD f1, BDD f2) {
    if (f1.equals(f2)) {
      return Optional.of(makeTrue());
    }

    if ((f1.isFalse() && f2.isTrue()) || (f2.isFalse() && f1.isTrue())) {
      return Optional.of(makeFalse());
    }

    return checkBinaryCache(f1, f2, ApplyOp.OP_XNOR.ordinal());
  }

  /**
   * Nested base case and cache check for apply op 'NAND' on two arguments.
   *
   * @param f1 - getIf {@link BDD} argument
   * @param f2 - getThen {@link BDD} argument
   * @return optional of check chain
   */
  private Optional<BDD> terminalNandCheck(BDD f1, BDD f2) {
    if (f1.isFalse()) {
      return Optional.of(makeTrue());
    }
    if (f2.isFalse()) {
      return Optional.of(makeTrue());
    }

    if (f1.isTrue() && f2.isTrue()) {
      return Optional.of(makeFalse());
    }
    return checkBinaryCache(f1, f2, ApplyOp.OP_NAND.ordinal());
  }

  /**
   * Nested base case and cache check for apply op 'AND' on two arguments.
   *
   * @param f1 - getIf {@link BDD} argument
   * @param f2 - getThen {@link BDD} argument
   * @return optional of check chain
   */
  private Optional<BDD> terminalAndCheck(BDD f1, BDD f2) {
    if (f1.equals(f2)) {
      return Optional.of(f1);
    }
    if (f1.isFalse() || f2.isFalse()) {
      return Optional.of(makeFalse());
    }
    if (f1.isTrue()) {
      return Optional.of(f2);
    }
    if (f2.isTrue()) {
      return Optional.of(f1);
    }
    return checkBinaryCache(f1, f2, ApplyOp.OP_AND.ordinal());
  }

  /**
   * Nested base case and cache check for apply op 'OR' on two arguments.
   *
   * @param f1 - getIf {@link BDD} argument
   * @param f2 - getThen {@link BDD} argument
   * @return optional of check chain
   */
  private Optional<BDD> terminalOrCheck(BDD f1, BDD f2) {
    if (f1.equals(f2)) {
      return Optional.of(f1);
    }
    if (f1.isTrue() || f2.isTrue()) {
      return Optional.of(makeTrue());
    }
    if (f2.isFalse()) {
      return Optional.of(f1);
    }
    if (f1.isFalse()) {
      return Optional.of(f2);
    }
    return checkBinaryCache(f1, f2, ApplyOp.OP_OR.ordinal());
  }

  /**
   * Nested base case and cache check for the negation of an argument.
   *
   * @param f - the {@link BDD} argument
   * @return optional of check chain
   */
  private Optional<BDD> terminalNotCheck(BDD f) {
    if (f.isTrue()) {
      return Optional.of(makeFalse());
    }
    if (f.isFalse()) {
      return Optional.of(makeTrue());
    }
    return checkNotCache(f);
  }

  /**
   * Nested base case and cache check for apply op 'IMPLY' on two arguments.
   *
   * @param f1 - getIf {@link BDD} argument
   * @param f2 - getThen {@link BDD} argument
   * @return optional of check chain
   */
  private Optional<BDD> terminalImplyCheck(BDD f1, BDD f2) {
    if (f1.isFalse() || f2.isTrue()) {
      return Optional.of(makeTrue());
    }
    if (f1.isTrue()) {
      return Optional.of(f2);
    }
    return checkBinaryCache(f1, f2, ApplyOp.OP_IMP.ordinal());
  }
}
