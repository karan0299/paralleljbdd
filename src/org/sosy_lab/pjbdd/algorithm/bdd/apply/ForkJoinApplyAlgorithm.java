package org.sosy_lab.pjbdd.algorithm.bdd.apply;

import java.util.Optional;
import java.util.concurrent.ForkJoinTask;
import org.sosy_lab.pjbdd.algorithm.bdd.BDDAlgorithm;
import org.sosy_lab.pjbdd.cache.bdd.Cache;
import org.sosy_lab.pjbdd.creator.bdd.BDDBuilder;
import org.sosy_lab.pjbdd.node.BDD;
import org.sosy_lab.pjbdd.node.manager.bdd.NodeManager;
import org.sosy_lab.pjbdd.util.threadpool.ParallelismManager;

/**
 * A {@link BDDAlgorithm} implementation which uses {@link ApplyBDDAlgorithm} as base class. But
 * ForkJoin Framework is used to perform asynchronous, parallel 'APPLY' calculations.
 *
 * @author Stephan Holzner
 * @see BDDAlgorithm
 * @see ApplyBDDAlgorithm
 * @since 1.0
 */
public class ForkJoinApplyAlgorithm extends ApplyBDDAlgorithm {

  /** Worker thread pool manager. */
  private final ParallelismManager parallelismManager;

  /**
   * Creates new {@link ForkJoinApplyAlgorithm} instances with specified parameters.
   *
   * <p>It is strongly recommended to use a {@link BDDBuilder} for instantiation
   *
   * @param computedTable - the computation cache component
   * @param nodeManager - the variable manager
   * @param parallelismManager - the worker thread pool manager
   */
  public ForkJoinApplyAlgorithm(
      Cache<Integer, Cache.CacheData> computedTable,
      NodeManager nodeManager,
      ParallelismManager parallelismManager) {
    super(computedTable, nodeManager);
    this.parallelismManager = parallelismManager;
  }

  /** {@inheritDoc} */
  @Override
  public BDD makeOp(BDD f1, BDD f2, ApplyOp op) {
    return terminalCheck(f1, f2, op).orElseGet(() -> asyncShannonExpansion(f1, f2, op));
  }

  /**
   * Helper method for recursive computation task. Apply an operation on two input arguments, with
   * an given terminal function.
   *
   * @param f1 - getIf {@link BDD} argument
   * @param f2 - getThen {@link BDD} argument
   * @param op - the operation to be applied
   * @return next recursion step
   */
  private BDD asyncShannonExpansion(BDD f1, BDD f2, ApplyOp op) {
    int topVar = topVar(level(f1), level(f2));
    BDD res;
    BDD low = null;
    BDD high = null;
    BDD lowF1 = low(f1, topVar);
    BDD lowF2 = low(f2, topVar);
    BDD highF1 = high(f1, topVar);
    BDD highF2 = high(f2, topVar);

    // avoid terminal case fork
    Optional<BDD> lowCheck = terminalCheck(lowF1, lowF2, op);
    Optional<BDD> highCheck = terminalCheck(highF1, highF2, op);

    if (lowCheck.isPresent()) {
      low = lowCheck.get();
    }
    if (highCheck.isPresent()) {
      high = highCheck.get();
    }

    if (parallelismManager.canFork(topVar) && low == null && high == null) {
      ForkJoinTask<BDD> lowTask =
          parallelismManager.getThreadPool().submit(() -> asyncShannonExpansion(lowF1, lowF2, op));
      ForkJoinTask<BDD> highTask =
          parallelismManager
              .getThreadPool()
              .submit(() -> asyncShannonExpansion(highF1, highF2, op));
      low = lowTask.join();
      high = highTask.join();
    } else {
      if (low == null) {
        low = asyncShannonExpansion(lowF1, lowF2, op);
      }
      if (high == null) {
        high = asyncShannonExpansion(highF1, highF2, op);
      }
    }
    res = makeNode(low, high, topVar);
    this.cacheBinaryItem(f1, f2, op.ordinal(), res);
    return res;
  }

  /** {@inheritDoc} */
  @Override
  public void shutdown() {
    super.shutdown();
    parallelismManager.shutdown();
  }
}
