package org.sosy_lab.pjbdd.algorithm.intBDD;

import com.google.common.primitives.Ints;
import java.util.HashSet;
import java.util.Set;
import org.sosy_lab.pjbdd.cache.intBDD.IntNotCache;
import org.sosy_lab.pjbdd.cache.intBDD.IntOpCache;
import org.sosy_lab.pjbdd.creator.bdd.BDDBuilder;
import org.sosy_lab.pjbdd.node.BDD;
import org.sosy_lab.pjbdd.node.manager.intBDD.IntNodeManager;
import org.sosy_lab.pjbdd.util.HashCodeGenerator;

/**
 * Serial {@link IntAlgorithm} implementation. Uses serial algorithms for bdd node operations.
 *
 * @author Stephan Holzner
 * @see IntAlgorithm
 * @since 1.0
 */
public class IntBDDAlgorithm implements IntAlgorithm {
  protected final IntOpCache opCache;
  protected final IntOpCache iteCache;
  protected final IntOpCache quantCache;
  protected final IntNotCache notCache;
  /** the {@link IntNodeManager} implementation used for variable management. */
  protected final IntNodeManager nodeManager;

  /**
   * Creates a new {@link IntBDDAlgorithm} with specified parameters.
   *
   * <p>It is strongly recommended to use a {@link BDDBuilder} for instantiation.
   *
   * @param iteCache - the ite cache
   * @param opCache - the op cache
   * @param notCache - the not cache
   * @param nodeManager - the variable manager
   */
  public IntBDDAlgorithm(
      IntOpCache opCache,
      IntOpCache iteCache,
      IntNotCache notCache,
      IntOpCache quantCache,
      IntNodeManager nodeManager) {
    this.notCache = notCache;
    this.iteCache = iteCache;
    this.opCache = opCache;
    this.quantCache = quantCache;
    this.nodeManager = nodeManager;
  }

  /** {@inheritDoc} */
  @Override
  public int makeNot(int bdd) {
    int res;
    if (isZero(bdd)) {
      return makeTrue();
    }
    if (isOne(bdd)) {
      return makeFalse();
    }

    res = notCacheLookup(bdd);

    if (res != -1) {
      addRefs(res);
      return res;
    }

    int low = makeNot(low(bdd));
    int high = makeNot(high(bdd));
    res = makeNode(low, high, var(bdd));

    cacheNot(bdd, res);
    return res;
  }

  /** {@inheritDoc} */
  @Override
  public void shutDown() {
    opCache.clear();
    notCache.clear();
  }

  /** {@inheritDoc} */
  @Override
  public int makeFalse() {
    return nodeManager.getFalse();
  }

  /** {@inheritDoc} */
  @Override
  public int makeTrue() {
    return nodeManager.getTrue();
  }

  /** {@inheritDoc} */
  @Override
  public int makeNode(int low, int high, int var) {
    return nodeManager.makeNode(var, low, high);
  }

  /**
   * Perform terminal check for 'ITE' calculations.
   *
   * @param f - the if branch
   * @param g - the then branch
   * @param h - the else branch
   * @return result or -1
   */
  protected int iteCheck(int f, int g, int h) {
    if (isOne(f) || g == h) {
      addRefs(g);
      return g;
    }

    if (isZero(f)) {
      addRefs(h);
      return h;
    }

    if (isOne(g) && isZero(h)) {
      addRefs(f);
      return f;
    }

    if (isZero(g) && isOne(h)) {
      return makeNot(f);
    }

    int res = iteCacheLookup(f, g, h);
    if (res != -1) {
      addRefs(res);
      return res;
    }
    return -1;
  }

  /** {@inheritDoc} */
  @Override
  public int makeIte(int f, int g, int h) {
    int res = iteCheck(f, g, h);
    if (res != -1) {
      return res;
    }

    int topVar = topVarForLevels(level(f), level(g), level(h));

    int low =
        makeIte(
            restrictIf(f, topVar, makeFalse()),
            restrictIf(g, topVar, makeFalse()),
            restrictIf(h, topVar, makeFalse()));
    int high =
        makeIte(
            restrictIf(f, topVar, makeTrue()),
            restrictIf(g, topVar, makeTrue()),
            restrictIf(h, topVar, makeTrue()));
    res = makeNode(low, high, topVar);

    cacheIte(f, g, h, res);

    return res;
  }

  /** {@inheritDoc} */
  @Override
  public int makeOp(int f1, int f2, ApplyOp op) {
    int res = applyCheck(f1, f2, op);
    if (res != -1) {
      return res;
    }
    int topVar = topVarForLevels(level(f1), level(f2));

    int low = makeOp(restrictIf(f1, topVar, makeFalse()), restrictIf(f2, topVar, makeFalse()), op);
    int high = makeOp(restrictIf(f1, topVar, makeTrue()), restrictIf(f2, topVar, makeTrue()), op);
    res = makeNode(low, high, topVar);
    cacheApply(f1, f2, op, res);
    return res;
  }

  /** {@inheritDoc} */
  @Override
  public int exQuant(int f1, int f2) {
    Set<Integer> varSet = createHighVarLevelSet(f2);
    for (Integer i : varSet) {
      f1 = exQuantHelper(f1, i);
    }
    return f1;
  }

  /** {@inheritDoc} */
  @Override
  public int restrict(int bdd, int var, boolean restrictionVar) {
    return var(bdd) != var ? bdd : restrictionVar ? high(bdd) : low(bdd);
  }

  /**
   * Helper Method for Existential quantification of f with var at specific level.
   *
   * @param f - the bdd argument
   * @param level - variable level to be existential quantified
   * @return existential quantification of f1 and f2
   */
  private int exQuantHelper(int f, int level) {
    if (isConst(f) || level(f) > level) {
      return f;
    }
    int res = quantCheck(f, level);
    if (res != -1) {
      return res;
    }

    if (level(f) == level) {
      return makeOp(high(f), low(f), ApplyOp.OP_OR);
    }
    int high = exQuantHelper(high(f), level);
    int low = exQuantHelper(low(f), level);

    res = makeNode(low, high, var(f));
    cacheQuant(f, level, res);
    return res;
  }

  /**
   * Takes a {@link BDD} and creates a set of all high branch variables.
   *
   * @param f - the {@link BDD} argument
   * @return a set of variables
   */
  private Set<Integer> createHighVarLevelSet(int f) {
    Set<Integer> varLevelSet = new HashSet<>();
    while (!isConst(f)) {
      varLevelSet.add(level(f));
      f = high(f);
    }
    return varLevelSet;
  }

  /**
   * Perform terminal check for 'APPLY' calculations.
   *
   * @param f1 - the getIf bdd
   * @param f2 - the getThen bdd
   * @param op - the operation to be applied
   * @return result or -1
   */
  protected int applyCheck(int f1, int f2, ApplyOp op) {
    switch (op) {
      case OP_OR:
        return orCheck(f1, f2);
      case OP_AND:
        return andCheck(f1, f2);
      case OP_XOR:
        if (f1 == f2) {
          return makeFalse();
        }
        if (isZero(f1)) {
          addRefs(f2);
          return f2;
        }
        if (isZero(f2)) {
          addRefs(f1);
          return f1;
        }
        break;
      case OP_NAND:
        if (isZero(f1) || isZero(f2)) {
          return makeTrue();
        }
        if (isOne(f1) && isOne(f2)) {
          return makeFalse();
        }
        break;
      case OP_NOR:
        if (isOne(f1) || isOne(f2)) {
          return makeFalse();
        }
        if (isZero(f1) && isZero(f2)) {
          return makeTrue();
        }
        break;
      case OP_IMP:
        if (isZero(f1) || isOne(f2)) {
          return makeTrue();
        }
        if (isOne(f1)) {
          addRefs(f2);
          return f2;
        }
        break;
      default:
        break;
    }
    if (isConst(f1) && isConst(f2)) {
      return baseOprResults[op.ordinal()][f1 << 1 | f2];
    }

    return opCacheLookup(f1, f2, op);
  }

  /**
   * Perform terminal check for 'AND' calculations.
   *
   * @param f1 - the getIf bdd
   * @param f2 - the getThen bdd
   * @return result or -1
   */
  protected int andCheck(int f1, int f2) {

    if (f1 == f2 || isOne(f2)) {
      addRefs(f1);
      return f1;
    }

    if (isZero(f1) || isZero(f2)) {
      return makeFalse();
    }
    if (isOne(f1)) {
      addRefs(f2);
      return f2;
    }
    return opCacheLookup(f1, f2, ApplyOp.OP_AND);
  }

  /**
   * Perform terminal check for 'OR' calculations.
   *
   * @param f1 - the getIf bdd
   * @param f2 - the getThen bdd
   * @return result or -1
   */
  protected int orCheck(int f1, int f2) {
    if (f1 == f2 || isZero(f2)) {
      addRefs(f1);
      return f1;
    }
    if (isOne(f1) || isOne(f2)) {
      return makeTrue();
    }
    if (isZero(f1)) {
      addRefs(f2);
      return f2;
    }

    return opCacheLookup(f1, f2, ApplyOp.OP_OR);
  }

  /**
   * Lookup performed not computation of bdd.
   *
   * @param root - the bdd
   * @return cached computation or -1 if there is no cached computation
   */
  protected int notCacheLookup(int root) {
    return notCache.get(root);
  }

  /**
   * Lookup performed ite computation of input triple.
   *
   * @param i - the if branch
   * @param t - the then branch
   * @param e - the else branch
   * @return cached computation or -1 if there is no cached computation
   */
  protected int iteCacheLookup(int i, int t, int e) {
    return checkCache(i, t, e, iteCache);
  }

  /**
   * Lookup performed apply computation of input triple.
   *
   * @param f1 - the getIf argument
   * @param f2 - the getThen argument
   * @param op - the apply operation
   * @return cached computation or -1 if there is no cached computation
   */
  protected int opCacheLookup(int f1, int f2, ApplyOp op) {
    return checkCache(f1, f2, op.ordinal(), opCache);
  }

  /**
   * Save computed apply operation in cash (increases bdds' reference count).
   *
   * @param f1 - the getIf argument
   * @param f2 - the getThen argument
   * @param op - the applied operator
   * @param res - the computed result
   */
  protected void cacheApply(int f1, int f2, ApplyOp op, int res) {
    cacheApply(f1, f2, op.ordinal(), res);
  }

  /**
   * Save computed apply operation in cash (increases bdds' reference count).
   *
   * @param f1 - the getIf argument
   * @param f2 - the getThen argument
   * @param op - the applied operator
   * @param res - the computed result
   */
  protected void cacheApply(int f1, int f2, int op, int res) {
    int hash = hash(f1, f2, op);
    IntOpCache.Data oldEntry = opCache.put(hash, opCache.createEntry(f1, f2, op, res));
    addRefs(f1, f2, res);
    if (oldEntry != null) {
      freeRefs(oldEntry.f1(), oldEntry.f2(), oldEntry.result());
    }
  }

  /**
   * Save computed not in cash (increases bdds' reference count).
   *
   * @param root - the getIf argument
   * @param res - the computed result
   */
  protected void cacheNot(int root, int res) {
    IntNotCache.Entry old = notCache.put(root, res);
    if (old != null) {
      freeRefs(old.getInput(), old.getResult());
    }
    addRefs(root, res);
  }

  /**
   * Save computed ite operation in cash (increases bdds' reference count).
   *
   * @param i - the if branch
   * @param t - the then branch
   * @param e - the else branch
   * @param res - the computed result
   */
  protected void cacheIte(int i, int t, int e, int res) {
    int applyHash = hash(i, t, e);
    IntOpCache.Data oldEntry = iteCache.put(applyHash, iteCache.createEntry(i, t, e, res));
    addRefs(i, t, e, res);
    if (oldEntry != null) {
      freeRefs(oldEntry.f1(), oldEntry.f2(), oldEntry.f3(), oldEntry.result());
    }
  }

  /**
   * Save computed ex quant operation in cash (increases bdds' reference count).
   *
   * @param f - the if branch
   * @param level - the level
   * @param res - the computed result
   */
  protected void cacheQuant(int f, int level, int res) {
    int applyHash = hash(f, level);
    IntOpCache.Data oldEntry = quantCache.put(applyHash, quantCache.createEntry(f, level, 0, res));
    addRefs(f, res);
    if (oldEntry != null) {
      freeRefs(oldEntry.f1(), oldEntry.result());
    }
  }

  /**
   * free a various number of bdd arguments.
   *
   * @param fs - bdds to be freed
   */
  private void freeRefs(int... fs) {
    nodeManager.freeRefs(fs);
  }

  /**
   * add a reference for various number of bdd arguments.
   *
   * @param fs - bdds to be referenced
   */
  private void addRefs(int... fs) {
    nodeManager.addRefs(fs);
  }

  /**
   * check given cache for computation with given input triple.
   *
   * @param a - getIf argument
   * @param b - getThen argument
   * @param c - getElse argument
   * @param cache - to be checked
   * @return cached computation or -1 if there is no cached computation
   */
  protected int checkCache(int a, int b, int c, IntOpCache cache) {
    int hash = hash(a, b, c);
    IntOpCache.Data data = cache.get(hash);
    if (data == null) {
      return -1;
    }
    if (data.f1() == a && data.f2() == b && data.f3() == c) {
      addRefs(data.result());
      return data.result();
    }
    return -1;
  }

  /**
   * check quant cache for computation with given input tuple.
   *
   * @param f - getIf argument
   * @param level - to be checked
   * @return cached computation or -1 if there is no cached computation
   */
  protected int quantCheck(int f, int level) {
    int hash = hash(f, level);
    IntOpCache.Data data = quantCache.get(hash);
    if (data == null) {
      return -1;
    }
    if (data.f1() == f && data.f2() == level) {
      addRefs(data.result());
      return data.result();
    }
    return -1;
  }

  /**
   * Get variable level for bdd.
   *
   * @param r - the bdd
   * @return level of r's variable
   */
  protected int level(int r) {
    return nodeManager.level(var(r));
  }

  /**
   * Return the topmost level of a various number of bdds.
   *
   * @param levels - the variable levels
   * @return topmost level
   */
  protected int topVarForLevels(int... levels) {
    return nodeManager.varForLevel(Ints.min(levels));
  }

  /**
   * Restrict root to low or high branch if root's level is equal to given level.
   *
   * @param root - the bdd
   * @param var - the restriction var
   * @param restrictWith - the restrict to low or high
   * @return restricted bdd
   */
  protected int restrictIf(int root, int var, int restrictWith) {
    return var(root) != var ? root : isOne(restrictWith) ? high(root) : low(root);
  }

  /**
   * hash input triple.
   *
   * @param a - getIf value
   * @param b - getThen value
   * @param c - getElse value
   * @return hash value
   */
  protected int hash(int a, int b, int c) {
    return HashCodeGenerator.generateHashCode(a, b, c);
  }

  /**
   * hash input tuple.
   *
   * @param a - getIf value
   * @param b - getThen value
   * @return hash value
   */
  protected int hash(int a, int b) {
    return HashCodeGenerator.generateHashCode(a, b);
  }

  /** Operator results - entry = left<<1 | right (left,right in {0,1}). */
  private static final int[][] baseOprResults = {
    {0, 0, 0, 1}, /* and                       ( & )         */
    {0, 1, 1, 0}, /* xor                       ( ^ )         */
    {0, 1, 1, 1}, /* or                        ( | )         */
    {1, 1, 1, 0}, /* nand                                    */
    {1, 0, 0, 0}, /* nor                                     */
    {1, 1, 0, 1}, /* implication               ( >> )        */
    {1, 0, 0, 1}, /* bi-implication                          */
    {0, 0, 1, 0}, /* difference /greater than  ( - ) ( > )   */
    {0, 1, 0, 0}, /* less than                 ( < )         */
    {1, 0, 1, 1}, /* inverse implication       ( << )        */
    {1, 1, 0, 0}, /* not                       ( ! )         */
  };

  /**
   * check if a bdd represents the logical false representation.
   *
   * @param root - the bdd
   * @return true if root is zero
   */
  protected boolean isZero(int root) {
    return root == makeFalse();
  }

  /**
   * check if a bdd represents the logical true representation.
   *
   * @param root - the bdd
   * @return if root is one
   */
  protected boolean isOne(int root) {
    return root == makeTrue();
  }

  /**
   * check whether a bdd represents the logical true or false representation.
   *
   * @param root - the bdd
   * @return if root is one or zero
   */
  protected boolean isConst(int root) {
    return root <= makeTrue();
  }

  /**
   * get low branch bdd of root.
   *
   * @param root - the root bdd
   * @return low branch of root
   */
  protected int low(int root) {
    return nodeManager.low(root);
  }

  /**
   * get high branch bdd of root.
   *
   * @param root - the root bdd
   * @return high branch of root
   */
  protected int high(int root) {
    return nodeManager.high(root);
  }

  /**
   * get bdd variable of root.
   *
   * @param root - the root bdd
   * @return variable of root
   */
  protected int var(int root) {
    return nodeManager.var(root);
  }
}
