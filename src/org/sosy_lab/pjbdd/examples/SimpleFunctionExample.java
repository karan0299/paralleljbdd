package org.sosy_lab.pjbdd.examples;

import java.util.logging.Logger;
import org.sosy_lab.pjbdd.Builders;
import org.sosy_lab.pjbdd.creator.bdd.Creator;
import org.sosy_lab.pjbdd.creator.zdd.ZDDCreator;
import org.sosy_lab.pjbdd.node.BDD;
import org.sosy_lab.pjbdd.util.parser.DotExporter;

/** Encodes simple function: f = x OR (y AND NOT z). */
public class SimpleFunctionExample {

  private SimpleFunctionExample() {}

  public static void main(String[] args) {
    Logger logger = Logger.getLogger(SimpleFunctionExample.class.getSimpleName());
    logger.info("Construct Boolean formula f = x | (y & -z)");
    logger.info("As BDD:");
    logger.info(new DotExporter().bddToString(buildBDDEncoding()));
    logger.info("As ZDD:");
    logger.info(new DotExporter().bddToString(buildZDDEncoding()));
  }

  private static BDD buildBDDEncoding() {
    Creator c = Builders.newBDDBuilder().setVarCount(3).build();
    BDD x = c.makeIthVar(0);
    BDD y = c.makeIthVar(1);
    BDD z = c.makeIthVar(2);
    // build function
    return c.makeOr(x, c.makeAnd(y, c.makeNot(z)));
  }

  private static BDD buildZDDEncoding() {
    ZDDCreator c = Builders.newZDDBuilder().setVarCount(3).build();

    // Encode zdd variable representations to be used in functions

    // encode x
    BDD dontcarz0 = c.makeNode(c.empty(), c.empty(), 2);
    BDD dontcarz1 = c.makeNode(c.base(), c.base(), 2);
    BDD dontcary0 = c.makeNode(dontcarz0, dontcarz0, 1);
    BDD dontcary1 = c.makeNode(dontcarz1, dontcarz1, 1);
    BDD bddx = c.makeNode(dontcary0, dontcary1, 0);

    // encode y
    BDD y = c.makeNode(dontcarz0, dontcarz1, 1);
    BDD bddy = c.makeNode(y, y, 0);

    // encode z
    BDD z = c.makeNode(c.empty(), c.base(), 2);
    BDD yz = c.makeNode(z, z, 1);
    BDD bddz = c.makeNode(yz, yz, 0);

    // build function (difference (x, y) = and(y, not(y)))
    return c.union(bddx, c.difference(bddy, bddz));
  }
}
