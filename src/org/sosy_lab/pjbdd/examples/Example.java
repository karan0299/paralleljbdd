package org.sosy_lab.pjbdd.examples;

import java.math.BigInteger;
import org.sosy_lab.pjbdd.node.BDD;

/**
 * Simple interface defining example problems which can be solved with bdd.
 *
 * @author Stephan Holzner
 * @since 1.0
 */
public interface Example {

  /** Builds the examples scenario. */
  void build();

  /**
   * Solve the example and return number of satisfying truth assignments.
   *
   * @return number of satisfying truth assignments.
   */
  BigInteger solve();

  /**
   * Get the examples solution as {@link BDD}.
   *
   * @return the examples solution as {@link BDD}
   */
  BDD solution();

  /** close example. */
  void close();
}
