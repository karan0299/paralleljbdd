package org.sosy_lab.pjbdd.examples;

import java.math.BigInteger;
import org.sosy_lab.pjbdd.Builders;
import org.sosy_lab.pjbdd.creator.zdd.ZDDCreator;
import org.sosy_lab.pjbdd.node.BDD;

/**
 * Implementation of the n queens problem as {@link Example} which can be solved with bdd.
 *
 * @author Stephan Holzner
 * @see Example
 * @since 1.0
 */
public class NQueensZDD implements Example {
  /** The boolean_operations used for bdd operations. */
  private final ZDDCreator creator;
  /** number of queens akka problem size. */
  private final int n;
  /** {@link BDD} matrix, representing chessboard. */
  private BDD[] board;
  /** representing whole problem bdd. */
  private BDD queen;

  /**
   * Creates new {@link NQueensZDD} instances with given parameter.
   *
   * @param n - the problem size
   * @param creator - the boolean_operations used for bdd operations
   */
  public NQueensZDD(int n, ZDDCreator creator) {
    this.creator = creator;
    this.n = n;
  }

  private BDD get(int i, int j) {
    return board[i + j * n];
  }

  /** {@inheritDoc} */
  @Override
  public void build() {
    queen = creator.empty();
    board = new BDD[n * n];
    BDD one = creator.base();
    BDD zero = creator.empty();

    boolean[] mark = new boolean[n * n];

    for (int i = 0; i < n * n; i++) {
      board[i] = creator.makeNode(zero, one, i);
    }

    for (int i = 0; i < n; i++) {
      queen = creator.union(queen, get(0, i));
    }

    for (int i = 1; i < n; i++) {
      BDD tmp = zero;
      for (int j = 0; j < n; ++j) {
        BDD build = build(i, j, mark);
        tmp = creator.union(tmp, build);
      }
      queen = tmp;
    }
  }

  /**
   * Build all rules for one field and append them to {@link #queen}.
   *
   * @param i - row index
   * @param j - column index
   */
  private BDD build(int i, int j, boolean[] mark) {
    for (int x = 0; x < mark.length; x++) {
      mark[x] = false;
    }
    for (int k = 0; k < i; k++) {
      mark[k + n * j] = true;
    }

    for (int k = 1; k <= i; k++) {
      int a = j - k;
      int b = i - k;
      if (valid(b, a)) {
        mark[b + n * a] = true;
      }
      a = j + k;
      if (valid(b, a)) {
        mark[b + n * a] = true;
      }
    }

    BDD ret = creator.empty();
    for (int k = 0; k < n * n; k++) {
      int a = k / n;
      int b = k % n;
      if (mark[k]) {
        ret = creator.union(ret, get(b, a));
      }
    }
    ret = creator.exclude(queen, ret);

    ret = creator.product(ret, get(i, j));
    return ret;
  }

  private boolean valid(int a, int b) {
    return (a >= 0 && a < n) && (b >= 0 && b < n);
  }

  /** {@inheritDoc} */
  @Override
  public BigInteger solve() {
    return creator.satCount(queen);
  }

  /** {@inheritDoc} */
  @Override
  public BDD solution() {
    return queen;
  }

  public static void main(String[] args) {

    if (args.length == 0) {
      args = new String[] {"8", "9", "10"};
    }

    for (String str : args) {
      final int n = Integer.parseInt(str);
      NQueensZDD queens =
          new NQueensZDD(
              n,
              Builders.newZDDBuilder()
                  .setVarCount(n * n)
                  .setTableSize(100000)
                  .setSelectedCacheSize(10000)
                  .build());

      long start = System.currentTimeMillis();

      queens.build();
      BigInteger count = queens.solve();
      long end = System.currentTimeMillis();
      System.out.println(
          "n="
              + n
              + ":\n Sat Count: "
              + count.intValueExact()
              + "\n Duration: "
              + (end - start)
              + "ms");

      queens.queen = null;
      queens.board = null;

      queens.close();
      System.gc();
    }
  }

  @Override
  public void close() {
    queen = null;
    board = null;
    creator.shutdown();
  }
}
