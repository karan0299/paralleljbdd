package org.sosy_lab.pjbdd.examples;

import java.math.BigInteger;
import org.sosy_lab.pjbdd.Builders;
import org.sosy_lab.pjbdd.creator.bdd.Creator;
import org.sosy_lab.pjbdd.node.BDD;

/**
 * Implementation of the theN queens problem as {@link Example} which can be solved with bdd.
 *
 * @author Stephan Holzner
 * @see Example
 * @since 1.0
 */
public class NQueens implements Example {
  /** The boolean_operations used for bdd operations. */
  private final Creator creator;
  /** number of queens akka problem size. */
  private final int theN;
  /** {@link BDD} matrix, representing chessboard. */
  private BDD[][] board;
  /** representing whole problem bdd. */
  private BDD queen;

  /**
   * Creates new {@link NQueens} instances with given parameter.
   *
   * @param n - the problem size
   * @param creator - the boolean_operations used for bdd operations
   */
  public NQueens(int n, Creator creator) {
    this.creator = creator;
    this.theN = n;
  }

  /** {@inheritDoc} */
  @Override
  public void build() {
    queen = creator.makeTrue();
    board = new BDD[theN][theN];
    BDD zero = creator.makeFalse();

    for (int i = 0; i < theN; ++i) {
      for (int j = 0; j < theN; ++j) {
        board[i][j] = creator.makeIthVar(i * theN + j);
      }
    }
    for (int i = 0; i < theN; ++i) {
      BDD f1 = zero;

      for (int j = 0; j < theN; ++j) {
        f1 = creator.makeOr(f1, board[i][j]);
      }
      queen = creator.makeAnd(queen, f1);
    }

    for (int i = 0; i < theN; ++i) {
      for (int j = 0; j < theN; ++j) {
        build(i, j);
      }
    }
  }

  /** {@inheritDoc} */
  @Override
  public BigInteger solve() {
    return creator.satCount(queen);
  }

  /** {@inheritDoc} */
  @Override
  public BDD solution() {
    return queen;
  }

  public static void main(String[] args) {

    if (args.length == 0) {
      args = new String[] {"8", "9", "10"};
    }

    for (String str : args) {
      final int n = Integer.parseInt(str);

      Creator apply =
          Builders.newBDDBuilder()
              .setVarCount(n * n)
              .setTableSize(100000)
              .setParallelism(500)
              .setThreads(4)
              .setSelectedCacheSize(10000)
              .build();

      NQueens queens = new NQueens(n, apply);

      long start = System.currentTimeMillis();

      queens.build();
      BigInteger count = queens.solve();
      long end = System.currentTimeMillis();
      System.out.println(
          "N=" + n + ":\n SatCount: " + count + "\n Duration: " + (end - start) + "ms");

      queens.queen = null;
      queens.board = null;
      queens.close();
      System.gc();
    }
  }

  /**
   * Build all rules for one field and append them to {@link #queen}.
   *
   * @param i - row index
   * @param j - column index
   */
  private void build(int i, int j) {

    BDD a = creator.makeTrue();
    BDD b = creator.makeTrue();
    BDD c = creator.makeTrue();
    BDD d = creator.makeTrue();

    int k;
    for (k = 0; k < theN; ++k) {
      if (k != j) {
        BDD f5 = creator.makeNand(board[i][k], board[i][j]);
        a = creator.makeAnd(a, f5);
      }
    }

    for (k = 0; k < theN; ++k) {
      if (k != i) {
        BDD f5 = creator.makeNand(board[i][j], board[k][j]);
        b = creator.makeAnd(b, f5);
      }
    }

    int n;
    for (k = 0; k < this.theN; ++k) {
      n = k - i + j;
      if (n >= 0 && n < this.theN && k != i) {
        BDD f6 = creator.makeNand(board[i][j], board[k][n]);
        c = creator.makeAnd(c, f6);
      }
    }

    for (k = 0; k < this.theN; ++k) {
      n = i + j - k;
      if (n >= 0 && n < this.theN && k != i) {
        BDD f6 = creator.makeNand(board[i][j], board[k][n]);
        d = creator.makeAnd(d, f6);
      }
    }

    c = creator.makeAnd(c, d);
    b = creator.makeAnd(b, c);
    a = creator.makeAnd(a, b);

    queen = creator.makeAnd(queen, a);
  }

  @Override
  public void close() {
    queen = null;
    board = null;
    creator.shutDown();
  }
}
