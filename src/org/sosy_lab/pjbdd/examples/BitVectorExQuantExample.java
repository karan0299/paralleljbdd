package org.sosy_lab.pjbdd.examples;

import org.sosy_lab.pjbdd.Builders;
import org.sosy_lab.pjbdd.creator.bdd.Creator;
import org.sosy_lab.pjbdd.node.BDD;

public class BitVectorExQuantExample {

  private BitVectorExQuantExample() {}

  public static void main(String[] args) {
    int vecN = 32;

    int varN = 10;
    bitvectorExquant(vecN, varN);
  }

  private static void bitvectorExquant(int vecN, int varN) {

    varN = (varN % 2 == 0) ? varN : varN + (varN % 2);

    BDD[][] varsi = new BDD[varN][vecN];
    Creator creator = Builders.newBDDBuilder().setThreads(6).setVarCount(vecN * varN).build();

    int var = 0;
    for (int j = 0; j < varN; j++) {
      for (int i = 0; i < vecN; i++) {
        varsi[j][i] = creator.makeIthVar(var++);
      }
    }

    BDD[] chains = new BDD[varN];
    for (int j = 0; j < varN; j++) {
      chains[j] = creator.makeTrue();
      for (int i = 0; i < vecN; i++) {
        chains[j] = creator.makeAnd(chains[j], varsi[j][i]);
      }
    }

    BDD chain = creator.makeTrue();
    for (int i = 0; i < chains.length; ) {
      chain = creator.makeAnd(chain, creator.makeEqual(chains[i++], chains[i++]));
    }
    System.out.println("Start");
    long start = System.currentTimeMillis();
    creator.makeExists(chain, chains[chains.length / 2]);
    System.out.println("End+ \n" + (System.currentTimeMillis() - start));
  }
}
