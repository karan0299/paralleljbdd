package org.sosy_lab.pjbdd.cache.intBDD;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.stream.IntStream;
import javax.annotation.Nullable;
import org.sosy_lab.pjbdd.util.PrimeUtils;

/**
 * Int operations cache implementation with underlying arrays and ReadWriteLocks.
 *
 * @author Stephan Holzner
 * @since 1.0
 */
public class IntOpCache {

  /** the backing array used for caching. */
  private final Data[] table;
  /** the segment lock objects. */
  private final Lock[] locks;

  /**
   * Creates new operations cache with given parameters.
   *
   * @param size - the cache size
   * @param parallelism - the number of independent segments
   */
  public IntOpCache(int size, int parallelism) {
    size = PrimeUtils.getGreaterNextPrime(size);
    table = new Data[size];
    locks = new Lock[parallelism];
    for (int i = 0; i < parallelism; i++) {
      locks[i] = new ReentrantLock();
    }
  }

  /**
   * Get saved entry for given hash.
   *
   * @param hash - the hash
   * @return the saved entry, may return null
   */
  @Nullable
  public Data get(int hash) {
    int index = Math.abs(hash % table.length);
    locks[Math.abs(index % locks.length)].lock();
    try {
      return table[index];
    } finally {
      locks[Math.abs(index % locks.length)].unlock();
    }
  }

  /**
   * Put a computed operation in cache.
   *
   * @param hash - the input value
   * @param data - the resulting
   * @return old value if hash collision occurs else null
   */
  @Nullable
  public Data put(int hash, Data data) {
    int index = Math.abs(hash % table.length);
    locks[Math.abs(index % locks.length)].lock();
    try {
      Data old = table[index];
      table[index] = data;
      return old;
    } finally {
      locks[Math.abs(index % locks.length)].unlock();
    }
  }
  /** Clear all entries. */
  public void clear() {
    fullyLock();
    try {
      IntStream.range(0, table.length).forEach(i -> table[i] = null);
    } finally {
      fullyUnlock();
    }
  }

  /** Fully lock table. */
  private void fullyLock() {
    for (Lock l : locks) {
      l.lock();
    }
  }

  /** Fully unlock table. */
  private void fullyUnlock() {
    for (Lock l : locks) {
      l.unlock();
    }
  }

  /**
   * Create new cache entry.
   *
   * @param f1 - first argument
   * @param f2 - second argument
   * @param op - third argument
   * @param res - operation result
   * @return new cache entry
   */
  public Data createEntry(int f1, int f2, int op, int res) {
    return new Data(f1, f2, op, res);
  }

  /**
   * Data implementation.
   *
   * @author Stephan Holzner
   * @since 1.0
   */
  public static class Data {
    /** cached input (a,b,c) and result(res) values. */
    private final int a;

    private final int b;
    private final int c;
    private final int res;

    /**
     * Creates new IntITEData with given input and result values.
     *
     * @param a - getIf input value
     * @param b - getThen input value
     * @param c - getElse input value
     * @param res - result value
     */
    Data(int a, int b, int c, int res) {
      this.a = a;
      this.b = b;
      this.c = c;
      this.res = res;
    }

    public int result() {
      return res;
    }

    public int f1() {
      return a;
    }

    public int f2() {
      return b;
    }

    public int f3() {
      return c;
    }
  }
}
