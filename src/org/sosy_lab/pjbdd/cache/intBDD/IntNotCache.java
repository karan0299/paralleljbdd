package org.sosy_lab.pjbdd.cache.intBDD;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.stream.IntStream;
import org.sosy_lab.pjbdd.util.PrimeUtils;

/**
 * {@link IntNotCache} implementation with underlying arrays and locks.
 *
 * @author Stephan Holzner
 * @since 1.1
 */
public class IntNotCache {

  /** the backing array used for caching. */
  private final Entry[] table;
  /** the locking table. */
  private final Lock[] locks;

  /**
   * Creates new Cache instances.
   *
   * @param size - the cache size
   * @param parallelism - the parallelling factor
   */
  public IntNotCache(int size, int parallelism) {
    size = PrimeUtils.getGreaterNextPrime(size);
    table = new Entry[size];
    locks = new Lock[parallelism];
    for (int i = 0; i < parallelism; i++) {
      locks[i] = new ReentrantLock();
    }
  }

  /**
   * Lookup a entry for given hash.
   *
   * @param hash - the given hash
   * @return found entry or -1
   */
  public int get(int hash) {
    int index = Math.abs(hash % table.length);
    Lock lock = locks[Math.abs(index % locks.length)];
    lock.lock();
    try {
      Entry savedEntry = table[index];
      return (savedEntry != null && hash == savedEntry.input) ? savedEntry.result : -1;
    } finally {
      lock.unlock();
    }
  }

  /**
   * Put a computed not operation in cache.
   *
   * @param input - the input value
   * @param res - the resulting negation
   * @return old value if hash collision occurs else -1
   */
  public Entry put(int input, int res) {
    int index = Math.abs(input % table.length);
    locks[Math.abs(index % locks.length)].lock();
    try {
      Entry old = table[input];
      table[Math.abs(input % table.length)] = new Entry(input, res);
      return old;
    } finally {
      locks[Math.abs(index % locks.length)].unlock();
    }
  }

  /** Clear all entries. */
  public void clear() {
    fullyLock();
    try {
      IntStream.range(0, table.length).forEach(i -> table[i] = null);
    } finally {
      fullyUnlock();
    }
  }

  /** Fully lock table. */
  private void fullyLock() {
    for (Lock l : locks) {
      l.lock();
    }
  }

  /** Fully unlock table. */
  private void fullyUnlock() {
    for (Lock l : locks) {
      l.unlock();
    }
  }

  /** Table Entry class. */
  public static class Entry {

    private int input;
    private int result;

    public Entry(int input, int result) {
      this.input = input;
      this.result = result;
    }

    public int getInput() {
      return input;
    }

    public void setInput(int input) {
      this.input = input;
    }

    public int getResult() {
      return result;
    }

    public void setResult(int result) {
      this.result = result;
    }
  }
}
