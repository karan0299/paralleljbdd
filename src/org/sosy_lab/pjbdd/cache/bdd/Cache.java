package org.sosy_lab.pjbdd.cache.bdd;

import org.sosy_lab.pjbdd.node.BDD;
import org.sosy_lab.pjbdd.util.IntArrayUtils;

/**
 * Interfaces for computation cache classes.
 *
 * @param <K> cache key type
 * @param <V> cache value type
 * @author Stephan Holzner
 * @since 1.0
 */
public interface Cache<K, V> {

  /**
   * init cache with given parameters.
   *
   * @param cacheSize - the fixed cache size
   * @param parallelism - number of concurrent accesses
   */
  void init(int cacheSize, int parallelism);

  /** clear cache. */
  void clear();

  /**
   * save value for given key.
   *
   * @param key - given key
   * @param value - to save
   */
  void put(K key, V value);

  /**
   * get value for given key.
   *
   * @param key - given key
   * @return value or null if no such key
   */
  V get(K key);

  /**
   * Clone the existing table without values.
   *
   * @return a copy
   */
  Cache<K, V> cleanCopy();

  /**
   * {@link CacheData} implementation for computation caching with two input values.
   *
   * @author Stephan Holzner
   * @see CacheData
   * @since 1.0
   */
  class CacheDataBinaryOp implements CacheData {
    private BDD f1;
    private BDD f2;
    private BDD res;

    public BDD getF1() {
      return f1;
    }

    public void setF1(BDD f1) {
      this.f1 = f1;
    }

    public BDD getF2() {
      return f2;
    }

    public void setF2(BDD f2) {
      this.f2 = f2;
    }

    public BDD getRes() {
      return res;
    }

    public void setRes(BDD res) {
      this.res = res;
    }

    public int getOp() {
      return op;
    }

    public void setOp(int op) {
      this.op = op;
    }

    private int op;
  }

  /**
   * {@link CacheData} implementation for computation caching exquant input values.
   *
   * @author Stephan Holzner
   * @see CacheData
   * @since 1.0
   */
  class CacheDataExQuantVararg implements CacheData {
    private BDD f;
    private BDD res;
    private int[] levels;

    public boolean matches(BDD node, int... mValues) {
      return f.equals(node) && IntArrayUtils.equals(levels, mValues);
    }

    public BDD getF() {
      return f;
    }

    public void setF(BDD f) {
      this.f = f;
    }

    public BDD getRes() {
      return res;
    }

    public void setRes(BDD res) {
      this.res = res;
    }

    public void setLevels(int... levels) {
      this.levels = levels;
    }
  }

  /**
   * {@link CacheData} implementation for computation caching negated input values.
   *
   * @author Stephan Holzner
   * @see CacheData
   * @since 1.0
   */
  class CacheDataExQuant implements CacheData {
    private BDD f;
    private BDD res;
    private int level;

    public BDD getF() {
      return f;
    }

    public void setF(BDD f) {
      this.f = f;
    }

    public BDD getRes() {
      return res;
    }

    public void setRes(BDD res) {
      this.res = res;
    }

    public int getLevel() {
      return level;
    }

    public void setLevel(int level) {
      this.level = level;
    }
  }

  /**
   * {@link CacheData} implementation for computation caching negated input values.
   *
   * @author Stephan Holzner
   * @see CacheData
   * @since 1.0
   */
  class CacheDataNot implements CacheData {
    private BDD f;
    private BDD res;

    public BDD getF() {
      return f;
    }

    public void setF(BDD f) {
      this.f = f;
    }

    public BDD getRes() {
      return res;
    }

    public void setRes(BDD res) {
      this.res = res;
    }
  }

  /**
   * Empty interface as most general data type to be cached in computation caches.
   *
   * @author Stephan Holzner
   * @since 1.0
   */
  interface CacheData {}

  /**
   * {@link CacheData} implementation for 'ITE' computation caching.
   *
   * @author Stephan Holzner
   * @see CacheData
   * @since 1.0
   */
  class CacheDataITE implements CacheData {
    private BDD f1;
    private BDD f2;
    private BDD f3;

    public BDD getF1() {
      return f1;
    }

    public void setF1(BDD f1) {
      this.f1 = f1;
    }

    public BDD getF2() {
      return f2;
    }

    public void setF2(BDD f2) {
      this.f2 = f2;
    }

    public BDD getF3() {
      return f3;
    }

    public void setF3(BDD f3) {
      this.f3 = f3;
    }

    public BDD getRes() {
      return res;
    }

    public void setRes(BDD res) {
      this.res = res;
    }

    private BDD res;
  }
}
