package org.sosy_lab.pjbdd.cache.bdd;

import com.google.common.cache.CacheBuilder;
import java.util.concurrent.TimeUnit;

/**
 * Simple {@link Cache} implementation with backing {@link com.google.common.cache.Cache}.
 * Especially used for sat count caches
 *
 * @param <K> cache key type
 * @param <V> cache value type
 * @author Stephan Holzner
 * @see Cache
 * @since 1.0
 */
public class GuavaCache<K, V> implements Cache<K, V> {

  /** the backing {@link com.google.common.cache.Cache}. */
  private com.google.common.cache.Cache<K, V> cache;

  private int lockCount;
  private int cacheSize;

  /** {@inheritDoc} */
  @Override
  public void init(int size, int parallelism) {
    this.cacheSize = size;
    this.lockCount = parallelism;
    this.cache =
        CacheBuilder.newBuilder()
            .maximumSize(cacheSize)
            .concurrencyLevel(parallelism)
            .expireAfterAccess(5, TimeUnit.MINUTES)
            .build();
  }

  /** {@inheritDoc} */
  @Override
  public void clear() {
    cache.invalidateAll();
  }

  /** {@inheritDoc} */
  @Override
  public void put(K key, V value) {
    cache.put(key, value);
  }

  /** {@inheritDoc} */
  @Override
  public V get(K key) {
    return cache.getIfPresent(key);
  }

  @Override
  public Cache<K, V> cleanCopy() {
    GuavaCache<K, V> copy = new GuavaCache<>();
    copy.init(this.cacheSize, this.lockCount);
    return copy;
  }
}
