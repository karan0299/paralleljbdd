package org.sosy_lab.pjbdd.cache.bdd;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.stream.IntStream;

/**
 * Simple {@link Cache} implementation with backing Array}.
 *
 * @author Stephan Holzner
 * @see Cache
 * @since 1.0
 */
public class ArrayCache implements Cache<Integer, Cache.CacheData> {

  /** the backing array. */
  private Cache.CacheData[] cache;
  /** synchronization mechanism. */
  private Lock[] locks;

  /** {@inheritDoc} */
  @Override
  public void init(int cacheSize, int parallelism) {
    cache = new Cache.CacheData[cacheSize];
    locks = new ReentrantLock[parallelism];
    for (int i = 0; i < parallelism; i++) {
      locks[i] = new ReentrantLock();
    }
  }
  /** {@inheritDoc} */
  @Override
  public void clear() {
    IntStream.range(0, cache.length).forEach(i -> cache[i] = null);
  }
  /** {@inheritDoc} */
  @Override
  public void put(Integer key, Cache.CacheData value) {
    int index = Math.abs(key % cache.length);
    Lock lock = locks[index % locks.length];
    lock.lock();
    try {
      cache[index] = value;
    } finally {
      lock.unlock();
    }
  }
  /** {@inheritDoc} */
  @Override
  public Cache.CacheData get(Integer key) {
    int index = Math.abs(key % cache.length);
    Lock lock = locks[index % locks.length];
    lock.lock();
    try {
      return cache[index];
    } finally {
      lock.unlock();
    }
  }

  @Override
  public Cache<Integer, CacheData> cleanCopy() {
    ArrayCache copy = new ArrayCache();
    copy.init(cache.length, locks.length);
    return copy;
  }
}
