package org.sosy_lab.pjbdd.util.reference;

import java.lang.ref.ReferenceQueue;
import java.lang.ref.WeakReference;

/**
 * Custom {@link WeakReference} holding an int value additionally to the referenced object.
 *
 * @param <T> the referenced objects type
 * @author Stephan Holzner
 * @see WeakReference
 * @since 1.0
 */
public class IntHoldingWeakReference<T> extends WeakReference<T> {

  /** the additionally holden int value. */
  private final transient int intValue;

  /**
   * Creates new {@link IntHoldingWeakReference} instances.
   *
   * @param referent - the referenced object
   * @param q - the queue the the reference should be added after referenced object's collection
   * @param intValue - the int value should be holden
   */
  public IntHoldingWeakReference(T referent, ReferenceQueue<? super T> q, int intValue) {
    super(referent, q);
    this.intValue = intValue;
  }

  /**
   * get the holden int value.
   *
   * @return the holden int value
   */
  public int intValue() {
    return intValue;
  }
}
