package org.sosy_lab.pjbdd.util.reference;

/**
 * Custom abstract daemon {@link Thread}. It's sub classes only designation is to collect and handle
 * {@link java.lang.ref.WeakReference} after garbage collection.
 *
 * @author Stephan Holzner
 * @see Thread
 * @see java.lang.ref.WeakReference
 * @since 1.0
 */
public abstract class ReclaimedReferenceCleaningThread extends Thread {
  /** indicates the threads alive state. */
  private boolean shutdown = false;

  /** Creates new {@link ReclaimedReferenceCleaningThread} instances. */
  protected ReclaimedReferenceCleaningThread() {
    this.setDaemon(true);
    this.setName("weakreferences-cleanupthread");
  }

  /** {@inheritDoc} */
  @Override
  public void run() {
    while (!shutdown) {
      try {
        deleteReclaimedEntries();
      } catch (InterruptedException e) {
        e.printStackTrace();
        return;
      }
    }
  }

  /** shut the thread down. */
  public void shutdown() {
    shutdown = true;
  }

  /**
   * Check if the thread was shutdown.
   *
   * @return {@link #shutdown}
   */
  protected boolean isShutdown() {
    return shutdown;
  }

  /**
   * cleanup garbage collected references.
   *
   * @throws InterruptedException e
   */
  protected abstract void deleteReclaimedEntries() throws InterruptedException;
}
