package org.sosy_lab.pjbdd.util.parser;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;
import org.sosy_lab.pjbdd.node.BDD;

/**
 * Main interface defining methods exporting a {@link BDD} to String or {@link File}.
 *
 * @author Stephan Holzner
 * @see BDD
 * @since 1.0
 */
public interface Exporter {

  /** default export directory. */
  String DEFAULT_PATH = "./res/";

  /**
   * Export a {@link BDD} to exporters string format.
   *
   * @param node - node to be exported
   * @return the string representation
   */
  String bddToString(BDD node);

  /**
   * get exporters file extension.
   *
   * @return exporters file extension
   */
  String extension();

  /**
   * Export a {@link BDD} to file with given path and name.
   *
   * @param graph - a {@link BDD}
   * @param path - the filepath
   * @param name - the filename
   * @return created file
   */
  default File export(BDD graph, String path, String name) {
    Path file = Paths.get(path + name + extension());
    write(file, bddToString(graph));
    return file.toFile();
  }

  /**
   * Export a {@link BDD} to file with given path and default name.
   *
   * @param graph - a {@link BDD}
   * @param path - the filepath
   * @return created file
   */
  default File export(BDD graph, String path) {
    return export(graph, path, generateFromTimeStamp());
  }

  /**
   * Export a {@link BDD} to file with default path and default name.
   *
   * @param bdd - the node to be exported
   * @return created file
   */
  default File export(BDD bdd) {
    return export(bdd, DEFAULT_PATH);
  }

  /**
   * Generate default filename from time stamp.
   *
   * @return current time stamp as string representation
   */
  default String generateFromTimeStamp() {
    Instant instant = Instant.ofEpochMilli(new Date().getTime());
    String s = LocalDateTime.ofInstant(instant, ZoneId.systemDefault()).toLocalDate().toString();
    s += "/" + LocalDateTime.ofInstant(instant, ZoneId.systemDefault()).toLocalTime();
    return s;
  }

  /**
   * Write the bdd in string to a given file.
   *
   * @param file - target file's path
   * @param stringBDD - string bdd representation to be written
   */
  default void write(Path file, String stringBDD) {
    try {
      Files.createDirectories(file.getParent());
      Files.write(file, stringBDD.getBytes(StandardCharsets.UTF_8));
    } catch (IOException e) {
      throw new ImportExportException(ImportExportException.ErrorCodes.FileOpen);
    }
  }
}
