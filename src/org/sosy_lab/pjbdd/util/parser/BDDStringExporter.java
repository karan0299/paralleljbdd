package org.sosy_lab.pjbdd.util.parser;

import java.util.HashMap;
import java.util.Map;
import org.sosy_lab.pjbdd.creator.bdd.Creator;
import org.sosy_lab.pjbdd.node.BDD;

/**
 * {@link Exporter} implementation which exports {@link BDD} in following custom String format:
 *
 * <p>Each node will be represented by a single line with 4 int values separated by ';' as follows:
 * index;variable;low_index;high_index there will be one header line as follows:
 * root_index;var_count;var_lvl1,var_lvl2,var_lvl3...
 *
 * @author Stephan Holzner
 * @see Exporter
 * @since 1.0
 */
public class BDDStringExporter implements Exporter {

  /** the {@link BDD}s boolean_operations context. */
  private final Creator creator;

  /**
   * Creates new {@link BDDStringExporter} instances.
   *
   * @param creator - the boolean_operations context
   */
  public BDDStringExporter(Creator creator) {
    this.creator = creator;
  }

  /**
   * Recursive fills {@link StringBuilder} with bdd nodes child String representations. Afterwards
   * nodes String representation will be appended.
   *
   * @param bdd - the node
   * @param cash - cashed used for enumerate bdd nodes
   * @param buffer - {@link StringBuilder} as buffer
   * @return the nodes String representation as {@link StringBuilder}
   */
  private StringBuilder toBDDString(BDD bdd, Map<BDD, Integer> cash, StringBuilder buffer) {
    if (!cash.containsKey(bdd.getLow())) {
      toBDDString(bdd.getLow(), cash, buffer);
    }
    if (!cash.containsKey(bdd.getHigh())) {
      toBDDString(bdd.getHigh(), cash, buffer);
    }
    cash.putIfAbsent(bdd, cash.size());
    return buffer
        .append(cash.get(bdd))
        .append(";")
        .append(bdd.getVariable())
        .append(";")
        .append(cash.get(bdd.getLow()))
        .append(";")
        .append(cash.get(bdd.getHigh()))
        .append("\n");
  }

  /** {@inheritDoc} */
  @Override
  public String extension() {
    return ".txt";
  }

  /** {@inheritDoc} */
  @Override
  public String bddToString(BDD node) {
    Map<BDD, Integer> cash = new HashMap<>();
    cash.put(creator.makeFalse(), 0);
    cash.put(creator.makeTrue(), 1);
    StringBuilder buffer = new StringBuilder(node.getVariable() * 4 * 3);
    toBDDString(node, cash, buffer.append("0\n").append("1\n"));
    StringBuilder result = makeHeaderLine(node, cash.size() - 1);
    return result.append(buffer).toString();
  }

  /**
   * Make header line for exported {@link BDD} node with format:
   * root_index;var_count;var_lvl1,var_lvl2,var_lvl3...
   *
   * @param node - the {@link BDD} node
   * @param index - the enumerated {@link BDD} value
   * @return header line as {@link StringBuilder}
   */
  private StringBuilder makeHeaderLine(BDD node, int index) {
    StringBuilder builder = new StringBuilder(node.getVariable() * 3);
    builder.append(index).append(";").append(creator.getVariableCount()).append(";");
    int[] varOrdering = creator.getVariableOrdering();
    for (int i : varOrdering) {
      builder.append(i).append(",");
    }
    return builder.append("\n");
  }
}
