package org.sosy_lab.pjbdd.util.parser;

import java.util.HashMap;
import java.util.Map;
import org.sosy_lab.pjbdd.node.BDD;

/**
 * {@link Exporter} implementation which exports {@link BDD} in Dot format.
 *
 * @author Stephan Holzner
 * @see Exporter
 * @since 1.0
 */
public class DotExporter implements Exporter {

  /** Cash used for unique bdd node enumeration. */
  private final Map<BDD, Integer> bddEnumeration = new HashMap<>();

  /** {@inheritDoc} */
  @Override
  public String extension() {
    return ".dot";
  }

  /** {@inheritDoc} */
  @Override
  public String bddToString(BDD node) {
    String result = "digraph G {\n";
    result += recursiveBddToDotStrings(node).toString();
    result += "}";
    bddEnumeration.clear();
    return result;
  }

  /**
   * Recursive fills {@link StringBuilder} with bdd nodes child String representations. Afterwards
   * nodes String representation will be appended.
   *
   * @param node - the node
   * @return the nodes String representation as {@link StringBuilder}
   */
  private StringBuilder recursiveBddToDotStrings(BDD node) {

    StringBuilder result = new StringBuilder();
    bddEnumeration.putIfAbsent(node, bddEnumeration.size());

    if (!node.isLeaf()) {
      if (!bddEnumeration.containsKey(node.getLow())) {
        result.append(recursiveBddToDotStrings(node.getLow()));
      }
      if (!bddEnumeration.containsKey(node.getHigh())) {
        result.append(recursiveBddToDotStrings(node.getHigh()));
      }
    }

    return result.append(nodeToString(node)).append("\n");
  }

  /**
   * converts a bdd node to .dot string.
   *
   * @param node - a bdd
   * @return bdd as .dot string
   */
  private String nodeToString(BDD node) {
    if (node.isFalse()) {
      int index = bddEnumeration.get(node);
      return index + "[label=" + 0 + "]";
    }
    if (node.isTrue()) {
      int index = bddEnumeration.get(node);
      return index + "[label=" + 1 + "]";
    }
    int index = bddEnumeration.get(node);
    return index
        + "[label=x"
        + node.getVariable()
        + "] \n"
        + index
        + "->"
        + bddEnumeration.get(node.getLow())
        + "[style=dashed]  \n"
        + index
        + "->"
        + bddEnumeration.get(node.getHigh());
  }
}
