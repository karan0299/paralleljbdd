package org.sosy_lab.pjbdd.util.parser;

/**
 * A custom {@link IllegalArgumentException} which will be thrown on import or export errors.
 *
 * @author Stephan Holzner
 * @since 1.0
 */
public class ImportExportException extends IllegalArgumentException {
  /** serial version uid. */
  private static final long serialVersionUID = -5365630128856068164L;

  /** Error messages. */
  private static final String[] messages = {"Invalid input file format!", "File opening failure"};

  /**
   * Creates new {@link ImportExportException} instances.
   *
   * @param errorCode - the error code used to determine error message
   */
  public ImportExportException(ErrorCodes errorCode) {
    super(messages[errorCode.ordinal()]);
  }

  /** States the error type. */
  public enum ErrorCodes {
    InvalidFileFormat,
    FileOpen
  }
}
