package org.sosy_lab.pjbdd.util.parser;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.stream.Stream;
import org.sosy_lab.pjbdd.node.BDD;

/**
 * Main interface defining methods importing a {@link BDD} from String representation or {@link
 * File}.
 *
 * @author Stephan Holzner
 * @see BDD
 * @since 1.0
 */
public interface Importer {

  /**
   * import bdd from String(line) stream.
   *
   * @param lines - lines Stream
   * @return imported bdd
   */
  BDD importFromLinesStream(Stream<String> lines);

  /**
   * import bdd from file.
   *
   * @param file - specified path
   * @return imported bdd
   */
  default BDD importBDDFromFile(Path file) {
    try (Stream<String> lines = Files.lines(file, StandardCharsets.UTF_8)) {
      return importFromLinesStream(lines);
    } catch (IOException e) {
      throw new ImportExportException(ImportExportException.ErrorCodes.FileOpen);
    }
  }

  /**
   * import bdd from path.
   *
   * @param path - specified path
   * @return imported bdd
   */
  default BDD importBDDFromPath(String path) {
    return importBDDFromFile(Paths.get(path));
  }

  /**
   * import bdd from string.
   *
   * @param node - specified string
   * @return imported bdd
   */
  default BDD bddFromString(String node) {
    return importFromLinesStream(Stream.of(node.split("\n")));
  }
}
