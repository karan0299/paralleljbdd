package org.sosy_lab.pjbdd;

import org.sosy_lab.pjbdd.creator.bdd.BDDBuilder;
import org.sosy_lab.pjbdd.creator.bdd.intBDD.IntBuilder;
import org.sosy_lab.pjbdd.creator.zdd.ZDDBuilder;

/**
 * Entry point for environment creation. Contains builder creation methods for {@link BDDBuilder},
 * {@link ZDDBuilder}, {@link IntBuilder}.
 *
 * @author Stephan Holzner
 * @see IntBuilder
 * @see BDDBuilder
 * @see ZDDBuilder
 * @since 1.0
 */
public final class Builders {

  /** Avoid instantiation. */
  private Builders() {}

  /**
   * instantiate new {@link ZDDBuilder}.
   *
   * @return new {@link ZDDBuilder}
   */
  public static ZDDBuilder newZDDBuilder() {
    return new ZDDBuilder();
  }

  /**
   * instantiate new {@link BDDBuilder}.
   *
   * @return new {@link BDDBuilder}
   */
  public static BDDBuilder newBDDBuilder() {
    return new BDDBuilder();
  }

  /**
   * instantiate new {@link IntBuilder}.
   *
   * @return new {@link IntBuilder}
   */
  public static IntBuilder newIntBuilder() {
    return new IntBuilder();
  }

  /** Implemented table types. */
  public enum TableType {
    ConcurrentHashBucket,
    ConcurrentHashMap,
    RWLockArray,
    Array
  }

  /** Implemented parallelization types. */
  public enum ParallelizationType {
    FORK_JOIN,
    COMPLETABLE_FUTURE,
    FUTURE,
    GUAVA_FUTURE,
    STREAM,
    NONE
  }
}
