/**
 * Builder pattern classes to handle configuration.
 *
 * @since 1.0
 * @author Stephan Holzner
 * @version 1.0
 */
package org.sosy_lab.pjbdd;
