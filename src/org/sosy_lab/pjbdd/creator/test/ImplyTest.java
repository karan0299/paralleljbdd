package org.sosy_lab.pjbdd.creator.test;

import static org.junit.Assert.assertEquals;

import org.junit.Assert;
import org.sosy_lab.pjbdd.node.BDD;
import org.sosy_lab.pjbdd.test.CreatorCombinatorTest;

/**
 * Make 'IMPLY' test class uses {@link CreatorCombinatorTest} as base class to perform make 'IMPLY'
 * test for all known creators.
 *
 * @author Stephan Holzner
 * @see CreatorCombinatorTest
 * @since 1.0
 */
public class ImplyTest extends CreatorCombinatorTest {
  /** {@inheritDoc} */
  @Override
  public void test() {
    BDD var1 = creator.makeVariable();
    BDD var2 = creator.makeVariable();

    BDD res1 = creator.makeImply(var1, creator.makeTrue());
    Assert.assertEquals(res1, creator.makeTrue());

    BDD res2 = creator.makeImply(creator.makeFalse(), var2);
    Assert.assertEquals(res2, creator.makeTrue());

    BDD res3 = creator.makeImply(creator.makeTrue(), var2);
    assertEquals(res3, var2);
    creator.shutDown();
  }
}
