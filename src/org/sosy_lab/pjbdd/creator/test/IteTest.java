package org.sosy_lab.pjbdd.creator.test;

import static org.junit.Assert.assertEquals;

import org.sosy_lab.pjbdd.node.BDD;
import org.sosy_lab.pjbdd.test.CreatorCombinatorTest;

/**
 * Make 'ITE' test class uses {@link CreatorCombinatorTest} as base class to perform make 'ITE' test
 * for all known creators.
 *
 * @author Stephan Holzner
 * @see CreatorCombinatorTest
 * @since 1.0
 */
public class IteTest extends CreatorCombinatorTest {

  /** {@inheritDoc} */
  @Override
  public void test() {
    BDD i = creator.makeVariable();
    BDD t = creator.makeVariable();
    BDD e = creator.makeVariable();

    BDD res1 = creator.makeIte(i, t, t);
    assertEquals(res1, t);

    BDD res2 = creator.makeIte(creator.makeTrue(), t, e);
    assertEquals(res2, t);

    BDD res3 = creator.makeIte(creator.makeFalse(), t, e);
    assertEquals(res3, e);

    BDD res4 = creator.makeIte(i, creator.makeTrue(), creator.makeFalse());
    assertEquals(res4, i);

    BDD notI = creator.makeNot(i);
    BDD res5 = creator.makeIte(i, creator.makeFalse(), creator.makeTrue());
    assertEquals(res5, notI);
    creator.shutDown();
  }
}
