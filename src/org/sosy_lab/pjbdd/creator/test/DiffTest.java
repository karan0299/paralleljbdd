package org.sosy_lab.pjbdd.creator.test;

import static org.junit.Assert.assertEquals;

import org.sosy_lab.pjbdd.node.BDD;
import org.sosy_lab.pjbdd.test.ZDDCombinatorTest;

public class DiffTest extends ZDDCombinatorTest {
  @Override
  public void test() {
    BDD var0 = creator.makeNode(creator.empty(), creator.base(), 0);
    BDD var1 = creator.makeNode(creator.empty(), creator.base(), 1);
    BDD var2 = creator.makeNode(creator.empty(), creator.base(), 2);

    BDD bdd0 = creator.union(var2, var1);
    BDD bdd1 = creator.union(bdd0, var0);

    BDD diff0 = creator.difference(creator.empty(), bdd1);
    assertEquals(creator.empty(), diff0);

    BDD diff1 = creator.difference(bdd1, creator.empty());
    assertEquals(bdd1, diff1);

    BDD diff2 = creator.difference(bdd1, bdd1);
    assertEquals(creator.empty(), diff2);
  }
}
