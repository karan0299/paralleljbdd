package org.sosy_lab.pjbdd.creator.test;

import static org.junit.Assert.assertEquals;

import org.junit.Assert;
import org.sosy_lab.pjbdd.node.BDD;
import org.sosy_lab.pjbdd.test.CreatorCombinatorTest;

/**
 * Make 'NOT' test class uses {@link CreatorCombinatorTest} as base class to perform make 'NOT' test
 * for all known creators.
 *
 * @author Stephan Holzner
 * @see CreatorCombinatorTest
 * @since 1.0
 */
public class NotTest extends CreatorCombinatorTest {

  /** {@inheritDoc} */
  @Override
  public void test() {
    BDD bdd = creator.makeVariable();
    BDD not = creator.makeNot(bdd);

    assertEquals(bdd.getHigh(), not.getLow());
    assertEquals(bdd.getLow(), not.getHigh());

    BDD notZero = creator.makeNot(creator.makeFalse());
    Assert.assertEquals(creator.makeTrue(), notZero);

    BDD notOne = creator.makeNot(creator.makeTrue());
    Assert.assertEquals(creator.makeFalse(), notOne);
    creator.shutDown();
  }
}
