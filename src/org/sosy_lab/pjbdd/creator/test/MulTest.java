package org.sosy_lab.pjbdd.creator.test;

import static org.junit.Assert.assertEquals;

import org.junit.Assert;
import org.sosy_lab.pjbdd.node.BDD;
import org.sosy_lab.pjbdd.test.ZDDCombinatorTest;

public class MulTest extends ZDDCombinatorTest {
  @Override
  public void test() {
    BDD var0 = creator.makeNode(creator.empty(), creator.base(), 0);
    BDD var1 = creator.makeNode(creator.empty(), creator.base(), 1);
    BDD var2 = creator.makeNode(creator.empty(), creator.base(), 2);

    BDD bdd0 = creator.union(var2, var1);
    BDD bdd1 = creator.union(bdd0, var0);

    BDD mul0 = creator.product(creator.empty(), bdd1);
    Assert.assertEquals(creator.empty(), mul0);

    BDD mul1 = creator.product(bdd1, creator.empty());
    Assert.assertEquals(creator.empty(), mul1);

    BDD mul2 = creator.product(bdd1, creator.base());
    assertEquals(bdd1, mul2);

    BDD mul3 = creator.product(creator.base(), bdd1);
    assertEquals(bdd1, mul3);
  }
}
