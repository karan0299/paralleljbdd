package org.sosy_lab.pjbdd.creator.test;

import static org.junit.Assert.assertEquals;

import org.junit.Assert;
import org.sosy_lab.pjbdd.node.BDD;
import org.sosy_lab.pjbdd.test.CreatorCombinatorTest;

/**
 * Make 'XOR' test class uses {@link CreatorCombinatorTest} as base class to perform make 'XOR' test
 * for all known creators.
 *
 * @author Stephan Holzner
 * @see CreatorCombinatorTest
 * @since 1.0
 */
public class XorTest extends CreatorCombinatorTest {
  /** {@inheritDoc} */
  @Override
  public void test() {
    BDD var1 = creator.makeVariable();
    BDD var2 = creator.makeVariable();

    BDD res1 = creator.makeXor(var1, var1);
    Assert.assertEquals(res1, creator.makeFalse());

    BDD res2 = creator.makeXor(creator.makeFalse(), var2);
    assertEquals(res2, var2);

    BDD res3 = creator.makeXor(var1, creator.makeFalse());
    assertEquals(res3, var1);
    creator.shutDown();
  }
}
