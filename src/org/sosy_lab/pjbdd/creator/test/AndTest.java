package org.sosy_lab.pjbdd.creator.test;

import static org.junit.Assert.assertEquals;

import org.junit.Assert;
import org.sosy_lab.pjbdd.node.BDD;
import org.sosy_lab.pjbdd.test.CreatorCombinatorTest;

/**
 * Make 'AND' test class uses {@link CreatorCombinatorTest} as base class to perform make 'AND' test
 * for all known creators.
 *
 * @author Stephan Holzner
 * @see CreatorCombinatorTest
 * @since 1.0
 */
public class AndTest extends CreatorCombinatorTest {

  /** {@inheritDoc} */
  @Override
  public void test() {
    BDD var1 = creator.makeVariable();
    BDD var2 = creator.makeVariable();

    BDD res1 = creator.makeAnd(var1, var1);
    assertEquals(res1, var1);

    BDD res2 = creator.makeAnd(var1, creator.makeTrue());
    assertEquals(res2, var1);

    BDD res3 = creator.makeAnd(var1, creator.makeFalse());
    Assert.assertEquals(res3, creator.makeFalse());

    BDD res4 = creator.makeAnd(creator.makeFalse(), var1);
    Assert.assertEquals(res4, creator.makeFalse());

    BDD res5 = creator.makeAnd(creator.makeTrue(), var2);
    assertEquals(res5, var2);

    BDD res = creator.makeAnd(var1, var2);
    assertEquals(res.getVariable(), var1.getVariable());
    Assert.assertEquals(res.getLow(), creator.makeFalse());
    assertEquals(res.getHigh(), var2);
    creator.shutDown();
  }
}
