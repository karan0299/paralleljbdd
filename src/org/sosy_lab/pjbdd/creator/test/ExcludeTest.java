package org.sosy_lab.pjbdd.creator.test;

import static org.junit.Assert.assertEquals;

import org.junit.Assert;
import org.sosy_lab.pjbdd.node.BDD;
import org.sosy_lab.pjbdd.test.ZDDCombinatorTest;

public class ExcludeTest extends ZDDCombinatorTest {
  @Override
  public void test() {
    BDD var0 = creator.makeNode(creator.empty(), creator.base(), 0);
    BDD var1 = creator.makeNode(creator.empty(), creator.base(), 1);
    BDD var2 = creator.makeNode(creator.empty(), creator.base(), 2);

    BDD bdd0 = creator.union(var2, var1);
    BDD bdd1 = creator.union(bdd0, var0);

    BDD exclude0 = creator.exclude(creator.empty(), bdd1);
    Assert.assertEquals(creator.empty(), exclude0);

    BDD exclude1 = creator.exclude(bdd1, creator.base());
    Assert.assertEquals(creator.empty(), exclude1);

    BDD exclude2 = creator.exclude(bdd1, bdd1);
    Assert.assertEquals(creator.empty(), exclude2);

    BDD exclude3 = creator.exclude(creator.base(), bdd1);
    Assert.assertEquals(creator.base(), exclude3);

    BDD exclude4 = creator.exclude(bdd1, creator.empty());
    assertEquals(bdd1, exclude4);
  }
}
