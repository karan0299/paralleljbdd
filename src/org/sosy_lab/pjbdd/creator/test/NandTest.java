package org.sosy_lab.pjbdd.creator.test;

import org.junit.Assert;
import org.sosy_lab.pjbdd.node.BDD;
import org.sosy_lab.pjbdd.test.CreatorCombinatorTest;

/**
 * Make 'NAND' test class uses {@link CreatorCombinatorTest} as base class to perform make 'NAND'
 * test for all known creators.
 *
 * @author Stephan Holzner
 * @see CreatorCombinatorTest
 * @since 1.0
 */
public class NandTest extends CreatorCombinatorTest {
  /** {@inheritDoc} */
  @Override
  public void test() {
    BDD var1 = creator.makeVariable();
    BDD var2 = creator.makeVariable();
    BDD res1 = creator.makeNand(var1, creator.makeFalse());
    Assert.assertEquals(res1, creator.makeTrue());

    BDD res2 = creator.makeNand(creator.makeFalse(), var2);
    Assert.assertEquals(res2, creator.makeTrue());
    creator.shutDown();
  }
}
