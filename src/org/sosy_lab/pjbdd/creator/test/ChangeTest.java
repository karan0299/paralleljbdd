package org.sosy_lab.pjbdd.creator.test;

import static org.junit.Assert.assertEquals;

import org.sosy_lab.pjbdd.node.BDD;
import org.sosy_lab.pjbdd.test.ZDDCombinatorTest;

public class ChangeTest extends ZDDCombinatorTest {
  @Override
  public void test() {
    BDD var0 = creator.makeNode(creator.empty(), creator.base(), 0);
    BDD var1 = creator.makeNode(creator.empty(), creator.base(), 1);
    BDD var2 = creator.makeNode(creator.empty(), creator.base(), 2);

    BDD bdd0 = creator.union(var2, var1);
    BDD bdd1 = creator.union(bdd0, var0);

    BDD change0 = creator.change(bdd0, var0);
    assertEquals(var0.getVariable(), change0.getVariable());
    assertEquals(creator.empty(), change0.getLow());
    assertEquals(bdd0, change0.getHigh());

    BDD change1 = creator.change(bdd1, var0);
    assertEquals(var0.getVariable(), change1.getVariable());
    assertEquals(bdd1.getLow(), change1.getHigh());
    assertEquals(bdd1.getHigh(), change1.getLow());
  }
}
