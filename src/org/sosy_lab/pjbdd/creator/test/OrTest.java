package org.sosy_lab.pjbdd.creator.test;

import static org.junit.Assert.assertEquals;

import org.junit.Assert;
import org.sosy_lab.pjbdd.node.BDD;
import org.sosy_lab.pjbdd.test.CreatorCombinatorTest;

/**
 * Make 'OR' test class uses {@link CreatorCombinatorTest} as base class to perform make 'OR' test
 * for all known creators.
 *
 * @author Stephan Holzner
 * @see CreatorCombinatorTest
 * @since 1.0
 */
public class OrTest extends CreatorCombinatorTest {
  /** {@inheritDoc} */
  @Override
  public void test() {
    BDD var1 = creator.makeVariable();
    BDD var2 = creator.makeVariable();

    BDD res1 = creator.makeOr(var1, var1);
    assertEquals(res1, var1);

    BDD res2 = creator.makeOr(var1, creator.makeFalse());
    assertEquals(res2, var1);

    BDD res3 = creator.makeOr(var1, creator.makeTrue());
    Assert.assertEquals(res3, creator.makeTrue());

    BDD res4 = creator.makeOr(creator.makeTrue(), var2);
    Assert.assertEquals(res4, creator.makeTrue());

    BDD res5 = creator.makeOr(creator.makeFalse(), var2);
    assertEquals(res5, var2);
    creator.shutDown();
  }
}
