package org.sosy_lab.pjbdd.creator.test;

import static org.junit.Assert.assertEquals;

import org.sosy_lab.pjbdd.node.BDD;
import org.sosy_lab.pjbdd.test.ZDDCombinatorTest;

public class DivTest extends ZDDCombinatorTest {
  @Override
  public void test() {
    BDD var0 = creator.makeNode(creator.empty(), creator.base(), 0);
    BDD var1 = creator.makeNode(creator.empty(), creator.base(), 1);
    BDD var2 = creator.makeNode(creator.empty(), creator.base(), 2);

    BDD bdd0 = creator.union(var2, var1);
    BDD bdd1 = creator.union(bdd0, var0);

    BDD div0 = creator.division(creator.empty(), bdd1);
    assertEquals(creator.empty(), div0);

    BDD div1 = creator.division(creator.base(), bdd1);
    assertEquals(creator.empty(), div1);

    BDD div2 = creator.division(bdd1, bdd1);
    assertEquals(creator.base(), div2);

    BDD div3 = creator.division(bdd1, creator.base());
    assertEquals(bdd1, div3);
  }
}
