package org.sosy_lab.pjbdd.creator.test;

import org.junit.Assert;
import org.sosy_lab.pjbdd.node.BDD;
import org.sosy_lab.pjbdd.test.CreatorCombinatorTest;

/**
 * Make 'NOR' test class uses {@link CreatorCombinatorTest} as base class to perform make 'NOR' test
 * for all known creators.
 *
 * @author Stephan Holzner
 * @see CreatorCombinatorTest
 * @since 1.0
 */
public class NorTest extends CreatorCombinatorTest {

  /** {@inheritDoc} */
  @Override
  public void test() {
    BDD var1 = creator.makeVariable();
    BDD var2 = creator.makeVariable();

    BDD res1 = creator.makeNor(var1, creator.makeTrue());
    Assert.assertEquals(res1, creator.makeFalse());

    BDD res2 = creator.makeNor(creator.makeTrue(), var2);
    Assert.assertEquals(res2, creator.makeFalse());
    creator.shutDown();
  }
}
