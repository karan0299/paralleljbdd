package org.sosy_lab.pjbdd.creator.test;

import static org.junit.Assert.assertEquals;

import org.junit.Assert;
import org.sosy_lab.pjbdd.node.BDD;
import org.sosy_lab.pjbdd.test.ZDDCombinatorTest;

public class IntsecTest extends ZDDCombinatorTest {
  @Override
  public void test() {
    BDD var0 = creator.makeNode(creator.empty(), creator.base(), 0);
    BDD var1 = creator.makeNode(creator.empty(), creator.base(), 1);
    BDD var2 = creator.makeNode(creator.empty(), creator.base(), 2);

    BDD bdd0 = creator.union(var2, var1);
    BDD bdd1 = creator.union(bdd0, var0);

    BDD intsec0 = creator.intersection(creator.empty(), bdd1);
    Assert.assertEquals(creator.empty(), intsec0);

    BDD intsec1 = creator.intersection(bdd1, creator.empty());
    Assert.assertEquals(creator.empty(), intsec1);

    BDD intsec2 = creator.intersection(bdd1, bdd1);
    assertEquals(bdd1, intsec2);
  }
}
