package org.sosy_lab.pjbdd.creator.test;

import static org.junit.Assert.assertEquals;

import org.sosy_lab.pjbdd.node.BDD;
import org.sosy_lab.pjbdd.test.ZDDCombinatorTest;

public class RestrictTest extends ZDDCombinatorTest {
  @Override
  public void test() {
    BDD var0 = creator.makeNode(creator.empty(), creator.base(), 0);
    BDD var1 = creator.makeNode(creator.empty(), creator.base(), 1);
    BDD var2 = creator.makeNode(creator.empty(), creator.base(), 2);

    BDD bdd0 = creator.union(var2, var1);
    BDD bdd1 = creator.union(bdd0, var0);

    BDD restrict = creator.restrict(creator.empty(), bdd1);
    assertEquals(creator.empty(), restrict);

    BDD restrict1 = creator.restrict(bdd1, creator.empty());
    assertEquals(creator.empty(), restrict1);

    BDD restrict2 = creator.restrict(bdd1, bdd1);
    assertEquals(bdd1, restrict2);
  }
}
