package org.sosy_lab.pjbdd.creator.test;

import static org.junit.Assert.assertEquals;

import org.sosy_lab.pjbdd.node.BDD;
import org.sosy_lab.pjbdd.test.CreatorCombinatorTest;

/**
 * Make 'EXISTENTIAL QUANTIFICATION' test class uses {@link CreatorCombinatorTest} as base class to
 * perform make 'EXISTENTIAL QUANTIFICATION' test for all known creators.
 *
 * @author Stephan Holzner
 * @see CreatorCombinatorTest
 * @since 1.0
 */
public class ExistsTest extends CreatorCombinatorTest {

  /** {@inheritDoc} */
  @Override
  public void test() {
    BDD bdd0 = creator.makeVariable();
    BDD bdd1 = creator.makeVariable();
    BDD bdd2 = creator.makeVariable();

    BDD conjunction = bdd2;
    conjunction = creator.makeAnd(conjunction, bdd1);
    conjunction = creator.makeAnd(conjunction, bdd0);

    BDD exquant = creator.makeExists(conjunction, bdd2, bdd1);
    assertEquals(exquant, bdd0);

    BDD exquant2 = creator.makeExists(creator.makeTrue(), creator.makeVariable());
    assertEquals(creator.makeTrue(), exquant2);

    BDD bdd3 = creator.makeVariable();
    BDD bdd4 = creator.makeVariable();

    BDD dis = creator.makeOr(bdd0, creator.makeOr(bdd1, creator.makeOr(bdd2, bdd4)));

    BDD exquant3 = creator.makeExists(dis, bdd3);

    assertEquals(dis, exquant3);

    creator.shutDown();
  }
}
