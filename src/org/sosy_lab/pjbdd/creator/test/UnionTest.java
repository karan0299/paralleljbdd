package org.sosy_lab.pjbdd.creator.test;

import static org.junit.Assert.assertEquals;

import org.sosy_lab.pjbdd.node.BDD;
import org.sosy_lab.pjbdd.test.ZDDCombinatorTest;

public class UnionTest extends ZDDCombinatorTest {
  @Override
  public void test() {
    BDD var0 = creator.makeNode(creator.empty(), creator.base(), 0);
    BDD var1 = creator.makeNode(creator.empty(), creator.base(), 1);
    BDD var2 = creator.makeNode(creator.empty(), creator.base(), 2);

    BDD bdd0 = creator.union(var2, var1);
    BDD bdd1 = creator.union(bdd0, var0);

    BDD union0 = creator.union(creator.empty(), bdd1);
    assertEquals(bdd1, union0);

    BDD union1 = creator.union(bdd1, creator.empty());
    assertEquals(bdd1, union1);

    BDD union2 = creator.union(bdd1, bdd1);
    assertEquals(bdd1, union2);
  }
}
