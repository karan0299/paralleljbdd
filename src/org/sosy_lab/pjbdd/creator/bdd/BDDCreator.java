package org.sosy_lab.pjbdd.creator.bdd;

import static org.sosy_lab.pjbdd.algorithm.bdd.BDDAlgorithm.ApplyOp.OP_AND;
import static org.sosy_lab.pjbdd.algorithm.bdd.BDDAlgorithm.ApplyOp.OP_OR;

import java.math.BigInteger;
import java.util.Set;
import java.util.TreeSet;
import org.sosy_lab.pjbdd.algorithm.bdd.BDDAlgorithm;
import org.sosy_lab.pjbdd.algorithm.sat.SatAlgorithm;
import org.sosy_lab.pjbdd.node.BDD;
import org.sosy_lab.pjbdd.node.manager.bdd.NodeManager;
import org.sosy_lab.pjbdd.util.IfThenElseData;
import org.sosy_lab.pjbdd.util.IntArrayUtils;

/**
 * {@link Creator} implementation with {@link BDD} objects as bdd data structure.
 *
 * @author Stephan Holzner
 * @see Creator
 * @since 1.0
 */
public class BDDCreator extends AbstractCreator implements Creator {

  protected BDDAlgorithm algorithm;

  /**
   * Creates new BDDCreator environment.
   *
   * @param nodeManager - the node manager to use
   * @param algorithm - the manipulation algorithm to use
   * @param satAlgorithm the sat algorithm to use
   */
  BDDCreator(NodeManager nodeManager, BDDAlgorithm algorithm, SatAlgorithm satAlgorithm) {
    super(nodeManager, satAlgorithm);
    this.algorithm = algorithm;
  }

  /** {@inheritDoc} */
  @Override
  public BDD makeNot(BDD f) {
    return algorithm.makeNot(f);
  }

  /** {@inheritDoc} */
  @Override
  public BDD makeAnd(BDD f1, BDD f2) {
    return makeOp(f1, f2, BDDAlgorithm.ApplyOp.OP_AND);
  }

  /** {@inheritDoc} */
  @Override
  public BDD makeOr(BDD f1, BDD f2) {
    return makeOp(f1, f2, OP_OR);
  }

  /** {@inheritDoc} */
  @Override
  public BDD makeXor(BDD f1, BDD f2) {
    return makeOp(f1, f2, BDDAlgorithm.ApplyOp.OP_XOR);
  }

  /** {@inheritDoc} */
  @Override
  public BDD makeNor(BDD f1, BDD f2) {
    return makeOp(f1, f2, BDDAlgorithm.ApplyOp.OP_NOR);
  }

  /** {@inheritDoc} */
  @Override
  public BDD makeXnor(BDD f1, BDD f2) {
    return makeOp(f1, f2, BDDAlgorithm.ApplyOp.OP_XNOR);
  }

  /** {@inheritDoc} */
  @Override
  public BDD makeNand(BDD f1, BDD f2) {
    return makeOp(f1, f2, BDDAlgorithm.ApplyOp.OP_NAND);
  }

  /** {@inheritDoc} */
  @Override
  public BDD makeEqual(BDD f1, BDD f2) {
    return makeXnor(f1, f2);
  }

  /** {@inheritDoc} */
  @Override
  public BDD makeUnequal(BDD f1, BDD f2) {
    return makeXor(f1, f2);
  }

  /** {@inheritDoc} */
  @Override
  public BDD makeImply(BDD f1, BDD f2) {
    return makeOp(f1, f2, BDDAlgorithm.ApplyOp.OP_IMP);
  }

  /**
   * Operation delegate method to algorithm implementation.
   *
   * @param f1 - first bdd argument
   * @param f2 - second bdd argument
   * @param op - operation to be performed
   * @return f1 op f2
   */
  private BDD makeOp(BDD f1, BDD f2, BDDAlgorithm.ApplyOp op) {
    reorderLock.readLock().lock();
    try {
      return algorithm.makeOp(f1, f2, op);
    } finally {
      reorderLock.readLock().unlock();
    }
  }

  /** {@inheritDoc} */
  @Override
  public BDD anySat(BDD bdd) {
    reorderLock.readLock().lock();
    try {
      return satAlgorithm.anySat(bdd);
    } finally {
      reorderLock.readLock().unlock();
    }
  }

  /** {@inheritDoc} */
  @Override
  public BigInteger satCount(BDD b) {
    reorderLock.readLock().lock();
    try {
      return satAlgorithm.satCount(b);
    } finally {
      reorderLock.readLock().unlock();
    }
  }

  /** {@inheritDoc} */
  @Override
  public Stats getCreatorStats() {
    throw new UnsupportedOperationException();
  }

  /** {@inheritDoc} */
  @Override
  public IfThenElseData getIfThenElse(BDD f) {
    return new IfThenElseData(makeIthVar(f.getVariable()), getHigh(f), getLow(f));
  }

  @Override
  public BDD makeNode(BDD low, BDD high, int var) {
    return nodeManager.makeNode(low, high, var);
  }

  /** {@inheritDoc} */
  @Override
  public boolean entails(BDD f1, BDD f2) {
    boolean entails;
    reorderLock.readLock().lock();
    try {
      entails = entailsUnblocking(f1, f2);
    } finally {
      reorderLock.readLock().unlock();
    }
    return entails;
  }

  /**
   * Helper method for recursive entails operations to avoid recursive acquire of resize lock.
   *
   * @param f1 - the first argument
   * @param f2 - the second argument
   * @return true if f1 entails f2
   */
  private boolean entailsUnblocking(BDD f1, BDD f2) {
    if (f1.equals(f2)) {
      return true;
    }
    if (f1.isLeaf()) {
      return false;
    }
    if (level(f1) <= level(f2)) {
      return false;
    }
    return entailsUnblocking(f1.getLow(), f2) || entailsUnblocking(f1.getHigh(), f2);
  }

  private int level(BDD node) {
    return nodeManager.level(node.getVariable());
  }

  /** {@inheritDoc} */
  @Override
  public BDD makeExists(BDD f1, BDD... f2) {
    reorderLock.readLock().lock();
    try {

      BDD fTemp = f2[0];
      for (BDD f : f2) {
        fTemp = makeOp(fTemp, f, OP_AND);
      }
      Set<Integer> varSet = createHighVarLevelSet(fTemp);
      f1 = algorithm.makeExists(f1, IntArrayUtils.toIntArray(varSet));
    } finally {
      reorderLock.readLock().unlock();
    }
    return f1;
  }

  /**
   * Takes a {@link BDD} and creates a set of all high branch variables.
   *
   * @param f - the {@link BDD} argument
   * @return a set of variables
   */
  private Set<Integer> createHighVarLevelSet(BDD f) {
    Set<Integer> varLevelSet = new TreeSet<>();
    while (!f.isLeaf()) {
      varLevelSet.add(level(f));
      f = f.getHigh();
    }
    return varLevelSet;
  }

  /** {@inheritDoc} */
  @Override
  public BDD getLow(BDD top) {
    return top.getLow();
  }

  /** {@inheritDoc} */
  @Override
  public BDD getHigh(BDD top) {
    return top.getHigh();
  }

  /** {@inheritDoc} */
  @Override
  public BDD makeTrue() {
    return nodeManager.getTrue();
  }

  /** {@inheritDoc} */
  @Override
  public BDD makeFalse() {
    return nodeManager.getFalse();
  }

  /** {@inheritDoc} */
  @Override
  public BDD makeIte(BDD f1, BDD f2, BDD f3) {
    reorderLock.readLock().lock();
    try {
      return algorithm.makeIte(f1, f2, f3);
    } finally {
      reorderLock.readLock().unlock();
    }
  }
  /** {@inheritDoc} */
  @Override
  public BDD makeVariable() {
    return nodeManager.makeNext();
  }

  /** {@inheritDoc} */
  @Override
  public BDD restrict(BDD bdd, int var, boolean restrictionVar) {
    return bdd.getVariable() != var ? bdd : restrictionVar ? bdd.getHigh() : bdd.getLow();
  }
  /** {@inheritDoc} */
  @Override
  public String toString() {
    return this.getClass().getSimpleName();
  }
}
