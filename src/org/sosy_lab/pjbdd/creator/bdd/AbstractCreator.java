package org.sosy_lab.pjbdd.creator.bdd;

import java.util.List;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import org.sosy_lab.pjbdd.algorithm.sat.SatAlgorithm;
import org.sosy_lab.pjbdd.node.manager.bdd.NodeManager;

public abstract class AbstractCreator {

  /** lock object for reorder operations: read lock for normal operations and write for reorder. */
  protected final ReadWriteLock reorderLock;

  protected final NodeManager nodeManager;

  protected final SatAlgorithm satAlgorithm;

  /**
   * Creates new {@link BDDCreator} instances with given parameters.
   *
   * @param nodeManager - the node manager used
   * @param satAlgorithm - the sat algorithm used
   */
  protected AbstractCreator(NodeManager nodeManager, SatAlgorithm satAlgorithm) {
    this.reorderLock = new ReentrantReadWriteLock();
    this.nodeManager = nodeManager;
    this.satAlgorithm = satAlgorithm;
  }

  /** shutdown creator. */
  public void shutDown() {
    nodeManager.shutdown();
  }

  /**
   * set new variable order.
   *
   * @param pOrder - the new order
   */
  public void setVarOrder(List<Integer> pOrder) {
    reorderLock.writeLock().lock();
    try {
      nodeManager.setVarOrder(pOrder);
    } finally {
      reorderLock.writeLock().unlock();
    }
  }

  /**
   * get current variable count.
   *
   * @return current variable count
   */
  public int getVariableCount() {
    return nodeManager.getVarCount();
  }

  /**
   * get current variable order.
   *
   * @return current variable order.
   */
  public int[] getVariableOrdering() {
    return nodeManager.getCurrentOrdering();
  }

  /**
   * set new variable count.
   *
   * @param count - the new count.
   */
  public void setVariableCount(int count) {
    reorderLock.readLock().lock();
    try {
      nodeManager.setVarCount(count);
    } finally {
      reorderLock.readLock().unlock();
    }
  }
}
