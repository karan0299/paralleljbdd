package org.sosy_lab.pjbdd.creator.bdd;

import java.math.BigInteger;
import org.sosy_lab.pjbdd.Builders;
import org.sosy_lab.pjbdd.algorithm.bdd.BDDAlgorithm;
import org.sosy_lab.pjbdd.algorithm.bdd.apply.ApplyBDDAlgorithm;
import org.sosy_lab.pjbdd.algorithm.bdd.apply.CompletableFutureApplyAlgorithm;
import org.sosy_lab.pjbdd.algorithm.bdd.apply.ForkJoinApplyAlgorithm;
import org.sosy_lab.pjbdd.algorithm.bdd.ite.CompletableFutureITEAlgorithm;
import org.sosy_lab.pjbdd.algorithm.bdd.ite.ForkJoinITEAlgorithm;
import org.sosy_lab.pjbdd.algorithm.bdd.ite.FutureITEAlgorithm;
import org.sosy_lab.pjbdd.algorithm.bdd.ite.GuavaFutureITEAlgorithm;
import org.sosy_lab.pjbdd.algorithm.bdd.ite.ITEBDDAlgorithm;
import org.sosy_lab.pjbdd.algorithm.bdd.ite.StreamITEAlgorithm;
import org.sosy_lab.pjbdd.algorithm.sat.BDDSat;
import org.sosy_lab.pjbdd.cache.bdd.ArrayCache;
import org.sosy_lab.pjbdd.cache.bdd.Cache;
import org.sosy_lab.pjbdd.cache.bdd.GuavaCache;
import org.sosy_lab.pjbdd.creator.Builder;
import org.sosy_lab.pjbdd.node.BDD;
import org.sosy_lab.pjbdd.node.BDDNode;
import org.sosy_lab.pjbdd.node.manager.bdd.BDDNodeManager;
import org.sosy_lab.pjbdd.node.manager.bdd.NodeManager;
import org.sosy_lab.pjbdd.uniquetable.bdd.BDDConcurrentArray;
import org.sosy_lab.pjbdd.uniquetable.bdd.BDDConcurrentWeakHashDeque;
import org.sosy_lab.pjbdd.uniquetable.bdd.BDDConcurrentWeakHashMap;
import org.sosy_lab.pjbdd.uniquetable.bdd.BDDLockOnWriteArray;
import org.sosy_lab.pjbdd.uniquetable.bdd.UniqueTable;
import org.sosy_lab.pjbdd.util.threadpool.ParallelismManager;
import org.sosy_lab.pjbdd.util.threadpool.ParallelismManagerImpl;

/**
 * Builder class to create {@link Creator} environments implemented with {@link BDDCreator}.
 *
 * @author Stephan Holzner
 * @see Creator
 * @see BDDCreator
 * @since 1.0
 */
public class BDDBuilder extends Builder {

  /** selected parallelization type. */
  protected Builders.ParallelizationType parallelizationType =
      Builders.ParallelizationType.FORK_JOIN;
  /** selected uniquetabble instance. */
  protected UniqueTable<BDD> selectedTable;
  /** use apply or ite algorithm. */
  protected boolean useApply = true;
  /** selected table type. */
  protected Builders.TableType tableType = Builders.TableType.Array;
  /** selected bdd instantiation factory. */
  protected BDDNode.Factory selectedFactory = new BDDNode.Factory();

  /**
   * Creates a new BDDCreator environment with previously set parameters.
   *
   * @return a new BDD environment
   */
  public Creator build() {

    NodeManager nodeManager = new BDDNodeManager(makeTable());
    nodeManager.setVarCount(this.selectedVarCount);

    Cache<Integer, Cache.CacheData> cache = new ArrayCache();
    Cache<BDD, BigInteger> satCache = new GuavaCache<>();
    cache.init(this.selectedCacheSize, this.selectedParallelism);
    satCache.init(this.selectedCacheSize, this.selectedParallelism);

    BDDAlgorithm algorithm = null;
    if (useApply) {
      switch (parallelizationType) {
        case COMPLETABLE_FUTURE:
          algorithm = new CompletableFutureApplyAlgorithm(cache, nodeManager, parallelismManager);
          break;
        case FORK_JOIN:
          algorithm = new ForkJoinApplyAlgorithm(cache, nodeManager, parallelismManager);
          break;
        default:
          algorithm = new ApplyBDDAlgorithm(cache, nodeManager);
          break;
      }
    } else {

      switch (parallelizationType) {
        case NONE:
          algorithm = new ITEBDDAlgorithm(cache, nodeManager);
          break;
        case FUTURE:
          algorithm = new FutureITEAlgorithm(cache, nodeManager, parallelismManager);
          break;
        case STREAM:
          algorithm = new StreamITEAlgorithm(cache, nodeManager, parallelismManager);
          break;
        case FORK_JOIN:
          algorithm = new ForkJoinITEAlgorithm(cache, nodeManager, parallelismManager);
          break;
        case GUAVA_FUTURE:
          algorithm = new GuavaFutureITEAlgorithm(cache, nodeManager, parallelismManager);
          break;
        case COMPLETABLE_FUTURE:
          algorithm = new CompletableFutureITEAlgorithm(cache, nodeManager, parallelismManager);
          break;
      }
    }
    return new BDDCreator(nodeManager, algorithm, new BDDSat(satCache, nodeManager));
  }

  public BDDBuilder setSelectedParallelism(int selectedParallelism) {
    this.selectedParallelism = selectedParallelism;
    return this;
  }

  public BDDBuilder setSelectedCacheSize(int selectedCacheSize) {
    this.selectedCacheSize = selectedCacheSize;
    return this;
  }

  public BDDBuilder setUseApply(boolean apply) {
    this.useApply = apply;
    return this;
  }

  public BDDBuilder setTableType(Builders.TableType type) {
    this.tableType = type;
    return this;
  }

  public BDDBuilder setSelectedFactory(BDDNode.Factory factory) {
    this.selectedFactory = factory;
    return this;
  }

  public BDDBuilder setParallelismManager(ParallelismManager manager) {
    this.parallelismManager = manager;
    return this;
  }

  public BDDBuilder setParallelism(int parallelism) {
    this.selectedParallelism = parallelism;
    return this;
  }

  public BDDBuilder setTableSize(int tableSize) {
    this.selectedTableSize = tableSize;
    return this;
  }

  public BDDBuilder setCacheSize(int cacheSize) {
    this.selectedCacheSize = cacheSize;
    return this;
  }

  public BDDBuilder setThreads(int selectedThreads) {
    if (selectedThreads != this.selectedThreads) {
      parallelismManager = new ParallelismManagerImpl(selectedThreads);
    }
    this.selectedThreads = selectedThreads;
    return this;
  }

  public BDDBuilder setVarCount(int selectedVarCount) {
    this.selectedVarCount = selectedVarCount;
    return this;
  }

  public BDDBuilder setIncreaseFactor(int selectedIncreaseFactor) {
    this.selectedIncreaseFactor = selectedIncreaseFactor;
    return this;
  }

  public BDDBuilder setParallelizationType(Builders.ParallelizationType type) {
    this.parallelizationType = type;
    return this;
  }

  public BDDBuilder setSelectedTable(UniqueTable<BDD> uniqueTable) {
    this.selectedTable = uniqueTable;
    return this;
  }

  protected UniqueTable<BDD> makeTable() {
    if (selectedTable != null) {
      return selectedTable;
    }
    switch (tableType) {
      case RWLockArray:
        return new BDDLockOnWriteArray(selectedIncreaseFactor, selectedTableSize, selectedFactory);
      case Array:
        return new BDDConcurrentArray(
            selectedParallelism, selectedIncreaseFactor, selectedTableSize, selectedFactory);
      case ConcurrentHashBucket:
        return new BDDConcurrentWeakHashDeque(
            selectedTableSize, selectedParallelism, selectedFactory);
      default:
        return new BDDConcurrentWeakHashMap(selectedTableSize, selectedParallelism);
    }
  }
}
