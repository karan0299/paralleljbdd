package org.sosy_lab.pjbdd.creator.bdd;

import java.math.BigInteger;
import java.util.List;
import org.sosy_lab.pjbdd.node.BDD;
import org.sosy_lab.pjbdd.util.IfThenElseData;

/**
 * Main {@link BDD} boolean_operations interface defining the following {@link BDD} operations.
 *
 * <ul>
 *   <li>{@link #makeVariable()}
 *   <li>{@link #restrict(BDD, int, boolean)}
 *   <li>{@link #anySat(BDD)}
 *   <li>{@link #satCount(BDD)}
 *   <li>{@link #makeTrue()}
 *   <li>{@link #makeFalse()}
 *   <li>{@link #makeNot(BDD)}
 *   <li>{@link #makeAnd(BDD, BDD)}
 *   <li>{@link #makeOr(BDD, BDD)}
 *   <li>{@link #makeXor(BDD, BDD)}
 *   <li>{@link #makeNor(BDD, BDD)}
 *   <li>{@link #makeXnor(BDD, BDD)}
 *   <li>{@link #makeNand(BDD, BDD)}
 *   <li>{@link #makeEqual(BDD, BDD)}
 *   <li>{@link #makeUnequal(BDD, BDD)}
 *   <li>{@link #makeIte(BDD, BDD, BDD)}
 *   <li>{@link #makeImply(BDD, BDD)}
 *   <li>{@link #makeExists(BDD, BDD...)}
 *   <li>{@link #entails(BDD, BDD)}
 *   <li>{@link #makeNode(BDD, BDD, int)}
 * </ul>
 *
 * <p>Implementations are:
 *
 * <ul>
 *   <li>{@link org.sosy_lab.pjbdd.creator.bdd.BDDCreator}
 * </ul>
 *
 * @author Stephan Holzner
 * @see org.sosy_lab.pjbdd.creator.bdd.BDDCreator
 * @since 1.0
 */
public interface Creator {

  String VERSION = "1.1";

  /**
   * Creates a new variable and returns the predicate representing it.
   *
   * @return predicate representing the new variable
   */
  BDD makeVariable();

  /**
   * Get the BDD representation of variable i.
   *
   * @param i - the ith variable tobe returned
   * @return the BDD representation of variable i
   */
  default BDD makeIthVar(int i) {
    return makeNode(makeFalse(), makeTrue(), i);
  }

  /**
   * Recursive restricts a nodes's specific variable.
   *
   * @param bdd - node to be restricted
   * @param var - variable to be restricted
   * @param restrictionVar - restricted to true or false branch
   * @return restricted bdd
   */
  BDD restrict(BDD bdd, int var, boolean restrictionVar);

  /**
   * Takes a {@link BDD} and finds any satisfying variable assignment.
   *
   * @param f - the {@link BDD} node
   * @return one satisfying variable assignment.
   */
  BDD anySat(BDD f);

  /**
   * Get {@link BDD}'s number of satisfying variable assignments. All created variables are
   * considered.
   *
   * @param f - the {@link BDD} node
   * @return the number of satisfying variable assignments.
   */
  BigInteger satCount(BDD f);

  /**
   * Get a representation of logical truth.
   *
   * @return a representation of logical truth
   */
  BDD makeTrue();

  /**
   * Get a representation of logical falseness.
   *
   * @return a representation of logical falseness
   */
  BDD makeFalse();

  /**
   * Creates a {@link BDD} representing a negation of the argument.
   *
   * @param f - the {@link BDD} argument
   * @return (not f1)
   */
  BDD makeNot(BDD f);

  /**
   * Creates a {@link BDD} representing an AND of the two arguments.
   *
   * @param f1 - getIf {@link BDD} argument
   * @param f2 - getThen {@link BDD} argument
   * @return (f1 and f2)
   */
  BDD makeAnd(BDD f1, BDD f2);

  /**
   * Creates a {@link BDD} representing an OR of the two arguments.
   *
   * @param f1 - getIf {@link BDD} argument
   * @param f2 - getThen {@link BDD} argument
   * @return (f1 or f2)
   */
  BDD makeOr(BDD f1, BDD f2);

  /**
   * Creates a {@link BDD} representing a Xor of the two arguments.
   *
   * @param f1 - getIf {@link BDD} argument
   * @param f2 - getThen {@link BDD} argument
   * @return (f1 xor f2)
   */
  BDD makeXor(BDD f1, BDD f2);

  /**
   * Creates a {@link BDD} representing a Nor of the two arguments.
   *
   * @param f1 - getIf {@link BDD} argument
   * @param f2 - getThen {@link BDD} argument
   * @return (f1 nor f2)
   */
  BDD makeNor(BDD f1, BDD f2);

  /**
   * Creates a {@link BDD} representing a Xnor of the two arguments.
   *
   * @param f1 - getIf {@link BDD} argument
   * @param f2 - getThen {@link BDD} argument
   * @return (f1 xnor f2)
   */
  BDD makeXnor(BDD f1, BDD f2);

  /**
   * Creates a {@link BDD} representing a Nand of the two arguments.
   *
   * @param f1 - getIf {@link BDD} argument
   * @param f2 - getThen {@link BDD} argument
   * @return (f1 nand f2)
   */
  BDD makeNand(BDD f1, BDD f2);

  /**
   * Creates a {@link BDD} representing an equality (bi-implication) of the two arguments.
   *
   * @param f1 - getIf {@link BDD} argument
   * @param f2 - getThen {@link BDD} argument
   * @return (f1 equal f2)
   */
  BDD makeEqual(BDD f1, BDD f2);

  /**
   * Creates a {@link BDD} representing an inequality (XOR) of the two arguments.
   *
   * @param f1 - getIf {@link BDD} argument
   * @param f2 - getThen {@link BDD} argument
   * @return (f1 inequal f2)
   */
  BDD makeUnequal(BDD f1, BDD f2);

  /**
   * Creates a {@link BDD} representing an if then else construct of the three arguments.
   *
   * @param f1 - getIf {@link BDD} argument
   * @param f2 - getThen {@link BDD} argument
   * @param f3 - getElse {@link BDD} argument
   * @return (if f1 then f2 else f3)
   */
  BDD makeIte(BDD f1, BDD f2, BDD f3);

  /**
   * Creates a {@link BDD} representing an existential quantification of the getThen argument. If
   * there are more arguments, each of them is quantified.
   *
   * @param f1 - getIf {@link BDD} argument
   * @param f2 - one or more {@link BDD}
   * @return (exists f2... : f1)
   */
  BDD makeExists(BDD f1, BDD... f2);

  /**
   * Creates a {@link BDD} representing an logical implication of the getThen argument.
   *
   * @param f1 - getIf {@link BDD} argument
   * @param f2 - getThen {@link BDD} argument
   * @return (f1 imply f2)
   */
  BDD makeImply(BDD f1, BDD f2);

  /**
   * checks whether the data {@link BDD} represented by f1 is a subset of that represented by f2.
   *
   * @param f1 - getIf {@link BDD} argument
   * @param f2 - getThen {@link BDD} argument
   * @return true if (f1 entails f2), false otherwise
   */
  boolean entails(BDD f1, BDD f2);

  /**
   * A {@link BDD} consists of the form if (predicate) then formula1 else formula2. This method
   * decomposes a {@link BDD} into these three parts.
   *
   * @param f - a {@link BDD}
   * @return a triple with the condition predicate and the formulas for the true branch and the else
   *     branch
   */
  IfThenElseData getIfThenElse(BDD f);

  /**
   * Sets the bdd variable ordering and updates all to the new ordering.
   *
   * @param pOrder - the new order of the variables.
   */
  void setVarOrder(List<Integer> pOrder);

  /**
   * Returns existing {@link BDD} node with given low, high branches and variable. Creates a new one
   * if there is no existing.
   *
   * @param low - the low branch
   * @param high - the high branch
   * @param var - the bdd variable
   * @return new or existing bdd
   */
  BDD makeNode(BDD low, BDD high, int var);

  /**
   * Get {@link BDD} node's low branch child node.
   *
   * @param top - the node
   * @return the low branch child node
   */
  BDD getLow(BDD top);

  /**
   * Get {@link BDD} node's high branch child node.
   *
   * @param top - the node
   * @return the high branch child node
   */
  BDD getHigh(BDD top);

  /** Shutdown boolean_operations and cleanup resources. */
  void shutDown();

  /**
   * Get number of existing variables.
   *
   * @return number of existing variables
   */
  int getVariableCount();

  /**
   * Get all existing variables in current ordering.
   *
   * @return all existing variables in current ordering
   */
  int[] getVariableOrdering();

  /**
   * Set number of existing variables.
   *
   * @param count - the new variable count
   */
  void setVariableCount(int count);

  /**
   * Get current statistics as {@link Stats} object.
   *
   * @return current statistics as {@link Stats} object
   */
  Stats getCreatorStats();

  /**
   * Get current framework version.
   *
   * @return current framework version
   */
  default String getVersion() {
    return VERSION;
  }

  /**
   * Stats interface defining getter methods for all all required parameters.
   *
   * @author Stephan Holzner
   * @since 1.0
   */
  interface Stats {

    /**
     * String format the objects properties.
     *
     * @return String formatted objects properties
     */
    String prettyPrint();

    /**
     * Get number of allocated nodes.
     *
     * @return number of allocated nodes
     */
    int getNodeCount();

    /**
     * Get number of created variables.
     *
     * @return number of created variables
     */
    int getVariableCount();

    /**
     * Get number of garbage collected elements.
     *
     * @return number of garbage collected elements
     */
    int getGarbageCollections();
  }
}
