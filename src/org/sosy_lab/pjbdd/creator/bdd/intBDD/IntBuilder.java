package org.sosy_lab.pjbdd.creator.bdd.intBDD;

import java.math.BigInteger;
import org.sosy_lab.pjbdd.algorithm.intBDD.ForkJoinIntAlgorithm;
import org.sosy_lab.pjbdd.algorithm.intBDD.IntAlgorithm;
import org.sosy_lab.pjbdd.algorithm.intBDD.IntBDDAlgorithm;
import org.sosy_lab.pjbdd.algorithm.sat.IntSatAlgorithm;
import org.sosy_lab.pjbdd.cache.bdd.Cache;
import org.sosy_lab.pjbdd.cache.bdd.GuavaCache;
import org.sosy_lab.pjbdd.cache.intBDD.IntNotCache;
import org.sosy_lab.pjbdd.cache.intBDD.IntOpCache;
import org.sosy_lab.pjbdd.creator.Builder;
import org.sosy_lab.pjbdd.creator.bdd.Creator;
import org.sosy_lab.pjbdd.node.manager.intBDD.IntBDDNodeManager;
import org.sosy_lab.pjbdd.node.manager.intBDD.IntNodeManager;
import org.sosy_lab.pjbdd.uniquetable.intBDD.ConcurrentIntUniqueTable;
import org.sosy_lab.pjbdd.util.threadpool.ParallelismManager;
import org.sosy_lab.pjbdd.util.threadpool.ParallelismManagerImpl;

/**
 * Builder class to create {@link Creator} environments implemented with {@link IntCreator}.
 *
 * @author Stephan Holzner
 * @see Creator
 * @see IntCreator
 * @since 1.0
 */
public class IntBuilder extends Builder {

  /**
   * Creates a new Creator environment with previously set parameters and backing int based
   * algorithms.
   *
   * @return a new int based BDD environment
   */
  public Creator build() {
    IntNodeManager nodeManager =
        new IntBDDNodeManager(
            new ConcurrentIntUniqueTable(
                selectedTableSize, selectedParallelism, selectedIncreaseFactor));
    nodeManager.setVarCount(this.selectedVarCount);
    IntOpCache iteCache = new IntOpCache(selectedCacheSize, selectedParallelism);
    IntOpCache opCache = new IntOpCache(selectedCacheSize, selectedParallelism);
    IntNotCache notCache = new IntNotCache(selectedCacheSize, selectedParallelism);
    IntOpCache quantCache = new IntOpCache(selectedCacheSize, selectedParallelism);
    Cache<Integer, BigInteger> satCountCache = new GuavaCache<>();
    satCountCache.init(selectedCacheSize, selectedParallelism);
    IntAlgorithm algorithm =
        this.selectedThreads > 1
            ? new ForkJoinIntAlgorithm(
                opCache, iteCache, notCache, quantCache, nodeManager, parallelismManager)
            : new IntBDDAlgorithm(opCache, iteCache, notCache, quantCache, nodeManager);
    return new IntCreator(algorithm, new IntSatAlgorithm(nodeManager, satCountCache), nodeManager);
  }

  public IntBuilder setSelectedParallelism(int selectedParallelism) {
    this.selectedParallelism = selectedParallelism;
    return this;
  }

  public IntBuilder setSelectedCacheSize(int selectedCacheSize) {
    this.selectedCacheSize = selectedCacheSize;
    return this;
  }

  public IntBuilder setSelectedThreads(int selectedThreads) {
    this.selectedThreads = selectedThreads;
    return this;
  }

  public IntBuilder setParallelismManager(ParallelismManager parallelismManager) {
    this.parallelismManager = parallelismManager;
    return this;
  }

  public IntBuilder setParallelism(int selectedParallelism) {
    this.selectedParallelism = selectedParallelism;
    return this;
  }

  public IntBuilder setTableSize(int selectedTableSize) {
    this.selectedTableSize = selectedTableSize;
    return this;
  }

  public IntBuilder setCacheSize(int selectedCacheSize) {
    this.selectedCacheSize = selectedCacheSize;
    return this;
  }

  public IntBuilder setThreads(int selectedThreads) {
    if (selectedThreads != this.selectedThreads) {
      parallelismManager = new ParallelismManagerImpl(selectedThreads);
    }
    this.selectedThreads = selectedThreads;
    return this;
  }

  public IntBuilder setVarCount(int selectedVarCount) {
    this.selectedVarCount = selectedVarCount;
    return this;
  }

  public IntBuilder setIncreaseFactor(int selectedIncreaseFactor) {
    this.selectedIncreaseFactor = selectedIncreaseFactor;
    return this;
  }
}
