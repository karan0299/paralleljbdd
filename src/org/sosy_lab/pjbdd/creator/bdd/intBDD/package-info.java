/**
 * classes for int based bdd calculations.
 *
 * @since 1.0
 * @author Stephan Holzner
 * @version 1.0
 */
package org.sosy_lab.pjbdd.creator.bdd.intBDD;
