package org.sosy_lab.pjbdd.creator.bdd.intBDD;

import java.lang.ref.Reference;
import java.lang.ref.ReferenceQueue;
import java.lang.ref.WeakReference;
import java.math.BigInteger;
import java.util.List;
import java.util.concurrent.ConcurrentLinkedDeque;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import org.sosy_lab.pjbdd.algorithm.intBDD.IntAlgorithm;
import org.sosy_lab.pjbdd.algorithm.sat.IntSatAlgorithm;
import org.sosy_lab.pjbdd.creator.bdd.Creator;
import org.sosy_lab.pjbdd.node.BDD;
import org.sosy_lab.pjbdd.node.manager.intBDD.IntNodeManager;
import org.sosy_lab.pjbdd.uniquetable.intBDD.IntUniqueTable;
import org.sosy_lab.pjbdd.util.IfThenElseData;
import org.sosy_lab.pjbdd.util.reference.IntHoldingWeakReference;
import org.sosy_lab.pjbdd.util.reference.ReclaimedReferenceCleaningThread;

/**
 * {@link Creator} implementation with ints as underlying bdd data structure.
 *
 * @author Stephan Holzner
 * @see Creator
 * @since 1.0
 */
public class IntCreator implements Creator {

  /** demon thread to cleanup reclaimed entries. */
  private final CleanUpThread cleaner;

  /** reference queue used to cleanup reclaimed entries. */
  private final ReferenceQueue<BDDimpl> queue = new ReferenceQueue<>();

  /** holds created weak references. */
  private final ConcurrentLinkedDeque<WeakReference<BDDimpl>> references =
      new ConcurrentLinkedDeque<>();

  /** lock object for reorder operations: read lock for normal operations and write for reorder. */
  protected final ReadWriteLock reorderLock = new ReentrantReadWriteLock();

  private final IntSatAlgorithm satAlgorithm;
  private final IntAlgorithm algorithm;
  private final IntNodeManager nodeManager;

  /** Constructor creates and starts a new {@link CleanUpThread}. */
  IntCreator(IntAlgorithm algorithm, IntSatAlgorithm satAlgorithm, IntNodeManager nodeManager) {
    this.algorithm = algorithm;
    this.satAlgorithm = satAlgorithm;
    this.nodeManager = nodeManager;
    cleaner = new CleanUpThread();
    startCleaner();
  }

  /** Start cleanup thread. */
  protected void startCleaner() {
    cleaner.start();
  }

  /** {@inheritDoc} */
  @Override
  public BDD restrict(BDD bdd, int var, boolean restrictionVar) {
    throw new IllegalArgumentException();
  }

  /** {@inheritDoc} */
  @Override
  public BDD anySat(BDD f) {
    reorderLock.readLock().lock();
    try {
      return makeBDD(satAlgorithm.anySat(((BDDimpl) f).index));
    } finally {
      reorderLock.readLock().unlock();
    }
  }

  /** {@inheritDoc} */
  @Override
  public BigInteger satCount(BDD f) {
    reorderLock.readLock().lock();
    try {
      return satAlgorithm.satCount(((BDDimpl) f).index);
    } finally {
      reorderLock.readLock().unlock();
    }
  }

  /** {@inheritDoc} */
  @Override
  public BDD makeTrue() {
    return makeBDD(IntUniqueTable.ONE);
  }

  /** {@inheritDoc} */
  @Override
  public BDD makeFalse() {
    return makeBDD(IntUniqueTable.ZERO);
  }

  /** {@inheritDoc} */
  @Override
  public BDD makeIte(BDD f1, BDD f2, BDD f3) {
    reorderLock.readLock().lock();
    try {
      return makeBDD(
          algorithm.makeIte(((BDDimpl) f1).index, ((BDDimpl) f2).index, ((BDDimpl) f3).index));
    } finally {
      reorderLock.readLock().unlock();
    }
  }

  /** {@inheritDoc} */
  @Override
  public BDD makeVariable() {
    return makeBDD(
        nodeManager.makeNode(
            nodeManager.getVarCount(), nodeManager.getFalse(), nodeManager.getTrue()));
  }

  /** {@inheritDoc} */
  @Override
  public boolean entails(BDD f1, BDD f2) {
    reorderLock.readLock().lock();
    try {
      return entailsRec(f1, f2);
    } finally {
      reorderLock.readLock().unlock();
    }
  }

  /**
   * recursive helper function for entails operations.
   *
   * @param f1 - getIf node argument
   * @param f2 - getThen node argument
   * @return f1 entails f2
   */
  private boolean entailsRec(BDD f1, BDD f2) {
    if (f1.equals(f2)) {
      return true;
    }
    if (f1.isLeaf()) {
      return false;
    }
    if (level(((BDDimpl) f1).index) <= level(((BDDimpl) f2).index)) {
      return false;
    }
    return entails(f1.getLow(), f2) || entails(f1.getHigh(), f2);
  }

  /** {@inheritDoc} */
  @Override
  public BDD makeNot(BDD f) {
    reorderLock.readLock().lock();
    try {
      return makeBDD(algorithm.makeNot(((BDDimpl) f).index));
    } finally {
      reorderLock.readLock().unlock();
    }
  }

  /** {@inheritDoc} */
  @Override
  public BDD makeAnd(BDD f1, BDD f2) {
    reorderLock.readLock().lock();
    try {
      return makeBDD(
          algorithm.makeOp(
              ((BDDimpl) f1).index, ((BDDimpl) f2).index, IntAlgorithm.ApplyOp.OP_AND));
    } finally {
      reorderLock.readLock().unlock();
    }
  }

  /** {@inheritDoc} */
  @Override
  public BDD makeOr(BDD f1, BDD f2) {
    reorderLock.readLock().lock();
    try {
      return makeBDD(
          algorithm.makeOp(((BDDimpl) f1).index, ((BDDimpl) f2).index, IntAlgorithm.ApplyOp.OP_OR));
    } finally {
      reorderLock.readLock().unlock();
    }
  }

  /** {@inheritDoc} */
  @Override
  public BDD makeXor(BDD f1, BDD f2) {
    reorderLock.readLock().lock();
    try {
      return makeBDD(
          algorithm.makeOp(
              ((BDDimpl) f1).index, ((BDDimpl) f2).index, IntAlgorithm.ApplyOp.OP_XOR));
    } finally {
      reorderLock.readLock().unlock();
    }
  }

  /** {@inheritDoc} */
  @Override
  public BDD makeNor(BDD f1, BDD f2) {
    reorderLock.readLock().lock();
    try {
      return makeBDD(
          algorithm.makeOp(
              ((BDDimpl) f1).index, ((BDDimpl) f2).index, IntAlgorithm.ApplyOp.OP_NOR));
    } finally {
      reorderLock.readLock().unlock();
    }
  }

  /** {@inheritDoc} */
  @Override
  public BDD makeNand(BDD f1, BDD f2) {
    reorderLock.readLock().lock();
    try {
      return makeBDD(
          algorithm.makeOp(
              ((BDDimpl) f1).index, ((BDDimpl) f2).index, IntAlgorithm.ApplyOp.OP_NAND));
    } finally {
      reorderLock.readLock().unlock();
    }
  }

  /** {@inheritDoc} */
  @Override
  public BDD makeExists(BDD f1, BDD... f2) {
    reorderLock.readLock().lock();
    try {
      BDD f = f2[0];
      for (int i = 1; i < f2.length; i++) {
        f = makeAnd(f, f2[i]);
      }
      return makeExQuant(f1, f);
    } finally {
      reorderLock.readLock().unlock();
    }
  }

  /** {@inheritDoc} */
  @Override
  public BDD makeXnor(BDD f1, BDD f2) {
    return makeIte(f1, f2, makeNot(f2));
  }

  /** {@inheritDoc} */
  @Override
  public BDD makeEqual(BDD f1, BDD f2) {
    return makeXnor(f1, f2);
  }

  /** {@inheritDoc} */
  @Override
  public BDD makeUnequal(BDD f1, BDD f2) {
    return makeXor(f1, f2);
  }

  /** {@inheritDoc} */
  @Override
  public BDD makeImply(BDD f1, BDD f2) {
    reorderLock.readLock().lock();
    try {
      return makeBDD(
          algorithm.makeOp(
              ((BDDimpl) f1).index, ((BDDimpl) f2).index, IntAlgorithm.ApplyOp.OP_IMP));
    } finally {
      reorderLock.readLock().unlock();
    }
  }

  /** {@inheritDoc} */
  @Override
  public IfThenElseData getIfThenElse(BDD f) {
    return new IfThenElseData(makeIthVar(f.getVariable()), f.getLow(), f.getHigh());
  }

  /** {@inheritDoc} */
  @Override
  public BDD makeNode(BDD low, BDD high, int var) {
    return makeBDD(nodeManager.makeNode(var, ((BDDimpl) low).index, ((BDDimpl) high).index));
  }

  /** {@inheritDoc} */
  @Override
  public BDD getLow(BDD top) {
    return makeBDD(low(((BDDimpl) top).index));
  }

  /** {@inheritDoc} */
  @Override
  public BDD getHigh(BDD top) {
    return makeBDD(high(((BDDimpl) top).index));
  }

  /** {@inheritDoc} */
  @Override
  public void shutDown() {
    cleaner.shutdown();
    references.clear();
  }

  @Override
  public int getVariableCount() {
    return nodeManager.getVarCount();
  }

  @Override
  public int[] getVariableOrdering() {
    return nodeManager.getCurrentOrdering();
  }

  @Override
  public void setVariableCount(int count) {
    reorderLock.writeLock().lock();
    try {
      nodeManager.setVarCount(var(count));
    } finally {
      reorderLock.writeLock().unlock();
    }
  }

  /** {@inheritDoc} */
  @Override
  public void setVarOrder(List<Integer> pOrder) {
    reorderLock.writeLock().lock();
    try {
      nodeManager.setVarOrdering(pOrder.stream().mapToInt(i -> i).toArray());
    } finally {
      reorderLock.writeLock().unlock();
    }
  }

  /** {@inheritDoc} */
  @Override
  public Stats getCreatorStats() {
    throw new UnsupportedOperationException("Not yet implemented");
  }

  /**
   * convert internal index representation to BDD object for external use. Creates WeakReference for
   * object to modify reference count on garbage collection reclaim.
   *
   * @param id - bdd's index
   * @return created bdd object
   */
  private BDD makeBDD(int id) {
    BDDimpl bdd = new BDDimpl(id);
    references.add(new IntHoldingWeakReference<>(bdd, queue, id));
    return bdd;
  }

  /**
   * Helper method for existential quantification.
   *
   * @param f1 - an abstract bdd formula
   * @param f2 - an abstract bdd formula
   * @return existential quantification of f1 and f2
   */
  private BDD makeExQuant(BDD f1, BDD f2) {
    return makeBDD(algorithm.exQuant(((BDDimpl) f1).index, ((BDDimpl) f2).index));
  }

  /**
   * add a new reference for given bdd.
   *
   * @param index - of given bdd
   */
  protected void addRef(int index) {
    nodeManager.addRefs(index);
  }

  /**
   * free one reference for given bdd. Cascading delete if there are no more references.
   *
   * @param index - of given bdd
   */
  protected void freeRef(int index) {
    nodeManager.freeRefs(index);
  }

  /**
   * get low child branch for given bdd.
   *
   * @param root - the given bdd
   * @return low branch for given bdd
   */
  protected int low(int root) {
    return nodeManager.low(root);
  }

  /**
   * get high child branch for given bdd.
   *
   * @param root - the given bdd
   * @return high branch for given bdd
   */
  protected int high(int root) {
    return nodeManager.high(root);
  }

  /**
   * get variable for given bdd.
   *
   * @param index - the given bdd
   * @return variable for given bdd
   */
  protected int var(int index) {
    return nodeManager.var(index);
  }

  /**
   * get level for given bdd's variable.
   *
   * @param index - the given bdd
   * @return level of given bdd's variable
   */
  protected int level(int index) {
    return nodeManager.level(var(index));
  }

  /**
   * Int Wrapper {@link BDD} implementation. Only holds bdd's unique table index. Uses index and
   * Creator as shadow reference to determine {@link BDD} methods.
   *
   * @author Stephan Holzner
   * @see BDD
   * @since 1.0
   */
  class BDDimpl implements BDD {
    /** the bdd's unique table index. */
    final int index;

    /**
     * Creates a new {@link BDDimpl} instance for a given index.
     *
     * @param index - bdd's index
     */
    BDDimpl(int index) {
      this.index = index;
      addRef(this.index);
    }

    /**
     * Get the negation of this bdd.
     *
     * @return the negation of this bdd
     */
    public BDD not() {
      return IntCreator.this.makeNot(this);
    }

    /** {@inheritDoc} */
    @Override
    public int getVariable() {
      return IntCreator.this.var(index);
    }

    /** {@inheritDoc} */
    @Override
    public BDD getLow() {
      return makeBDD(IntCreator.this.low(index));
    }

    /** {@inheritDoc} */
    @Override
    public BDD getHigh() {
      return makeBDD(IntCreator.this.high(index));
    }

    /** {@inheritDoc} */
    @Override
    public boolean isTrue() {
      return index == IntUniqueTable.ONE;
    }

    /** {@inheritDoc} */
    @Override
    public boolean isFalse() {
      return index == IntUniqueTable.ZERO;
    }

    /** {@inheritDoc} */
    @Override
    public boolean equals(Object o) {
      if (o instanceof BDDimpl) {
        return ((BDDimpl) o).index == index;
      }
      return false;
    }

    /** {@inheritDoc} */
    @Override
    public int hashCode() {
      return index;
    }
  }

  /**
   * {@link ReclaimedReferenceCleaningThread} sub class to handle garbage collected bdd instances.
   *
   * @author Stephan Holzner
   * @see ReclaimedReferenceCleaningThread
   * @since 1.0
   */
  private class CleanUpThread extends ReclaimedReferenceCleaningThread {

    /** {@inheritDoc} */
    @Override
    protected void deleteReclaimedEntries() throws InterruptedException {
      Reference<? extends BDD> sv = queue.remove(1000);
      if (sv != null) {
        if (sv instanceof IntHoldingWeakReference && !isShutdown()) {
          references.remove(sv);
          freeRef(((IntHoldingWeakReference<?>) sv).intValue());
        }
      }
    }
  }

  @Override
  public String toString() {
    return this.getClass().getSimpleName();
  }
}
