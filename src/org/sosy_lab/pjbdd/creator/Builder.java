package org.sosy_lab.pjbdd.creator;

import org.sosy_lab.pjbdd.creator.bdd.Creator;
import org.sosy_lab.pjbdd.util.threadpool.ParallelismManager;
import org.sosy_lab.pjbdd.util.threadpool.ParallelismManagerImpl;

/**
 * Builder base class for creator environment instantiations.
 *
 * @author Stephan Holzner
 * @see Creator
 * @since 1.0
 */
public class Builder {
  /** indicates the number of concurrent accesses to the selectedTable default value = 1000. */
  protected int selectedParallelism = 1000;
  /** indicates the initial selectedTable size default value = 500000. */
  protected int selectedTableSize = 500000;
  /** indicates the fixed computation cache size default value = 10000. */
  protected int selectedCacheSize = 10000;
  /**
   * indicates the fixed worker thread count default value =
   * Runtime.getRuntime().availableProcessors().
   */
  protected int selectedThreads = Runtime.getRuntime().availableProcessors();
  /** indicates the initial created variable count default value = 10. */
  protected int selectedVarCount = 10;
  /** indicates the selectedTable's fixed increase factor default value = 1. */
  protected int selectedIncreaseFactor = 1;
  /** selected parallelism manager instance. */
  protected ParallelismManager parallelismManager = new ParallelismManagerImpl();
}
