package org.sosy_lab.pjbdd.creator.zdd;

import java.math.BigInteger;
import java.util.List;
import org.sosy_lab.pjbdd.node.BDD;

/**
 * Main zdd boolean_operations interface defining the following operations.
 *
 * <p>Implementations are:
 *
 * <ul>
 *   <li>{@link ZDDCreatorImpl}
 * </ul>
 *
 * @author Stephan Holzner
 * @see ZDDCreatorImpl
 * @since 1.0
 */
public interface ZDDCreator {

  /**
   * Creates a {@link BDD} representing the high subset of an zdd with restricting a given variable
   * to 1.
   *
   * @param zdd - given zdd
   * @param var - {@link BDD} representation of given variable
   * @return (subset of zdd with var equals ONE)
   */
  BDD subSet1(BDD zdd, BDD var);

  /**
   * Creates a {@link BDD} representing the high subset of an zdd with restricting a given variable
   * to 0.
   *
   * @param zdd - given zdd
   * @param var - {@link BDD} representation of given variable
   * @return (subset of zdd with var equals ZERO)
   */
  BDD subSet0(BDD zdd, BDD var);

  /**
   * Creates a {@link BDD} with inverted variable.
   *
   * @param zdd - given zdd
   * @param var - {@link BDD} representation of given variable
   * @return (invert variable var for zdd)
   */
  BDD change(BDD zdd, BDD var);

  /**
   * Creates a zdd representing the union set of two arguments.
   *
   * @param zdd1 - getIf {@link BDD} argument
   * @param zdd2 - getThen {@link BDD} argument
   * @return (zdd1 UNION zdd2)
   */
  BDD union(BDD zdd1, BDD zdd2);

  /**
   * Creates a zdd representing the difference set of two arguments.
   *
   * @param zdd1 - getIf {@link BDD} argument
   * @param zdd2 - getThen {@link BDD} argument
   * @return (zdd1 DIFFERENCE zdd2)
   */
  BDD difference(BDD zdd1, BDD zdd2);

  /**
   * Creates a zdd representing the intersection set of two arguments.
   *
   * @param zdd1 - getIf {@link BDD} argument
   * @param zdd2 - getThen {@link BDD} argument
   * @return (zdd1 INTERSECT zdd2)
   */
  BDD intersection(BDD zdd1, BDD zdd2);

  /**
   * Creates a zdd representing the product product set of two arguments.
   *
   * @param zdd1 - getIf {@link BDD} argument
   * @param zdd2 - getThen {@link BDD} argument
   * @return (zdd1 { @ literal * } zdd2)
   */
  BDD product(BDD zdd1, BDD zdd2);

  /**
   * Creates a zdd representing the remainder set of two arguments.
   *
   * @param zdd1 - getIf {@link BDD} argument
   * @param zdd2 - getThen {@link BDD} argument
   * @return (zdd1 { @ literal % } zdd2)
   */
  BDD modulo(BDD zdd1, BDD zdd2);

  /**
   * Creates a zdd representing the quotient set of two arguments.
   *
   * @param zdd1 - getIf {@link BDD} argument
   * @param zdd2 - getThen {@link BDD} argument
   * @return (zdd1 { @ literal / } zdd2)
   */
  BDD division(BDD zdd1, BDD zdd2);

  /**
   * Creates a zdd representing the exclusion set of two arguments.
   *
   * @param zdd1 - getIf {@link BDD} argument
   * @param zdd2 - getThen {@link BDD} argument
   * @return (zdd1 EXCLUDE zdd2)
   */
  BDD exclude(BDD zdd1, BDD zdd2);

  /**
   * Creates a zdd representing the restriction set of two arguments.
   *
   * @param zdd1 - getIf {@link BDD} argument
   * @param zdd2 - getThen {@link BDD} argument
   * @return (zdd1 RESTRICT zdd2)
   */
  BDD restrict(BDD zdd1, BDD zdd2);

  /**
   * Returns existing {@link BDD} node with given low, high branches and variable. Creates a new one
   * if there is no existing.
   *
   * @param low - the low branch
   * @param high - the high branch
   * @param var - the bdd variable
   * @return new or existing bdd
   */
  BDD makeNode(BDD low, BDD high, int var);

  /**
   * Get the representation of empty set.
   *
   * @return empty set
   */
  BDD empty();

  /**
   * Get the representation of the base set.
   *
   * @return base set
   */
  BDD base();

  /**
   * Get the universal set representation.
   *
   * @return universal set
   */
  BDD universe();

  /**
   * creates a cube of a variable amount of input variables. Example: Ordering = x1,x2,x3,x4 ...
   * Cube of: x1, x3 Use: cube(1,0,1).
   *
   * @param vars - input variables
   * @return cube of input variables
   */
  BDD cube(int... vars);

  /**
   * Get {@link BDD}'s number of satisfying variable assignments. All created variables are
   * considered.
   *
   * @param bdd - the {@link BDD} node
   * @return the number of satisfying variable assignments.
   */
  BigInteger satCount(BDD bdd);

  /** Shutdown boolean_operations and cleanup resources. */
  void shutdown();

  /**
   * Sets the bdd variable ordering and updates all to the new ordering.
   *
   * @param pOrder - the new order of the variables.
   */
  void setVarOrder(List<Integer> pOrder);

  /**
   * Set number of existing variables.
   *
   * @param count - the new variable count
   */
  void setVariableCount(int count);
}
