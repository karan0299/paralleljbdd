package org.sosy_lab.pjbdd.creator.zdd;

import java.math.BigInteger;
import org.sosy_lab.pjbdd.Builders;
import org.sosy_lab.pjbdd.algorithm.sat.ZDDSat;
import org.sosy_lab.pjbdd.algorithm.zdd.ZDDAlgorithm;
import org.sosy_lab.pjbdd.algorithm.zdd.ZDDConcurrentAlgorithm;
import org.sosy_lab.pjbdd.algorithm.zdd.ZDDSerialAlgorithm;
import org.sosy_lab.pjbdd.cache.bdd.ArrayCache;
import org.sosy_lab.pjbdd.cache.bdd.Cache;
import org.sosy_lab.pjbdd.cache.bdd.GuavaCache;
import org.sosy_lab.pjbdd.creator.Builder;
import org.sosy_lab.pjbdd.node.BDD;
import org.sosy_lab.pjbdd.node.BDDNode;
import org.sosy_lab.pjbdd.node.manager.bdd.ZDDNodeManager;
import org.sosy_lab.pjbdd.uniquetable.bdd.BDDConcurrentArray;
import org.sosy_lab.pjbdd.uniquetable.bdd.BDDConcurrentWeakHashDeque;
import org.sosy_lab.pjbdd.uniquetable.bdd.BDDConcurrentWeakHashMap;
import org.sosy_lab.pjbdd.uniquetable.bdd.BDDLockOnWriteArray;
import org.sosy_lab.pjbdd.uniquetable.bdd.UniqueTable;
import org.sosy_lab.pjbdd.util.threadpool.ParallelismManager;
import org.sosy_lab.pjbdd.util.threadpool.ParallelismManagerImpl;

/**
 * Builder class to create {@link ZDDCreator} environments implemented with {@link ZDDCreatorImpl}.
 *
 * @author Stephan Holzner
 * @see ZDDCreator
 * @see ZDDCreatorImpl
 * @since 1.0
 */
public class ZDDBuilder extends Builder {

  /** selected uniquetabble instance. */
  protected UniqueTable<BDD> selectedTable;
  /** selected bdd table type. */
  protected Builders.TableType tableType = Builders.TableType.Array;
  /** selected bdd instantiation factory. */
  protected BDDNode.Factory selectedFactory = new BDDNode.Factory();

  /**
   * Creates a new ZDDCreator environment with previously set parameters.
   *
   * @return a new ZDD environment.
   */
  public ZDDCreator build() {
    ZDDNodeManager nodeManager = new ZDDNodeManager(makeTable());
    nodeManager.setVarCount(this.selectedVarCount);

    Cache<Integer, Cache.CacheData> cache = new ArrayCache();
    cache.init(selectedCacheSize, selectedParallelism);
    ZDDAlgorithm algorithm =
        this.selectedThreads > 1
            ? new ZDDConcurrentAlgorithm(cache, nodeManager, parallelismManager)
            : new ZDDSerialAlgorithm(cache, nodeManager);
    Cache<BDD, BigInteger> satCountCache = new GuavaCache<>();
    satCountCache.init(selectedCacheSize, selectedParallelism);

    return new ZDDCreatorImpl(new ZDDSat(satCountCache, nodeManager), nodeManager, algorithm);
  }

  public ZDDBuilder setSelectedParallelism(int parallelism) {
    this.selectedParallelism = parallelism;
    return this;
  }

  public ZDDBuilder setSelectedCacheSize(int cacheSize) {
    this.selectedCacheSize = cacheSize;
    return this;
  }

  public ZDDBuilder setTableType(Builders.TableType type) {
    this.tableType = type;
    return this;
  }

  public ZDDBuilder setSelectedFactory(BDDNode.Factory factory) {
    this.selectedFactory = factory;
    return this;
  }

  public ZDDBuilder setParallelismManager(ParallelismManager manager) {
    this.parallelismManager = manager;
    return this;
  }

  public ZDDBuilder setParallelism(int parallelism) {
    this.selectedParallelism = parallelism;
    return this;
  }

  public ZDDBuilder setTableSize(int tableSize) {
    this.selectedTableSize = tableSize;
    return this;
  }

  public ZDDBuilder setCacheSize(int cacheSize) {
    this.selectedCacheSize = cacheSize;
    return this;
  }

  public ZDDBuilder setThreads(int selectedThreads) {
    if (selectedThreads != this.selectedThreads) {
      parallelismManager = new ParallelismManagerImpl(selectedThreads);
    }
    this.selectedThreads = selectedThreads;
    return this;
  }

  public ZDDBuilder setVarCount(int selectedVarCount) {
    this.selectedVarCount = selectedVarCount;
    return this;
  }

  public ZDDBuilder setIncreaseFactor(int selectedIncreaseFactor) {
    this.selectedIncreaseFactor = selectedIncreaseFactor;
    return this;
  }

  public ZDDBuilder setSelectedTable(UniqueTable<BDD> uniqueTable) {
    this.selectedTable = uniqueTable;
    return this;
  }

  protected UniqueTable<BDD> makeTable() {
    if (selectedTable != null) {
      return selectedTable;
    }
    switch (tableType) {
      case RWLockArray:
        return new BDDLockOnWriteArray(selectedIncreaseFactor, selectedTableSize, selectedFactory);
      case Array:
        return new BDDConcurrentArray(
            selectedParallelism, selectedIncreaseFactor, selectedTableSize, selectedFactory);
      case ConcurrentHashBucket:
        return new BDDConcurrentWeakHashDeque(
            selectedTableSize, selectedParallelism, selectedFactory);
      default:
        return new BDDConcurrentWeakHashMap(selectedTableSize, selectedParallelism);
    }
  }
}
