package org.sosy_lab.pjbdd.creator.zdd;

import java.math.BigInteger;
import java.util.List;
import org.sosy_lab.pjbdd.algorithm.sat.SatAlgorithm;
import org.sosy_lab.pjbdd.algorithm.zdd.ZDDAlgorithm;
import org.sosy_lab.pjbdd.creator.bdd.AbstractCreator;
import org.sosy_lab.pjbdd.node.BDD;
import org.sosy_lab.pjbdd.node.manager.bdd.ZDDNodeManager;

/**
 * {@link ZDDCreator} implementation which uses {@link AbstractCreator} as a base class.
 *
 * @author Stephan Holzner
 * @see ZDDCreator
 * @see AbstractCreator
 * @since 1.0
 */
public class ZDDCreatorImpl extends AbstractCreator implements ZDDCreator {

  /** the backing zdd algorithm implantation. */
  private ZDDAlgorithm algorithm;

  /**
   * Creates a new zdd creator environment.
   *
   * @param satAlgorithm - the chosen sat algorithm
   * @param nodeManager - the backing node handler
   * @param algorithm - the backing algorithm for zdd implementation
   */
  ZDDCreatorImpl(SatAlgorithm satAlgorithm, ZDDNodeManager nodeManager, ZDDAlgorithm algorithm) {
    super(nodeManager, satAlgorithm);
    this.algorithm = algorithm;
  }

  /** {@inheritDoc} */
  @Override
  public BDD subSet1(BDD zdd, BDD var) {
    reorderLock.readLock().lock();
    try {
      return algorithm.subSet1(zdd, var);
    } finally {
      reorderLock.readLock().unlock();
    }
  }
  /** {@inheritDoc} */
  @Override
  public BDD subSet0(BDD zdd, BDD var) {
    reorderLock.readLock().lock();
    try {
      return algorithm.subSet0(zdd, var);
    } finally {
      reorderLock.readLock().unlock();
    }
  }
  /** {@inheritDoc} */
  @Override
  public BDD change(BDD zdd, BDD var) {
    reorderLock.readLock().lock();
    try {
      return algorithm.change(zdd, var);
    } finally {
      reorderLock.readLock().unlock();
    }
  }
  /** {@inheritDoc} */
  @Override
  public BDD union(BDD zdd1, BDD zdd2) {
    reorderLock.readLock().lock();
    try {
      return algorithm.union(zdd1, zdd2);
    } finally {
      reorderLock.readLock().unlock();
    }
  }
  /** {@inheritDoc} */
  @Override
  public BDD difference(BDD zdd1, BDD zdd2) {
    reorderLock.readLock().lock();
    try {
      return algorithm.difference(zdd1, zdd2);
    } finally {
      reorderLock.readLock().unlock();
    }
  }
  /** {@inheritDoc} */
  @Override
  public BDD intersection(BDD zdd1, BDD zdd2) {
    reorderLock.readLock().lock();
    try {
      return algorithm.intersection(zdd1, zdd2);
    } finally {
      reorderLock.readLock().unlock();
    }
  }
  /** {@inheritDoc} */
  @Override
  public BDD product(BDD zdd1, BDD zdd2) {
    reorderLock.readLock().lock();
    try {
      return algorithm.product(zdd1, zdd2);
    } finally {
      reorderLock.readLock().unlock();
    }
  }
  /** {@inheritDoc} */
  @Override
  public BDD modulo(BDD zdd1, BDD zdd2) {
    reorderLock.readLock().lock();
    try {
      return algorithm.modulo(zdd1, zdd2);
    } finally {
      reorderLock.readLock().unlock();
    }
  }
  /** {@inheritDoc} */
  @Override
  public BDD division(BDD zdd1, BDD zdd2) {
    reorderLock.readLock().lock();
    try {
      return algorithm.division(zdd1, zdd2);
    } finally {
      reorderLock.readLock().unlock();
    }
  }
  /** {@inheritDoc} */
  @Override
  public BDD exclude(BDD zdd1, BDD zdd2) {
    reorderLock.readLock().lock();
    try {
      return algorithm.exclude(zdd1, zdd2);
    } finally {
      reorderLock.readLock().unlock();
    }
  }
  /** {@inheritDoc} */
  @Override
  public BDD restrict(BDD zdd1, BDD zdd2) {
    reorderLock.readLock().lock();
    try {
      return algorithm.restrict(zdd1, zdd2);
    } finally {
      reorderLock.readLock().unlock();
    }
  }
  /** {@inheritDoc} */
  @Override
  public BDD makeNode(BDD low, BDD high, int var) {
    return nodeManager.makeNode(low, high, var);
  }
  /** {@inheritDoc} */
  @Override
  public BDD empty() {
    return nodeManager.getFalse();
  }
  /** {@inheritDoc} */
  @Override
  public BDD base() {
    return nodeManager.getTrue();
  }
  /** {@inheritDoc} */
  @Override
  public BDD universe() {
    reorderLock.readLock().lock();
    try {
      BDD universe = base();
      int[] order = nodeManager.getCurrentOrdering();
      for (int i = order.length; i > 0; ) {
        universe = makeNode(universe, universe, order[--i]);
      }
      return universe;
    } finally {
      reorderLock.readLock().unlock();
    }
  }
  /** {@inheritDoc} */
  @Override
  public BDD cube(int... vars) {
    reorderLock.readLock().lock();
    try {
      BDD cube = base();
      int[] order = getVariableOrdering();
      for (int i = vars.length; i > 0; ) {
        if (vars[--i] == 1) {
          cube = makeNode(empty(), cube, order[i]);
        }
        if (vars[i] == -1) {
          cube = makeNode(cube, cube, order[i]);
        }
      }
      return cube;
    } finally {
      reorderLock.readLock().unlock();
    }
  }
  /** {@inheritDoc} */
  @Override
  public BigInteger satCount(BDD bdd) {
    reorderLock.readLock().lock();
    try {

      return satAlgorithm.satCount(bdd);
    } finally {
      reorderLock.readLock().unlock();
    }
  }
  /** {@inheritDoc} */
  @Override
  public void shutdown() {
    algorithm.shutdown();
    nodeManager.shutdown();
  }
  /** {@inheritDoc} */
  @Override
  public void setVarOrder(List<Integer> pOrder) {
    reorderLock.writeLock().lock();
    try {
      nodeManager.setVarOrder(pOrder);
    } finally {
      reorderLock.writeLock().unlock();
    }
  }
  /** {@inheritDoc} */
  @Override
  public void setVariableCount(int count) {
    reorderLock.writeLock().lock();
    try {
      nodeManager.setVarCount(count);
    } finally {
      reorderLock.writeLock().unlock();
    }
  }
}
